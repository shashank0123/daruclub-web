<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass;
use App\services;
use App\Carmodel;
use App\Package;

class phase3ServiceApiController extends Controller
{
    public function get_packages(Request $request)
 {
    $response = new StdClass;
    $status = 400;
    $message = "Something Went Wrong!!!";
    if (isset($request->user()->id))
        $user_id = $request->user()->id;
    $brand_id = $request->brand_id;
    $model_id = $request->model_id;
    $service_id = $request->service_id;
    $model_data = Carmodel::where('brand_id',$brand_id)
                    ->where('id',$model_id)
                    ->first();  
    if (isset($model_data->type)){                
        $type = $model_data->type;

        $package = Package::where('package_category',$type)
        							->where('service_id', $service_id)
                                    ->get();            
       if ($package){
            $status = 200;
            $message = 'Data Processed';
            $response->package = $package;
        }
    }
    else{
            $status = 200;
            $message = 'model not found';
        
    }
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);

	}

	public function get_services(Request $request)
	{
		$response = new StdClass;
	    $status = 400;
	    $message = "Something Went Wrong!!!";
		$services = services::all();
		if ($services){
			$response->services = $services;
		}
		$response->status = $status;
        $response->message = $message;
        return response()->json($response);
	}
}
