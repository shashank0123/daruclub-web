<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\feedbacks;
use Input;

class FeedbackController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $feedback = feedbacks::all();
        return view('admin.feedbacks', compact('feedback'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_feedback');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validatedData = $request->validate([
            'name' => 'required',            
            'pic' => 'required',            
            'desc' => 'required'            
        ]);
        $feedback = new feedbacks;
        if (Input::hasfile('pic')) {
           $file=Input::file('pic');
           $file->move(public_path(). '/images/feedbacks', $file->getClientOriginalName());
           $feedback->pic=$file->getClientOriginalName();
        }
        $feedback->name          = $request->name;
        $feedback->desc          = $request->desc;
        
        $feedback->save();

        return redirect()->Route('feedbacks.index')->with('message', "Data Successfully Inserted");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feedback = feedbacks::where('id', $id)->first();
        return view('admin.add_feedback', compact('feedback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feedback = feedbacks::find($id);
        if (Input::hasfile('pic')) {
           $file=Input::file('pic');
           $file->move(public_path(). '/images/feedbacks', $file->getClientOriginalName());
           $feedback->pic=$file->getClientOriginalName();
        }
         $feedback->name          = $request->name;
        $feedback->desc          = $request->desc;
        
        $feedback->update();

        return redirect()->Route('feedbacks.index')->with('message', "Data Successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feedback = feedbacks::find($id);
        $feedback->delete();
        return redirect()->Route('feedbacks.index')->with('message', "Data Successfully Deleted");
    }
}
