<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM, StdClass;
use App\BarBooking;

class BarBookingController extends Controller
{
    public function viewMyorders(Request $request)
    {
    	# code...
    }

    public function saveMyorders(Request $request)
    {
    	$response = new StdClass;
    	$status = 400;
    	$message = "Data not found";
        $check = BarBooking::where('user_id', $request->user()->id)->where('bar_id', $request->bar_id)->orderBy('id', 'DESC')->first();
    	if (!$check){
        	$booking = new BarBooking;
        	$booking->bar_id		=	$request->bar_id;
        	$booking->user_id		=	$request->user()->id;

        }

    	$booking->no_of_seat		=	$request->no_of_item;
    	$booking->items		=	$request->item_id;
    	$booking->save();
    	if ($booking){
    		$response->booking = $booking;
    		$status = 200;
    		$message = 'Data retrieved successfully';

    	}

    	$response->status = $status;
    	$response->message = $message;
    	return response()->json($response);

    }

    public function sendnotif(Request $request)
    {
    	$optionBuilder = new OptionsBuilder();
		$optionBuilder->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder('my title');
		$notificationBuilder->setBody('Hello world')
						    ->setSound('default');

		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData(['a_data' => 'my_data']);

		$option = $optionBuilder->build();
		$notification = $notificationBuilder->build();
		$data = $dataBuilder->build();

		$token = "dmezsRhUX6k:APA91bGciFai72p0kxn9G9c6TGT3GooU6nRGW4qHOSYQVMvsEPDkki4a1qYP_nrNaULi145rHCqqeBT41oFApVY8yT0GeiLwjX_fDjae__biEdwE_D6aGPrrjkP0_8XmNHR0kdgg_zNx";

		$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

		$downstreamResponse->numberSuccess();
		$downstreamResponse->numberFailure();
		$downstreamResponse->numberModification();

		//return Array - you must remove all this tokens in your database
		$downstreamResponse->tokensToDelete();

		//return Array (key : oldToken, value : new token - you must change the token in your database )
		$downstreamResponse->tokensToModify();

		//return Array - you should try to resend the message to the tokens in the array
		$downstreamResponse->tokensToRetry();

		// return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }
}
