<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Package;
use App\bookPackage;
use Redirect;
use Hash;
use Auth;
use App\wallet_transaction;
use App\CarWashService;
use DB;
use StdClass;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     return view('home');
    // }

    public function home()
    {
        $user_data = auth()->user();
        $user_id = $user_data->id;
        $credit = wallet_transaction::where('user_id', $user_id)
                                ->where('transaction_type', 'credit')
                                ->sum('amount');
        $debit = wallet_transaction::where('user_id', $user_id)
                                ->where('transaction_type', 'debit')
                                ->sum('amount');
        $amount = $credit - $debit;
        $carwasherquest = CarWashService::join('brands', 'brands.id', '=', 'brand_id')->join('carmodels', 'carmodels.id', '=', 'model_id')->join('packages', 'packages.id', '=', 'package_id')->where('car_wash_services.user_id', $user_id)->get();
        
        return view('frontend/dashboard',compact('user_data','amount', 'carwasherquest'));
    }


    public function user_profile(Request $request)
    {
        $user_data = auth()->user();
        $user_id = $user_data->id;
        $user = User::find($user_id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->mobile = $request->phone;
        $user->update();
        return Redirect::back()->withErrors(['message', 'Data Successfully Updated']);
    }

    public function changePassword(Request $request){
            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("message","Your current password does not matches with the password you provided. Please try again.");
            }
            if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("message","New Password cannot be same as your current password. Please choose a different password.");
            }
            $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required',
            ]);
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();
            return redirect()->back()->with("message","Password changed successfully !");
    }

    public function bookPackage($pack_id,$model_id){

        $package_data = DB::table('packages')
            ->where('id',$pack_id)
            ->first();
        $car_data = DB::table('carmodels')
                    ->join('brands', 'carmodels.brand_id', '=' , 'brands.id')
                    ->select('carmodels.id','carmodels.model_name','carmodels.brand_id','carmodels.type','carmodels.model_img','carmodels.model_desc','carmodels.status','brands.brand_name','brands.id','brands.description','brands.brand_img')
                    ->where('carmodels.id',$model_id)
                    ->first();
                        
        return view('frontend/package_book', compact('package_data','car_data')); 
        
    }





     public function done_booking(Request $request)
    {
        $user_data = auth()->user();
        $user_id = $user_data->id;
        $booking = new bookPackage;
        $booking->user_id = $user_data->id;
        $booking->packageid = $request->package_id;
        $booking->modelid = $request->model_id;
        $booking->brandid = $request->brand_id;
        $booking->price = $request->package_price;
        $booking->address = $request->packageaddress;
        $booking->timeperiod = $request->period;
        $booking->numberplate = $request->numberPlate;
        // dd($booking);
        $booking->save();

         $credit = wallet_transaction::where('user_id', $user_id)
                                ->where('transaction_type', 'credit')
                                ->sum('amount');
        $debit = wallet_transaction::where('user_id', $user_id)
                                ->where('transaction_type', 'debit')
                                ->sum('amount');
        $amount = $credit - $debit;
        $price = $request->package_price;
        $date           = date('y-m-d h:i:s');
            $pay_data = new StdClass;
            $pay_data->firstname = $user_data->id;
            $pay_data->email = $user_data->email;
            $pay_data->phone = $request->mobile;
            $pay_data->productinfo = $request->package_id;
         if ($amount>=$price){


            $Transaction = new wallet_transaction();
            $Transaction->user_id               = $user_data->id;
            $Transaction->amount                = $price;
            $Transaction->transaction_type      = "debit";
            $Transaction->status                = 1;
            $Transaction->date                  = $date;
            $Transaction->sent_to               = "wallet transaction";
            $Transaction->save();
            return redirect('dashboard');
        }
      else 
      {
            $Transaction = new wallet_transaction();
            $Transaction->user_id               = $user_data->id;
            $Transaction->amount                = $price;
            $Transaction->transaction_type      = "debit";
            $Transaction->status                = 1;
            $Transaction->date                  = $date;
            $Transaction->sent_to               = "wallet transaction";
            $Transaction->save();
            $pay_data->amount = $price-$amount;
            return view('frontend/payment', compact('pay_data')); 

      }
    }
}
