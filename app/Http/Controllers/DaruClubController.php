<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DaruClubController extends Controller
{
    public function index() {
    	return view('home.index');
    }

    public function aboutUs() {
    	return view('home.about');
    }

    public function contactUs() {
    	return view('home.contact');
    }

    public function getGallery() {
    	return view('home.gallery');
    }

    public function getPartners() {
    	return view('home.partner');
    }

    public function getFaq() {
    	return view('home.faq');
    }

    public function getSingle() {
        return view('home.single');
    }
}
