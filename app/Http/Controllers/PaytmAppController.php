<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
define('PAYTM_MERCHANT_KEY', 'bN5vXT1gSZH9ptUk');

class PaytmAppController extends Controller
{
	function encrypt_e($input, $ky) {
		$iv = "@@@@&&&&####$$$$";
	    $data = openssl_encrypt ( $input , "AES-128-CBC" , $ky, 0, $iv );
	    return $data;
	}

	function decrypt_e($crypt, $ky) {

		$iv = "@@@@&&&&####$$$$";
	    $data = openssl_decrypt ( $crypt , "AES-128-CBC" , $ky, 0, $iv );
	    return $data;
	}

	function pkcs5_pad_e($text, $blocksize) {
		$pad = $blocksize - (strlen($text) % $blocksize);
		return $text . str_repeat(chr($pad), $pad);
	}

	function pkcs5_unpad_e($text) {
		$pad = ord($text{strlen($text) - 1});
		if ($pad > strlen($text))
			return false;
		return substr($text, 0, -1 * $pad);
	}

	function generateSalt_e($length) {
		$random = "";
		srand((double) microtime() * 1000000);

		$data = "AbcDE123IJKLMN67QRSTUVWXYZ";
		$data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
		$data .= "0FGH45OP89";

		for ($i = 0; $i < $length; $i++) {
			$random .= substr($data, (rand() % (strlen($data))), 1);
		}

		return $random;
	}

	function checkString_e($value) {
		if ($value == 'null')
			$value = '';
		return $value;
	}

	function getChecksumFromArray($arrayList, $key, $sort=1) {
		if ($sort != 0) {
			ksort($arrayList);
		}
		$str = $this->getArray2Str($arrayList);
		$salt = $this->generateSalt_e(4);
		$finalString = $str . "|" . $salt;
		$hash = hash("sha256", $finalString);
		$hashString = $hash . $salt;
		$checksum = $this->encrypt_e($hashString, $key);
		return $checksum;
	}
	function getChecksumFromString($str, $key) {
		
		$salt = $this->generateSalt_e(4);
		$finalString = $str . "|" . $salt;
		$hash = hash("sha256", $finalString);
		$hashString = $hash . $salt;
		$checksum = $this->encrypt_e($hashString, $key);
		return $checksum;
	}

	function verifychecksum_e($arrayList, $key, $checksumvalue) {
		$arrayList = removeCheckSumParam($arrayList);
		ksort($arrayList);
		$str = getArray2StrForVerify($arrayList);
		$paytm_hash = decrypt_e($checksumvalue, $key);
		$salt = substr($paytm_hash, -4);

		$finalString = $str . "|" . $salt;

		$website_hash = hash("sha256", $finalString);
		$website_hash .= $salt;

		$validFlag = "FALSE";
		if ($website_hash == $paytm_hash) {
			$validFlag = "TRUE";
		} else {
			$validFlag = "FALSE";
		}
		return $validFlag;
	}

	function verifychecksum_eFromStr($str, $key, $checksumvalue) {
		$paytm_hash = decrypt_e($checksumvalue, $key);
		$salt = substr($paytm_hash, -4);

		$finalString = $str . "|" . $salt;

		$website_hash = hash("sha256", $finalString);
		$website_hash .= $salt;

		$validFlag = "FALSE";
		if ($website_hash == $paytm_hash) {
			$validFlag = "TRUE";
		} else {
			$validFlag = "FALSE";
		}
		return $validFlag;
	}

	function getArray2Str($arrayList) {
		$findme   = 'REFUND';
		$findmepipe = '|';
		$paramStr = "";
		$flag = 1;	
		foreach ($arrayList as $key => $value) {
			$pos = strpos($value, $findme);
			$pospipe = strpos($value, $findmepipe);
			if ($pos !== false || $pospipe !== false) 
			{
				continue;
			}
			
			if ($flag) {
				$paramStr .= $this->checkString_e($value);
				$flag = 0;
			} else {
				$paramStr .= "|" . $this->checkString_e($value);
			}
		}
		return $paramStr;
	}

	function getArray2StrForVerify($arrayList) {
		$paramStr = "";
		$flag = 1;
		foreach ($arrayList as $key => $value) {
			if ($flag) {
				$paramStr .= $this->checkString_e($value);
				$flag = 0;
			} else {
				$paramStr .= "|" . $this->checkString_e($value);
			}
		}
		return $paramStr;
	}

	function redirect2PG($paramList, $key) {
		$hashString = $this->getchecksumFromArray($paramList);
		$checksum = $this->encrypt_e($hashString, $key);
	}

	function removeCheckSumParam($arrayList) {
		if (isset($arrayList["CHECKSUMHASH"])) {
			unset($arrayList["CHECKSUMHASH"]);
		}
		return $arrayList;
	}

	function getTxnStatus($requestParamList) {
		return callAPI(PAYTM_STATUS_QUERY_URL, $requestParamList);
	}

	function getTxnStatusNew($requestParamList) {
		return callNewAPI(PAYTM_STATUS_QUERY_NEW_URL, $requestParamList);
	}

	function initiateTxnRefund($requestParamList) {
		$CHECKSUM = $this->getChecksumFromArray($requestParamList,PAYTM_MERCHANT_KEY,0);
		$requestParamList["CHECKSUM"] = $CHECKSUM;
		return callAPI(PAYTM_REFUND_URL, $requestParamList);
	}

	function callAPI($apiURL, $requestParamList) {
		$jsonResponse = "";
		$responseParamList = array();
		$JsonData =json_encode($requestParamList);
		$postData = 'JsonData='.urlencode($JsonData);
		$ch = curl_init($apiURL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
		'Content-Type: application/json', 
		'Content-Length: ' . strlen($postData))                                                                       
		);  
		$jsonResponse = curl_exec($ch);   
		$responseParamList = json_decode($jsonResponse,true);
		return $responseParamList;
	}

	function callNewAPI($apiURL, $requestParamList) {
		$jsonResponse = "";
		$responseParamList = array();
		$JsonData =json_encode($requestParamList);
		$postData = 'JsonData='.urlencode($JsonData);
		$ch = curl_init($apiURL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
		'Content-Type: application/json', 
		'Content-Length: ' . strlen($postData))                                                                       
		);  
		$jsonResponse = curl_exec($ch);   
		$responseParamList = json_decode($jsonResponse,true);
		return $responseParamList;
	}
	function getRefundChecksumFromArray($arrayList, $key, $sort=1) {
		if ($sort != 0) {
			ksort($arrayList);
		}
		$str = getRefundArray2Str($arrayList);
		$salt = generateSalt_e(4);
		$finalString = $str . "|" . $salt;
		$hash = hash("sha256", $finalString);
		$hashString = $hash . $salt;
		$checksum = $this->encrypt_e($hashString, $key);
		return $checksum;
	}
	function getRefundArray2Str($arrayList) {	
		$findmepipe = '|';
		$paramStr = "";
		$flag = 1;	
		foreach ($arrayList as $key => $value) {		
			$pospipe = strpos($value, $findmepipe);
			if ($pospipe !== false) 
			{
				continue;
			}
			
			if ($flag) {
				$paramStr .= $this->checkString_e($value);
				$flag = 0;
			} else {
				$paramStr .= "|" . $this->checkString_e($value);
			}
		}
		return $paramStr;
	}
	function callRefundAPI($refundApiURL, $requestParamList) {
		$jsonResponse = "";
		$responseParamList = array();
		$JsonData =json_encode($requestParamList);
		$postData = 'JsonData='.urlencode($JsonData);
		$ch = curl_init($apiURL);	
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, $refundApiURL);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
		$jsonResponse = curl_exec($ch);   
		$responseParamList = json_decode($jsonResponse,true);
		return $responseParamList;
	}


	public function generatechecksum(Request $request)
	{
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");
		// following files need to be included
		$checkSum = "";

		// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
		$findme   = 'REFUND';
		$findmepipe = '|';

		$paramList = array();

		$paramList["MID"] = '';
		$paramList["ORDER_ID"] = '';
		$paramList["CUST_ID"] = '';
		$paramList["INDUSTRY_TYPE_ID"] = '';
		$paramList["CHANNEL_ID"] = '';
		$paramList["TXN_AMOUNT"] = '';
		$paramList["WEBSITE"] = '';
		$paramList["CALLBACK_URL"] = '';
		foreach($_POST as $key=>$value)
		{
			if ($key=='token'){
				continue;
			}
		  $pos = strpos($value, $findme);
		  $pospipe = strpos($value, $findmepipe);
		  if ($pos === false || $pospipe === false)
		    {
		        $paramList[$key] = $value;
		    }
		}



		//Here checksum string will return by $this->getChecksumFromArray() function.
		$checkSum = $this->getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
		//print_r($_POST);
		 echo json_encode(array("CHECKSUMHASH" => $checkSum,"ORDER_ID" => $_POST["ORDER_ID"], "payt_STATUS" => "1"));
		  //Sample response return to SDK

		//  {"CHECKSUMHASH":"GhAJV057opOCD3KJuVWesQ9pUxMtyUGLPAiIRtkEQXBeSws2hYvxaj7jRn33rTYGRLx2TosFkgReyCslu4OUj\/A85AvNC6E4wUP+CZnrBGM=","ORDER_ID":"asgasfgasfsdfhl7","payt_STATUS":"1"}
	}


	public function verifychecksum(Request $request)
	{
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");

	
		$paytmChecksum = "";
		$paramList = array();
		$isValidChecksum = FALSE;

		$paramList = $_POST;
		$return_array = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

		//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
		$isValidChecksum = $this->verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

		// if ($isValidChecksum===TRUE)
		// 	$return_array["IS_CHECKSUM_VALID"] = "Y";
		// else
		// 	$return_array["IS_CHECKSUM_VALID"] = "N";

		$return_array["IS_CHECKSUM_VALID"] = $isValidChecksum ? "Y" : "N";
		//$return_array["TXNTYPE"] = "";
		//$return_array["REFUNDAMT"] = "";
		unset($return_array["CHECKSUMHASH"]);

		$encoded_json = htmlentities(json_encode($return_array));

	

		echo '<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-I">
			<title>Paytm</title>
			<script type="text/javascript">
				function response(){
					return document.getElementById("response").value;
				}
			</script>
		</head>
		<body>
		  Redirect back to the app<br>

		  <form name="frm" method="post">
		    <input type="hidden" id="response" name="responseField" value="'.$encoded_json.'">
		  </form>
		</body>
		</html>';

	}


	public function verifytransaction(Request $request)
	{
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");


		$findme   = 'REFUND';
		$findmepipe = '|';

		$ORDER_ID = "";
		$requestParamList = array();
		$responseParamList = array();

		//$requestParamList = array("MID" => 'MyPicB67978471273555' , "ORDERID" => "MPB865");
		$requestParamList["MID"] = '';
		$requestParamList["ORDER_ID"] = '';

		foreach($_POST as $key=>$value)
		{
			if ($key=='token'){
				continue;
			}
		  $pos = strpos($value, $findme);
		  $pospipe = strpos($value, $findmepipe);
		  if ($pos === false || $pospipe === false)
		    {
		        $requestParamList[$key] = $value;
		    }
		}

		$checkSum = $this->getChecksumFromArray($requestParamList,PAYTM_MERCHANT_KEY);
		$requestParamList['CHECKSUMHASH'] = urlencode($checkSum);

		$data_string = "JsonData=".json_encode($requestParamList);
		//echo $data_string;


		$ch = curl_init();                    // initiate curl
		//$url = "https://securegw-stage.paytm.in/merchant-status/getTxnStatus?"; // where you want to post data  pg+ testing
		//$url = "https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus";

		//$url = "https://secure.paytm.in/oltp/HANDLER_INTERNAL/getTxnStatus?"; // where you want to post data PG live
		$url = "https://securegw.paytm.in/merchant-status/getTxnStatus";      //   pg+ live
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string); // define what you want to post
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$output = curl_exec($ch); // execute
		$info = curl_getinfo($ch);

		//echo "kkk".$output;
		$data = json_decode($output, true);
		//echo "<pre>";
		echo json_encode($data);
		//echo "</pre>";

	}
}
