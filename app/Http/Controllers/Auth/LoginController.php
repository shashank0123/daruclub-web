<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

   use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected function redirectTo()
    {
        url()->previous();
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }
        return view('auth.login');    
    }

    public function login(Request $request)
    {
        $next = session()->get('url.intended');
            
        if ( Auth::attempt([
            'mobile' => $request->mobile,
            'password' => $request->password
            ],$request->has('remember'))){
            if(session()->has('url.intended'))
            {
                echo $next = session()->get('url.intended');
            }
            if (Auth::user()->user_type == 'admin')
                    return redirect('admin/home');
            elseif (Auth::user()->user_type == 'user'){
                // var_dump(auth()->user());
                // die();
                return redirect($next);
            }
            else
                return redirect('/login');

        }
        else
            return redirect('login')->withErrors([
                                'credentials' => 'Please, check your credentials'
                            ]);
           

     
    
     }




    public function loginAdmin(Request $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'user_type' => 'admin'])) {
            // Authentication passed...
            return redirect()->intended('admin/dashboard');
        }
    }
}
