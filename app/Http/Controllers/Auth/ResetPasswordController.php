<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\User;
use Hash;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function postReset(Request $request)
    {
        $id = $request->mobile;
        $mobile = $request->mobile;
        $user = User::where('mobile', $id)->first();
        $randphone = rand(100000,999999);
        $user->remember_token = $randphone;
        $token = Hash::make($randphone);
        $user->update(); 
        $message = "Your%20OTP%20is%20$randphone.%20Please%20use%20this%20otp%20to%20reset%20your%20password.";
        $url = "http://103.16.101.52:8080/sendsms/bulksms?username=bcks-imzhnd&password=Super123&type=0&dlr=1&destination=$mobile&source=BSSPLI&message='$message'";
        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c,CURLOPT_HTTPGET ,1);
         
        curl_setopt($c, CURLOPT_URL, $url);
        $contents = curl_exec($c);
        if (curl_errno($c)) {
            echo 'Curl error: ' . curl_error($c);
        }else{
            curl_close($c);
        }

            
        return view('auth.passwords.reset', compact('token'));
    }

    public function updatePassword(Request $request)
    {
        $user = User::where('remember_token', $request->otp)->first();
        if ($user){
            $user->password = Hash::make($request->password);
            $user->update();
            return redirect('/login');
        }
        else{
            return redirect('password/reset');            
        }
    }

    

}
