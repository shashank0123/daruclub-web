<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\wallet_transaction;
use App\wallet_money;
use App\User;
use Hash;
use Auth;
use StdClass;

class WalletController extends Controller
{


     public function getBalance(Request $request)
    {
        $user_data = auth()->user();
        $user_id = $user_data->id;
        if ($user_id)
        {
            $credit = wallet_transaction::where('user_id', $user_id)
                                    ->where('transaction_type', 'credit')
                                    ->sum('amount');
            $debit = wallet_transaction::where('user_id', $user_id)
                                    ->where('transaction_type', 'debit')
                                    ->sum('amount');
            $amount = $credit - $debit;
            $status = 200;
            $message = "data received";
        }
        else{
            $message = "Please send the user id";
            $status = 400;
        }

        $data = new StdClass;
        $data->status = $status;
        $data->amount = $amount;
        $data->message = $message;


        return response()->json($data);
    }

    public function getTransactions(Request $request)
    {
        $id = $request->user()->id;
        $message = "";
        $amount = 0;
        if ($id)
        {
            $transaction = wallet_transaction::where('user_id', $id)
                                    ->get();
            $status = 200;
            $message = "data received";
        }
        else{
            $message = "Please send the user id";
            $status = 400;
        }

        $data = new StdClass;
        $data->status = $status;
        $data->amount = $transaction;
        $data->message = $message;


        return response()->json($data);
    }

    public function payBalance(Request $request)
    {
        $id = $request->user()->id;
        $payment = $request->money;
        $sent_to = $request->send_to;
        $date = $request->date;
        $message = "";
        $amount = 0;
        if ($id)
        {
            $credit = wallet_transaction::where('user_id', $id)
                                    ->where('transaction_type', 'credit')
                                    ->sum('amount');
            $debit = wallet_transaction::where('user_id', $id)
                                    ->where('transaction_type', 'debit')
                                    ->sum('amount');
            $amount = $credit - $debit;
            if ($amount>=$payment){
                $Transaction = new wallet_transaction();
                $Transaction->user_id               = $id;
                $Transaction->amount                = $payment;
                $Transaction->transaction_type      = "debit";
                $Transaction->status                = 1;
                $Transaction->date                  = $date;
                $Transaction->sent_to               = '9999999';
                $Transaction->save();
                $status = 200;
                if ($Transaction)
                    $message = "amount debited";
                else
                    $message = "amount not sent";
                
            }
            else {
                // $amount 
            }
        }
        else{
            $message = "Please send the user id";
            $status = 400;
        }

        $data = new StdClass;
        $data->status = $status;
        $data->amount = $amount;
        $data->message = $message;


        return response()->json($data);
    }


    public function addBalance(Request $request)
    {
        $user_data = auth()->user();
        $user_id = $user_data->id;
        $email = $user_data->email;
        $phone = $user_data->phone;
        $gateway        = "payumoney";
        $status         = "pending";
        $remark         = "Add Money to Wallet";
        $transaction_id = Hash::make(str_random(8));
        $amount         = $request->amount;
        $date           = date('y-m-d h:i:s');
        $message = "";
        $wallets = "";
        // $data = new StdClass;
        if ($user_id)
        {
            $Wallet = new wallet_money();
            $Wallet->user_id        = $user_id;
            $Wallet->gateway        = $gateway;
            $Wallet->status         = $status;
            $Wallet->remark         = $remark;
            $Wallet->transaction_id = $transaction_id;
            $Wallet->amount         = $amount;
            $Wallet->date           = $date;
            $Wallet->save();

            $pay_data->firstname = $user_id;
            $pay_data->amount = $amount;
            $pay_data->email = $email;
            $pay_data->phone = $phone;
            $pay_data->productinfo = $remark;
            dd($pay_data);

            return view('frontend/payment', compact('pay_data')); 
}
        //     $message = "Data saved Successfully";
        //     $data->message = $message;
        //     $status = 200;

        //     if ($Wallet){
        //         $Transaction = new wallet_transaction();
        //         $Transaction->user_id               = $user_id;
        //         $Transaction->amount                = $amount;
        //         $Transaction->transaction_type      = "credit";
        //         $Transaction->status                = 1;
        //         $Transaction->date                  = $date;
        //         $Transaction->save();
        //     }
        // }
        // else{
        //     $message = "Please send the user id";
        //     $status = 400;
        // }

        // $data->status = $status;
        // $data->wallets = $Wallet;
        // $data->message = $message;


        // return response()->json($data);
    }


    function getHashes($txnid, $amount, $productinfo, $firstname, $email, $user_credentials, $udf1, $udf2, $udf3, $udf4, $udf5)
    {
        // $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
        $key = 'zhr8UvQn';
        $salt = 'PSz3qDmg8a';
     
        $payhash_str = $key . '|' . $this->checkNull($txnid) . '|' .$this->checkNull($amount) . '|' .$this->checkNull($productinfo) . '|' . $this->checkNull($firstname) . '|' . $this->checkNull($email) . '|' . $this->checkNull($udf1) . '|' . $this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '||||||' . $salt;
        $paymentHash = strtolower(hash('sha512', $payhash_str));
        $arr['payment_hash'] = $paymentHash;

        $cmnNameMerchantCodes = 'get_merchant_ibibo_codes';
        $merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt ;
        $merchantCodesHash = strtolower(hash('sha512', $merchantCodesHash_str));
        $arr['get_merchant_ibibo_codes_hash'] = $merchantCodesHash;

        $cmnMobileSdk = 'vas_for_mobile_sdk';
        $mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
        $mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
        $arr['vas_for_mobile_sdk_hash'] = $mobileSdk;

        $cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
        $detailsForMobileSdk_str1 = $key . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt ;
        $detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
        $arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1;

        //used for verifying payment(optional) 
        $cmnVerifyPayment = 'verify_payment';
        $verifyPayment_str = $key . '|' . $cmnVerifyPayment . '|'.$txnid .'|' . $salt;
        $verifyPayment = strtolower(hash('sha512', $verifyPayment_str));
        $arr['verify_payment_hash'] = $verifyPayment;

        if($user_credentials != NULL & $user_credentials != '')
        {
            $cmnNameDeleteCard = 'delete_user_card';
            $deleteHash_str = $key . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt ;
            $deleteHash = strtolower(hash('sha512', $deleteHash_str));
            $arr['delete_user_card_hash'] = $deleteHash;

            $cmnNameGetUserCard = 'get_user_cards';
            $getUserCardHash_str = $key . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt ;
            $getUserCardHash = strtolower(hash('sha512', $getUserCardHash_str));
            $arr['get_user_cards_hash'] = $getUserCardHash;

            $cmnNameEditUserCard = 'edit_user_card';
            $editUserCardHash_str = $key . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt ;
            $editUserCardHash = strtolower(hash('sha512', $editUserCardHash_str));
            $arr['edit_user_card_hash'] = $editUserCardHash;

            $cmnNameSaveUserCard = 'save_user_card';
            $saveUserCardHash_str = $key . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt ;
            $saveUserCardHash = strtolower(hash('sha512', $saveUserCardHash_str));
            $arr['save_user_card_hash'] = $saveUserCardHash;

            $cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
            $detailsForMobileSdk_str = $key . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt ;
            $detailsForMobileSdk = strtolower(hash('sha512', $detailsForMobileSdk_str));
            $arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk;
        }


        // if($udf3!=NULL &amp;&amp; !empty($udf3)){
        $cmnSend_Sms='send_sms';
        $sendsms_str=$key . '|' . $cmnSend_Sms . '|' . $udf3 . '|' . $salt;
        $send_sms = strtolower(hash('sha512',$sendsms_str));
        $arr['send_sms_hash']=$send_sms;
        // }

        return $arr;
    }

    function checkNull($value) {
        if ($value == null) {
            return '';
        } else {
            return $value;
        }
    }

    public function createHash(Request $request)
    {
        $output=$this->getHashes($request->txnid, $request->amount, $request->productinfo, $request->firstname, $request->email, $request->user_credentials, $request->udf1, $request->udf2, $request->udf3, $request->udf4, $request->udf5);
        return response()->json($output);
    }


    public function getBill(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        
        $booking = new StdClass;
        $booking->amount = 200;
        $booking->email = $request->user()->mobile.'@daru.club';
        $booking->productinfo = 'daruclub bill';
        $booking->firstname = $request->user()->name;
        $booking->user_credentials = $request->user()->id;


        if ($booking){
            $response->bill = $booking;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }
}
