<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\CarWashService;
use DB;
use App\Profile;
use Image;
use StdClass;
use Hash;
use File;
use App\MyCar;
use App\MyBike;
use App\AppCallbacks;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('users'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name','id');
        return view('users.create',compact('roles')); //return the view with the list of roles passed as an array
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'roles' => 'required'
        ]);
        $input = $request->only('name', 'email', 'password');
        $input['password'] = Hash::make($input['password']); //Hash password
        $user = User::create($input); //Create User table entry
        //Attach the selected Roles
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }
        return redirect()->route('users.index')
            ->with('success','User created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get(); //get all roles
        $userRoles = $user->roles->pluck('id')->toArray();
        return view('users.edit',compact('user','roles','userRoles'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'confirmed',
            'roles' => 'required'
        ]);
        $input = $request->only('name', 'email', 'password');
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']); //update the password
        }else{
            $input = array_except($input,array('password')); //remove password from the input array
        }
        $user = User::find($id);
        $user->update($input); //update the user info
        //delete all roles currently linked to this user
        DB::table('role_user')->where('user_id',$id)->delete();
        //attach the new roles to the user
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }
        return redirect()->route('users.index')
            ->with('success','User updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success','User deleted successfully');
    }


    public function profileinfo(Request $request)
    {
        $id = $request->user()->id;
        $data = new StdClass;
        $data->status = 400;
        if ($id){
            $userdata = User::where('id', $id)
                            ->first(); 
            $user = new StdClass;
            $user->firstname = $userdata->firstname;
            $user->lastname = $userdata->lastname;
            $user->email = $userdata->email;
            $userProfileData = Profile::where('user_id', $id)->first();
            $user->dob="";
            $user->gender="";
            $user->profile_pic="/profile_pic/profile.png";
            $user->mobile = "";
            $user->profession="";
            $user->email_verification = $userdata->email_verification;
            $user->mobile_verification = $userdata->mobile_verification;           

            if ($userProfileData){
                $user->dob = $userProfileData->dob;
                $user->gender = $userProfileData->gender;
                $user->profile_pic = $userProfileData->profile_pic;
                $user->mobile = $userdata->mobile;
                $user->profession = $userProfileData->profession;
            }
            if ($user->profile_pic == null)
                $user->profile_pic="/profile_pic/profile.png";
            $data->profile = $user;
            $data->status = 200;
        }
        return response()->json($data);
    }


    public function editprofileinfo(Request $request)
    {
        $id = $request->user()->id;
        $data = new StdClass;
        $status = 400;

        //this is a test comment 

        if ($id){
            $userdata = User::where('id', $id)
                            ->update(['firstname' => $request->firstname, 'lastname' => $request->lastname, 'mobile' => $request->mobile ]); 


            if($request->hasFile('profile_pic')){
                $profile_pic = $request->file('profile_pic');
                $filename = '/profile_pic/'.$id."/". time() . '.' . $profile_pic->getClientOriginalExtension();
                Image::make($profile_pic)->resize(300, 300)->save( public_path($filename));
     
                
            }
            else $filename = '/profile_pic/profile.png';
            $profile = Profile::where('user_id', $id)->first();
            if ($profile){
                if (!isset($filename)){
                    $filename = $profile->profile_pic;
                }
                $userProfileData = Profile::where('user_id', $id)->
                                        update([
                                                'dob' => $request->dob,
                                                'gender' => $request->gender,
                                                'profile_pic' => $filename,
                                                'profession' => $request->profession
                                            ]);
           }
           else 
                $userProfileData = Profile::where('user_id', $id)->
                                        insert([
                                                'dob' => $request->dob,
                                                'user_id' => $request->user()->id,
                                                'gender' => $request->gender,
                                                'profession' => $request->profession
                                            ]);
            

           
           if ($userdata && $userProfileData){
                $response = new StdClass;
                $response->id = $request->user()->id;
                $response->firstname = $request->firstname;
                $response->lastname = $request->lastname;
                $response->dob = $request->dob;
                $response->gender = $request->gender;
                $response->profile_pic = $request->profile_pic;
                $response->mobile = $request->mobile;
                $response->profession = $request->profession;
                $status = 200;
                $data->data = $response;
                $message = "Profile info saved";
            }

            else{
                $message = "Profile info not saved";
            }
            $data->status = $status;
            $data->message = $message;
        }
    return response()->json($data);
    }


    public function addMyCar(Request $request){
        $user_id = $request->user()->id;
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong!!!";
        $validatedData = $request->validate([
            'brand_id'        => 'required',
            'model_id'        => 'required',
               
            ]);

        

        $mycar = new MyCar;
        $mycar->car_brand      = $request->brand_id;
        $mycar->car_model      = $request->model_id;
        $mycar->user_id        = $user_id;        
        $mycar->vehicle_no        = $request->vehicle_no;        
        $mycar->save();


        if ($mycar){
                $response->mycar = $mycar;
                $status = 200;
                $message = "Car information saved Successfully";

        }   

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);     
 }

 public function editMyCar(Request $request){
        $user_id = $request->user()->id;
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong!!!";
        $validatedData = $request->validate([
            'brand_id'        => 'required',
            'model_id'        => 'required',
            ]);

        

        $mycar = MyCar::where('user_id', $user_id)->where('id', $request->car_id)->first();
        $mycar->car_brand      = $request->brand_id;
        $mycar->car_model      = $request->model_id;
        $mycar->user_id        = $user_id;        
        $mycar->vehicle_no      = $request->vehicle_no;
        $mycar->update();


        if ($mycar){
                $response->mycar = $mycar;
                $status = 200;
                $message = "Car information saved Successfully";

        }   

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);     
 }  
 public function deleteMyCar(Request $request){
        $user_id = $request->user()->id;
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong!!!";
        
        

        $mycar = MyCar::where('user_id', $user_id)->where('id', $request->car_id)->delete();
        // $mycar->update();


        if ($mycar){
                $response->mycar = $mycar;
                $status = 200;
                $message = "Car information deleted Successfully";

        }   

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);     
 }

 public function viewMycar(Request $request)
 {
    $response = new StdClass;
    $status = 400;
    $message = "Something Went Wrong!!!";
    $user_id = $request->user()->id;
    $mycar = MyCar::join('carmodels', 'carmodels.id', '=', 'my_cars.car_model')->join('brands', 'brands.id', '=', 'my_cars.car_brand')->select('my_cars.id as car_id','my_cars.*', 'carmodels.*', 'brands.*')->where('my_cars.user_id', $user_id)->get();
    $mybike = MyBike::join('carmodels', 'carmodels.id', '=', 'my_bikes.bike_model')->join('brands', 'brands.id', '=', 'my_bikes.bike_brand')->select('my_bikes.id as bike_id','my_bikes.*', 'carmodels.*', 'brands.*')->where('my_bikes.user_id', $user_id)->get();
    if ($mycar){
        $status = 200;
        $message = 'Data Processed';
        $response->my_car = $mycar;
    }
    if ($mybike){
        $status = 200;
        $message = 'Data Processed';
        $response->my_bike = $mybike;
    }
    $response->status = $status;
    $response->message = $message;
    return response()->json($response);

 }
    public function request_callback(Request $request){
        $user_id = $request->user()->id;
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong!!!";
        $validatedData = $request->validate([
            'car_brand'        => 'required',
            'car_model'        => 'required',
            'fuel_type'        => 'required',   
            ]);

        

        $mycar = new AppCallbacks;
        $mycar->car_brand      = $request->car_brand;
        $mycar->car_model      = $request->car_model;
        $mycar->fuel_type      = $request->fuel_type;
        $mycar->description      = $request->description;
        $mycar->enquiry_for      = $request->enquiry_for;
        $mycar->user_id        = $user_id;
        $mycar->save();


        if ($mycar){
                $response->callback = $mycar;
                $status = 200;
                $message = "Car information saved Successfully";

        }   

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);     
 }  

  public function profilepic(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something went wrong";
         $id = $request->user()->id;
        if($request->hasFile('profile_pic')){
            $directoryName = '/profile_pic/'.$id;
            $profile_pic = $request->file('profile_pic');
            if(!is_dir($directoryName)){
                //Directory does not exist, so lets create it.
                $result = File::makeDirectory(public_path($directoryName), 0777, true, true);
            }
            $filename = $directoryName."/". time() . '.' . $profile_pic->getClientOriginalExtension();
            Image::make($profile_pic)->resize(300, 300)->save( public_path($filename));
            $status = 400;
            $message = "profile pic saved";
            $profile = Profile::where('user_id', $request->user()->id)
                                ->update([
                                   'profile_pic' => $filename,                                                
                                   ]);
                
        }
        else 
        {
            $status = 400;
            $message = "upload an image";
        }
        $response->status = $status;
        $response->message = $message;

        return response()->json($response);
        
    }


 


 public function viewMyorders(Request $request)
 {
    $response = new StdClass;
    $status = 400;
    $message = "Something Went Wrong!!!";
    $user_id = $request->user()->id;
    $mycar = CarWashService::where('user_id', $user_id)->where('payment_status', '!=', 'Pay Later')->get();
    $mycarpaylater = CarWashService::where('user_id', $user_id)->where('payment_status', 'Pay Later')->get();
    if ($mycar){
        $status = 200;
        $message = 'Data Processed';
        $response->orders = $mycar;
        $response->later = $mycarpaylater;
    }
    $response->status = $status;
    $response->message = $message;
    return response()->json($response);

 }

 public function viewPendingOrder(Request $request)
 {
    $response = new StdClass;
    $status = 400;
    $message = "Something Went Wrong!!!";
    $user_id = $request->user()->id;
    $mycarpaylater = CarWashService::where('user_id', $user_id)->where('payment_status', 'Pay Later')->get();
    if ($mycarpaylater){
        $status = 200;
        $message = 'Data Processed';
        $response->later = $mycarpaylater;
    }
    $response->status = $status;
    $response->message = $message;
    return response()->json($response);

 }


 // for Bike

 public function addMyBike(Request $request){
        $user_id = $request->user()->id;
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong!!!";
        $validatedData = $request->validate([
            'brand_id'        => 'required',
            'model_id'        => 'required',
               
            ]);

        

        $mybike = new MyBike;
        $mybike->bike_brand      = $request->brand_id;
        $mybike->bike_model      = $request->model_id;
        $mybike->user_id        = $user_id;        
        $mybike->vehicle_no        = $request->vehicle_no;        
        $mybike->save();


        if ($mybike){
                $response->mybike = $mybike;
                $status = 200;
                $message = "Bike information saved Successfully";

        }   

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);     
 }

 public function editMyBike(Request $request){
        $user_id = $request->user()->id;
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong!!!";
        $validatedData = $request->validate([
            'brand_id'        => 'required',
            'model_id'        => 'required',
            ]);

        

        $mybike = MyBike::where('user_id', $user_id)->where('id', $request->bike_id)->first();
        $mybike->bike_brand      = $request->brand_id;
        $mybike->bike_model      = $request->model_id;
        $mybike->user_id        = $user_id;        
        $mybike->vehicle_no      = $request->vehicle_no;
        $mybike->update();


        if ($mybike){
                $response->mybike = $mybike;
                $status = 200;
                $message = "Bike information saved Successfully";

        }   

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);     
 }  
 public function deleteMyBike(Request $request){
        $user_id = $request->user()->id;
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong!!!";
        
        

        $mybike = MyBike::where('user_id', $user_id)->where('id', $request->bike_id)->delete();
        // $mycar->update();


        if ($mybike){
                $response->mybike = $mybike;
                $status = 200;
                $message = "Bike information deleted Successfully";

        }   

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);     
 }

}
