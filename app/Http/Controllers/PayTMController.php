<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PaytmWallet;
use App\EventRegistration;
use App\User;
use App\CarWashService;

class PayTMController extends Controller
{
    public function order(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            'mobile_no' => 'required|numeric|digits:10|unique:event_registration,mobile_no',
            'address' => 'required',
            'email' => 'required',
        ]);


        $input = $request->all();
        $input['order_id'] = $request->mobile_no.rand(1,100);
        $input['fee'] = $request->fee;


        EventRegistration::create($input);


        $payment = PaytmWallet::with('receive');
        $payment->prepare([
          'order' => $input['order_id'],
          'user' => $request->name,
          'mobile_number' => $request->mobile_no,
          'email' => $request->email,
          'amount' => $input['fee'],
          'callback_url' => url('api/payment/status')
        ]);
        return $payment->receive();
    }

    public function customerpayment($id, $booking, Request $request)
    {
      $user = User::where('id', $id)->first();
      $bookings = CarWashService::where('id', $booking)->first();
      $input = array();
      $input['name'] = $user->firstname;
      $input['mobile_no'] = $user->mobile;
      $input['address'] = "New Delhi";
      $input['order_id'] = $user->mobile.rand(1,100);
      $input['fee'] = $bookings->paid_amount;

      EventRegistration::create($input);


        $payment = PaytmWallet::with('receive');
        $payment->prepare([
          'order' => $input['order_id'],
          'user' => $user->firstname,
          'mobile_number' => $user->mobile,
          'email' => $user->email,
          'amount' => $input['fee'],
          'callback_url' => url('api/payment/status')
        ]);
        return $payment->receive();

    }


    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');


        $response = $transaction->response();
        $order_id = $transaction->getOrderId();


        if($transaction->isSuccessful()){
          EventRegistration::where('order_id',$order_id)->update(['status'=>2, 'transaction_id'=>$transaction->getTransactionId()]);


          dd('Payment Successfully Paid.');
        }else if($transaction->isFailed()){
          EventRegistration::where('order_id',$order_id)->update(['status'=>1, 'transaction_id'=>$transaction->getTransactionId()]);
          dd('Payment Failed.');
        }
    }    
}
