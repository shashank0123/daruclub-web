<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass;
use App\AndroidHome;
use App\PromoCode;

class AndroidHomepageController extends Controller
{
    public function getHome(Request $request)
    {
    	$response = new StdClass;
        $status = 200;
        $message = "success";
        $data = new StdClass;
        $data->banner_list = AndroidHome::where('type', 'banner')->select('id','banner_url','banner_image','intent')->get();
        foreach ($data->banner_list as $key => $value) {
            $data->banner_list[$key]->banner_url =  "https://camphorservices.com/images/banner/".$value->banner_image;
            $data->banner_list[$key]->intent =  str_replace(' ', '_', $value->intent);

        }

        $data->grid_list = AndroidHome::where('type', 'grid')->select('id','banner_url','banner_image','intent')->get();
        foreach ($data->grid_list as $key => $value) {
            $data->grid_list[$key]->banner_url =  "https://camphorservices.com/images/banner/".$value->banner_image;
            $data->grid_list[$key]->intent =  str_replace(' ', '_', $value->intent);
        }
        $data->text_part = AndroidHome::where('type', 'offers')->select('id','banner_url','banner_image','intent')->first();
        if (isset($data->text_part->banner_url)){
            $data->text_part->banner_url =  "https://camphorservices.com/images/banner/".$value->banner_image;
            $data->text_part->intent =  str_replace(' ', '_', $value->intent);
        }
        $data->rsa = AndroidHome::where('type', 'rsa')->select('id','banner_url','banner_image','intent')->get();
        foreach ($data->rsa as $key => $value) {
            $data->rsa[$key]->banner_url =  "https://camphorservices.com/images/banner/".$value->banner_image;
            $data->rsa[$key]->intent =  str_replace(' ', '_', $value->intent);
        }
        $data->offers = AndroidHome::where('type', 'offers')->select('id', 'banner_url','banner_image','intent')->get();
        foreach ($data->offers as $key => $value) {
            $data->offers[$key]->banner_url =  "https://camphorservices.com/images/banner/".$value->banner_image;
            $data->offers[$key]->text =  $value->intent;
            $data->offers[$key]->intent =  str_replace(' ', '_', $value->intent);
            $data->offers[$key]->price = 'RS '.rand(300, 400);
        }


        // $data
        $response->data = $data;       
        $response->status = $status;
        $response->message = $message;

        return response()->json($response);
    }


    public function offerlist(Request $request)
    {
        $Promocodes = PromoCode::where('status', '1')
                            ->where('promo_code', $promocode)
                            ->where('usable', 1)
                            ->select('promo_code','amount','message','promo_type','usage_type','user_id')
                            ->first();
    }
}
