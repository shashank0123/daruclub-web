<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\SEO;
use Illuminate\Http\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request)
    {
    	$page = 'index';
    	$url = explode('/', $request->url());
    	if (isset($url[3]))
	    	$page = $url[3];
	    $seo = SEO::where('page_name', $page)->first();
	    if ($seo){
	    	$request->session()->put('keywords', $seo->keywords);
	    	$request->session()->put('meta', $seo->meta_desc);
	    	$request->session()->put('title', $seo->title);
	    	$request->session()->put('tags', $seo->tags);
	    }
	    

    }

}
