<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\drink_type;


class DrinkTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drinks = drink_type::orderBy('id','DESC')->paginate(15);
        return view('admin.daruclub.drink-type',compact('drinks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.daruclub.add-drink');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $drink = new drink_type;

        if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/daru-assets/images/Admin/Drinks/', time().$file->getClientOriginalName());
           $drink->image=time().$file->getClientOriginalName();           
        }

        $drink->name          = $request->name;        
        $drink->status        = $request->status;
        
        $drink->save();

        return redirect()->back()->with('message','Data inserted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $drink = drink_type::find($id);

        return view('admin.daruclub.edit-drink',compact('drink'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $drink = drink_type::find($id);

        if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/daru-assets/images/Admin/Drinks/', time().$file->getClientOriginalName());
           $drink->image=time().$file->getClientOriginalName();           
        }

        $drink->name          = $request->name;        
        $drink->status        = $request->status;        
        $drink->update();

        return redirect()->back()->with('message','Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $drink = drink_type::find($id);
        $drink->delete();

        return redirect()->back()->with('message','Data Deleted Successfully');
    }
}
