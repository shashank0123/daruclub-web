<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Bar;

class BarListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bars = Bar::orderBy('id','DESC')->paginate(15);
        return view('admin.daruclub.bar-list',compact('bars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.daruclub.add-bar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bar = new Bar;

        if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/daru-assets/images/Admin/Bars/', time().$file->getClientOriginalName());
           $bar->image=time().$file->getClientOriginalName();           
        }

        $bar->name          = $request->name;
        $bar->rating        = $request->rating;
        $bar->status        = $request->status;
        $bar->open_time     = $request->open_time;
        $bar->bar_desc      = $request->bar_desc;
        $bar->total_seats   = $request->total_seats;

        $bar->save();

        return redirect()->back()->with('message','Data inserted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bar = Bar::find($id);
        return view('admin.daruclub.edit-bar',compact('bar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bar = Bar::find($id);

        if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/daru-assets/images/Admin/Bars/', time().$file->getClientOriginalName());
           $bar->image=time().$file->getClientOriginalName();           
        }

        $bar->name          = $request->name;
        $bar->rating        = $request->rating;
        $bar->status        = $request->status;
        $bar->open_time     = $request->open_time;
        $bar->bar_desc      = $request->bar_desc;
        $bar->total_seats   = $request->total_seats;

        $bar->update();

        return redirect()->back()->with('message','Data updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bar = Bar::find($id);
        $bar->delete();

        return redirect()->back()->with('message','Data Deleted Successfully');
    }
}
