<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Bar;
use App\Menu;

class MenuListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::orderBy('created_at','DESC')->get();

        return view('admin.daruclub.menu-list',compact('menus'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bars = Bar::all();
        return view('admin.daruclub.add-menu',compact('bars'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu;

        if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/daru-assets/images/Admin/Menu/', time().$file->getClientOriginalName());
           $menu->image=time().$file->getClientOriginalName();           
        }

        $menu->name         = $request->name;   
        $menu->bar_id            = $request->bar_id;   
        $menu->price             = $request->price;   
        $menu->availability           = $request->availability;   
        $menu->status            = $request->status;

        $menu->save();

        return redirect()->back()->with('message','Data inserted successfully');   
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        $bars = Bar::all();
        return view('admin.daruclub.edit-menu',compact('menu','bars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);

        if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/daru-assets/images/Admin/Menu/', time().$file->getClientOriginalName());
           $menu->image=time().$file->getClientOriginalName();           
        }

        $menu->name         = $request->name;   
        $menu->bar_id            = $request->bar_id;   
        $menu->price             = $request->price;   
        $menu->availability           = $request->availability;   
        $menu->status            = $request->status;

        $menu->update();

        return redirect()->back()->with('message','Data updated successfully');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();

        return redirect()->back()->with('message','Data deleted successfully'); 
    }
}
