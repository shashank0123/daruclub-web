<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\OTP;
use JWTFactory;
use JWTAuth;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Illuminate\Support\Facades\Password, StdClass;
use Illuminate\Auth\Events\Registered;
use App\Jobs\SignupMail;
use App\Jobs\SendVerificationEmail;

class AuthApiController extends Controller
{
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */


    public function register(Request $request)
    {
        $credentials = $request->only('mobile');
        $user = User::where('mobile', $request->mobile)->first();
        if ($user){
            $request->email = $request->mobile;
            $mobile = $request->mobile;
            $name = 0;
            // return $this->login($request);
            

        }
        else{
            $rules = [
                'mobile' => 'required|unique:users',
            ];
            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['success'=> false, 'error'=> $validator->messages()]);
            }
            $password = 'JaiDaru123';
            $mobile = $request->mobile;
            $email_token = base64_encode($request->mobile);
            $randphone = '123456';
            $user = User::create(['mobile' => $mobile, 'otp' => $randphone, 'email_token' => $email_token, 'password' => Hash::make($password)]);
            $name = 1;
        }
        // ==========================otp ==================================
        if ($mobile){               
                
                $message = "Your%20OTP%20for%20is%20$user->otp";
                $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=1&number=$mobile&message=$message"; 
                // die;
                // $url ="http://103.247.98.91/API/SendMsg.aspx?uname=backstage023&pass=backstage023&send=MBIZTL&dest=$mobile&msg=$message";  
                // $url = "";
                 // $c = curl_init();
                 // curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                 // curl_setopt($c,CURLOPT_HTTPGET ,1);
                 
                 // curl_setopt($c, CURLOPT_URL, $url);
                 // $contents = curl_exec($c);
               // if (curl_errno($c)) {
               //   echo 'Curl error: ' . curl_error($c);
               // }else{
               //   curl_close($c);
               // }
            }
        // event(new Registered($user = $this->create($request->all())));
        // dispatch(new SendVerificationEmail($user));
        // $request->email = $request->mobile;
        // return $this->login($request);
            
        // // ==========================otp ==================================
        // return $this->login($request);
            return response()->json(['success' => true, 'message' => 'Success. OTP Sent', 'name'=> $name], 200);
    }




    public function firebase(Request $request)
    {
        $usercheck = User::where('id', $request->user()->id)->first();
        if ($request->device_token && $usercheck){
            $usercheck->device_token = $request->device_token;
            $usercheck->update();
        }
        return response()->json(['success' => true, 'message' => 'Token updated'], 200);

    }

    public function access(Request $request)
    {
        $mobile = $request->mobile;
        $token = $request->token;
        return $this->login($request);
    }
    public function login(Request $request)
    {
        $request->password = 'JaiDaru123';
        $rules = [
            'mobile' => 'required',
        ];
        $usercheck = User::where('mobile', $request->mobile)->where('otp', $request->otp)->first();
        if ($usercheck){
            if ($request->name != ""){
                $usercheck->name = $request->name;
                $usercheck->update();
            }
            if ($request->device_token){
                $usercheck->device_token = $request->device_token;
                $usercheck->update();
            }
            if ($usercheck){
                $credentials = $request->only('mobile', 'password');
                $validator = Validator::make([
                    'mobile' => $request->mobile,
                    'password' => $request->password
                    ], $rules);
                if($validator->fails()) {
                    return response()->json(['success'=> false, 'message'=> "Please validate before sending"], 302);
                }
                try {
                    // attempt to verify the credentials and create a token for the user
                    if (!$token = JWTAuth::attempt([
                                                    'mobile' => $request->mobile,
                                                    'password' => $request->password
                                                    ])) {
                        return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials.'], 401);
                    }
                } catch (JWTException $e) {
                    // something went wrong whilst attempting to encode the token
                    return response()->json(['success' => false, 'message' => 'Failed to login, please try again.'], 500);
                }
                // all good so return the token
                $user = User::where('mobile', $request->mobile)->first();
                return response()->json(['success' => true, 'user'=> [ 'token' => $token, 'user' => $user], 'message' => 'data received successfully'], 200);
            }
           
        else{

            return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials.'], 401);
        }
         }
        else{

            return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials.'], 401);
        }
    }
    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
        $this->validate($request, ['token' => 'required']);
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }


    public function recover(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $error_message = "Your email address was not found.";
            return response()->json(['success' => false, 'error' => ['email'=> $error_message]], 401);
        }
        try {
            // Password::sendResetLink($request->only('email'), function (Message $message) {
            //     $message->subject('Your Password Reset Link');
            // });
        } catch (\Exception $e) {
            //Return with error
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 401);
        }
        return response()->json([
            'success' => true, 'data'=> ['message'=> 'A reset email has been sent! Please check your email.']
        ]);
    }

    public function change_password(Request $request)
    {

        $response = new StdClass;
        $status = 400;
        $message = "Password not matched";
        $id = $request->user()->id;
        if (!(Hash::check($request->old_password, $request->user()->password))) {
            // The passwords matches
            $message = "Your current password does not matches with the password you provided. Please try again.";
        }
        if(strcmp($request->old_password, $request->new_password) == 0){
            //Current password and new password are same
            $message = "New Password cannot be same as your current password. Please choose a different password.";
        }
        $validatedData = $request->validate([
        'old_password' => 'required',
        'new_password' => 'required',
        ]);
        //Change Password
        $user = $request->user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        if ($user){
            $status = 200;
            $message = "Password changed successfully";
        }
        $response->status = $status;
        $response->message = $message;

        return response()->json($response);

    }

    public function passowordsendOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->mobile;
        $mobile = $request->mobile;
        $user = User::where('mobile', $id)->first();
        $randphone = rand(100000,999999);
        $user->remember_token = $randphone;
        $user->update(); 
        $message = "Your%20OTP%20is%20$randphone.%20Please%20use%20this%20otp%20to%20reset%20your%20password.";
        $url = "http://103.16.101.52:8080/sendsms/bulksms?username=bcks-imzhnd&password=Super123&type=0&dlr=1&destination=$mobile&source=BSSPLI&message='$message'";
         $c = curl_init();
         curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
         curl_setopt($c,CURLOPT_HTTPGET ,1);
         
         curl_setopt($c, CURLOPT_URL, $url);
         $contents = curl_exec($c);
           if (curl_errno($c)) {
             echo 'Curl error: ' . curl_error($c);
           }else{
             curl_close($c);
           }

       $message = "Sms sent";
       $status = 200;

            
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }
    public function sendOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->user()->id;
        $user = User::find($id);
        if ($user->mobile_verification != 'Verified'){
            $mobile = $user->mobile;
            if ($mobile){
                $randphone = rand(100000,999999);
                $otp = new OTP;
                $otp->user_id = $id;
                $otp->otp = $randphone;
                $otp->save();
                $message = "Your%20OTP%20is%20$randphone.%20Please%20use%20this%20otp%20to%20verify%20your%20account.";
                $url = "http://103.16.101.52:8080/sendsms/bulksms?username=bcks-imzhnd&password=Super123&type=0&dlr=1&destination=$mobile&source=BSSPLI&message='$message'";
                 $c = curl_init();
                 curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                 curl_setopt($c,CURLOPT_HTTPGET ,1);
                 
                 curl_setopt($c, CURLOPT_URL, $url);
                 $contents = curl_exec($c);
                   if (curl_errno($c)) {
                     echo 'Curl error: ' . curl_error($c);
                   }else{
                     curl_close($c);
                   }

                   $message = "Sms sent";
                   $status = 200;

            }
            else {
                $message = "No number found";
                $status = 200;
            }

        }
        else {
            $message = "Already Verified";
            $status = 200;
        }
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }







    // public function validateOTP(Request $request)
    // {
    //     $response = new StdClass;
    //     $status = 400;
    //     $message = "Something Went Wrong";
    //     $id = $request->user()->id;
    //     $user = User::find($id);
    //     if ($user){
    //         $otp = OTP::where('user_id', $id)->orderBy('id', 'DESC')->first();
    //         if ($request->otp ==$otp->otp){
    //             $user->mobile_verification = "Verified";
    //             $user->update();
    //             $status = 200;
    //             $message = "Otp verified";

    //         }
    //         else {
    //             $status = 200;
    //             $message = "Otp Missmatch";
    //         }
    //     }
    //     else {
    //         $status = 200;
    //         $message = "User Not Found";
    //     }
    //     $response->status = $status;
    //     $response->message = $message;
    //     return response()->json($response);
    // }







    public function validateOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->user()->id;
        $user = User::find($id);
        if ($user){
            $otp = OTP::where('user_id', $id)->orderBy('id', 'DESC')->first();
            if (isset($request->otp) && isset($otp->otp) && $request->otp == $otp->otp){
                $user->mobile_verification = "Verified";
                $user->update();
                $status = 200;
                $message = "Otp verified";

            }
            else {
                $status = 200;
                $message = "Otp Missmatch";
            }
        }
        else {
            $status = 200;
            $message = "User Not Found";
        }
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    } 

    public function validatechangeOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->mobile;
        $user = User::where('mobile', $id)->first();
        if ($user){
            if ($user->remember_token == $request->otp){
                $user->password = Hash::make($request->password);
                $user->update();
                $status = 200;
                $message = "Password Changed";

            }
            else {
                $message = "Otp Missmatch";
            }
        }
        else {
            $message = "User Not Found";
        }
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }

}
