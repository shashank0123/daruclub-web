<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass, Input;
use App\AndroidHome;

class AndroidHomeController extends Controller
{
	public function index()
    {
       $android_homes =AndroidHome::all();
        return view('admin.android_home', compact('android_homes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_android_home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

    	$validatedData = $this->validate($request, [
            'banner_url' => 'required'            
        ]);
    	
    	$androidhome = new AndroidHome;
    	$androidhome->banner_image 	=$request->banner_image;
    	$androidhome->banner_url 	=$request->banner_url;
    	$androidhome->intent 		=$request->intent;
    	$androidhome->type 		 	=$request->type;
        $androidhome->status          ='1';

    	if (Input::hasfile('banner_image')) {
           $file=Input::file('banner_image');
           $file->move(public_path(). '/images/banner/', $file->getClientOriginalName());
           $androidhome->banner_image=$file->getClientOriginalName();
        }

         $androidhome->save();

        return redirect()->Route('androidhomes.index')->with('message', "Data Successfully Inserted");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $androidhomes =AndroidHome::where('id', $id)->first();
        return view('admin.add_android_home', compact('android_homes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $androidhome =AndroidHome::find($id);
        $androidhome->banner_image 	=$request->banner_image;
    	$androidhome->banner_url 	=$request->banner_url;
    	$androidhome->intent 		=$request->intent;
    	$androidhome->type 		 	=$request->type;
        $androidhome->status          ='1';

    	if (Input::hasfile('banner_image')) {
           $file=Input::file('banner_image');
           $file->move(public_path(). '/images/banner/', $file->getClientOriginalName());
           $androidhome->image=$file->getClientOriginalName();
        }
        $androidhome->update();

        return redirect()->Route('androidhomes.index')->with('message', "Data Successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand =AndroidHome::find($id);
        $brand->delete();
        return redirect()->Route('androidhomes.index')->with('message', "Data Successfully Deleted");
    }


    


}
