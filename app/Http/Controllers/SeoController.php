<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SEO;

class SeoController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $seo = SEO::all();
        return view('admin.seo', compact('seo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_seo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validatedData = $request->validate([
            'page_name' => 'required',            
            'tags' => 'required',            
            'meta_desc' => 'required',            
            'title' => 'required'            
        ]);
        $seo = new SEO;
        $seo->page_name          = $request->page_name;
        $seo->title   = $request->title;
        $seo->tags   = $request->tags;
        $seo->meta_desc   = $request->meta_desc;
	    $seo->save();

        return redirect()->Route('seo.index')->with('message', "Data Successfully Inserted");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seo = SEO::where('id', $id)->first();
        return view('admin.add_seo', compact('seo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seo = SEO::find($id);
        $seo->page_name          = $request->page_name;
        $seo->title   = $request->title;
        $seo->tags   = $request->tags;
        $seo->meta_desc   = $request->meta_desc;
        
        $seo->update();

        return redirect()->Route('seo.index')->with('message', "Data Successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seo = SEO::find($id);
        return redirect()->Route('slider_img.index')->with('message', "Data Successfully Deleted");
    }
}
