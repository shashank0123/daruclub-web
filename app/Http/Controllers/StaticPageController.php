<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function showprivacy()
    {
        return view('frontend.privacypolicy');
    }

    public function showterms()
    {
        return view('frontend.termscondition');
    }
}
