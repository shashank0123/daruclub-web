<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass;
use App\Banner;
use App\accessories;
use App\Modification;
use App\Servicing;
use App\Wrapping;


class BannerController extends Controller
{
    public function get_banner(Request $request)
    {
    	$response = new StdClass;
    	$status = 400;
    	$message = "Data not found";
    	$banner = Banner::where('status', 1)->get();
    	if ($banner){
    		$response->banner = $banner;
    		$status = 200;
    		$message = 'Data retrieved successfully';

    	}

    	$response->status = $status;
    	$response->message = $message;
    	return response()->json($response);
    }


    public function get_accessories(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $accessories = accessories::all();
        if ($accessories){
            $response->accessories = $accessories;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }
    public function get_modification(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $modification = Modification::all();
        if ($modification){
            $response->modification = $modification;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }


    public function get_servicing(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $servicing = Servicing::all();
        if ($servicing){
            $response->servicing = $servicing;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }


    public function get_wrapping(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $wrapping = Wrapping::all();
        if ($wrapping){
            $response->wrapping = $wrapping;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }
}
