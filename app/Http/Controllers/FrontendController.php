<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Sliderimages;
use App\requests;
use App\contacts;
use DB;

class FrontendController extends Controller
{
   



    public function index(){
        return view('home/index',compact('slider','brands'));
    }

    public function about(){
        return view('home/about',compact('slider','brands'));
    }

    public function admin(){
        return view('masterpanel/index');
    }

	public function complain_store(Request $request){
		$validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required'            
        ]);

        $complain = new Complain;
        $complain->name      = $request->name;
        $complain->email      = $request->email;
        $complain->phone      = $request->phone;
        $complain->message      = $request->message;
        $complain->save();

        // return view('frontend/index');
        return \Redirect::back()->with('message', "Your Complain register successfully");
            // return redirect()->back()->with('message', 'Complain');


	}





    public function requests_store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'city' => 'required'            
        ]);

        $requests = new requests;
        $requests->name      = $request->name;
        $requests->email      = $request->email;
        $requests->phone      = $request->phone;
        $requests->city      = $request->city;
        $requests->save();
        // var_dump($requests);
        // die();

        // return view('frontend/index');
        return \Redirect::back()->with('message', "Your request register successfully");

    }




    public function callback_store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required'          
        ]);

        $requests = new callbacks;
        $requests->name      = $request->name;
        $requests->email      = $request->email;
        $requests->phone      = $request->phone;
        $requests->message      = $request->message;
        $requests->save();

        // return view('frontend/index');
        return \Redirect::back()->with('message', "Your request register successfully");

    }



    public function contact_store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required'          
        ]);

        $requests = new contacts;
        $requests->name      = $request->name;
        $requests->email      = $request->email;
        $requests->phone      = $request->phone;
        $requests->message      = $request->message;
        $requests->save();

        // return view('frontend/index');
        return \Redirect::back()->with('message', "Your request register successfully");

    }



    public function ondemand(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'brand_id' => 'required',
            'model_id' => 'required'          
        ]);

        $demand = new Ondemand;
        $demand->name      = $request->name;
        $demand->email      = $request->email;
        $demand->phone      = $request->phone;
        $demand->fuel_type      = $request->fuel_type;
        $demand->address      = $request->address;
        $demand->brand_id      = $request->brand_id;
        $demand->model_id      = $request->model_id;
        $demand->save();

        return \Redirect::back()->with('message', "Your request register successfully");
        // return view('frontend/car_accessories',compact('brands'));


    }





    public function car_accessories(){

        $accessories = accessories::all();
        return view('frontend/car_accessories',compact('accessories'));

    }



    public function accessories(){

        $brands = Brand::orderBy('brand_name')->get();
        return view('frontend/accessories',compact('brands'));

    }

    public function car_servicing(){

        $brands = Brand::orderBy('brand_name')->get();
        $models = Carmodel::all();
        return view('frontend/car_servicing',compact('brands','models'));

    }


    public function car_brand(){

        $brands_data = Brand::orderBy('brand_name')->get();
        // dd($brands_data);
        return view('frontend/brands',compact('brands_data'));

    }


     public function car_packages(){

        $brands = Brand::orderBy('brand_name')->get();
        return view('frontend/package',compact('brands'));

    }



    public function package(Request $request){

        $service = $request->service;
        $category = $request->category;
        $brand_id = $request->brand_id;
        $model_id = $request->model_id;
            $cardata = Car::where('brand_id',$brand_id)
                        ->where('car_model',$model_id)
                        ->get();
              // dd($car);          
        if ($service=='car_wash') {
            $packages = DB::table('carwashes')->where('package_category',$category)->get();
            // return view('frontend/wash_pack',compact('packages'));
            
        }else{
            $packages = DB::table('carservicings')->where('package_category',$category)->get();
        }
            return view('frontend/wash_pack',compact('packages','cardata'));
    }


    public function package_data(Request $request){
        $brand_id = $request->brand;
        $model_id = $request->model;
        $model_data = Carmodel::where('brand_id',$brand_id)
                        ->where('id',$model_id)
                        ->first();  
        $type = $model_data->type;

          $packagedata = Package::where('package_category',$type)
                                  ->get();
           $branddata = Brand::where('id',$brand_id)->get();                       
            // dd($packagedata);
            return view('frontend/packages_data',compact('packagedata','model_data','branddata'));
    }



public function package_form(Request $request){
        $brand_id = $request->brand;
        $model_id = $request->model;
            $model_form_data = Carmodel::where('brand_id',$brand_id)
                        ->where('id',$model_id)
                        ->first();  
          $type = $model_form_data->type;

          $packageFormdata = Package::where('package_category',$type)
                                  ->get();
           $brand_form_data = Brand::where('id',$brand_id)->get();   
           $brands = Brand::orderBy('brand_name')->get();                    
            // dd($packagedata);
            return view('frontend/package',compact('packageFormdata','brands','model_form_data','brand_form_data'));
    }




    public function choosebrand($service, $category){
        $brands = Brand::orderBy('brand_name')->get();
       return view('frontend/choose_brand',compact('service','category','brands'));
    }

 public function shop_prod($accesories_id){
        $prod_id = $accesories_id;
        $brands = Brand::orderBy('brand_name')->get();
        return view('frontend/shop_prod',compact('brands','prod_id'));

    }


     public function model_data($brand_id){

        // $brands = Brand::orderBy('brand_name')->get();
        $data = Carmodel::where('brand_id',$brand_id)->orderBy('model_name')->get();

        return view('frontend/model_data',compact('data'));

            }
    public function model_data2($brand_id){

        // $brands = Brand::orderBy('brand_name')->get();
        $data = Carmodel::where('brand_id',$brand_id)->orderBy('model_name')->get();

        return view('frontend/model_data2',compact('data'));

            }
    //To show brand name
    public function brand_data($brand_name){
        $data = Carmodel::where('brand_name',$brand_name)->get();
        return view('frontend/brandname',compact('data'));
    }

    public function brand(){
         $brands = Brand::orderBy('brand_name')->get();
         return view('frontend/luxury',compact('brands'));
    }


public function getserform(Request $request){
    $brand_id = $request->brand;
    $model_id = $request->model;
    $fuel_type = $request->fuel_type;
    return view('frontend/servicing_form',compact('brand_id','model_id','fuel_type'));

}






}
