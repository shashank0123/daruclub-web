<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bar;
use App\drink_type;
use App\Review;
use App\Menu;
use StdClass;

class BarApiController extends Controller
{
    public function get_bars(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $banner = Bar::where('status', 1)->select('id','name','image','rating')->get();

        if ($banner){
            foreach ($banner as $key => $value) {
                $banner[$key]->image = url('/daru-assets/images/Admin/Bars/'.$value->image);
            }
            $response->bars = $banner;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }

    public function get_drinks(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $banner = drink_type::where('status', 1)->get();
        if ($banner){
            foreach ($banner as $key => $value) {
                $banner[$key]->image = url('/daru-assets/images/Admin/Drinks/'.$value->image);
            }
            $response->drink_type = $banner;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }

    public function get_menu_details(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $banner = Menu::where('status', 1)->get();
        foreach ($banner as $key => $value) {
            $banner[$key]->price = rand(250, 300);
            $banner[$key]->image = url('/daru-assets/images/Admin/Menu/'.$value->image);
        }
        if ($banner){
            $response->bars = $banner;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }


    public function get_bar_details(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Data not found";
        $banner = Bar::where('status', 1)->where('id', $request->bar_id)->first();
        $reviews = Review::where('bar_id', $request->bar_id)->get();
        if ($banner){
            $banner->image = url('/daru-assets/images/Admin/Bars/'.$banner->image);
            $response->bar_details = $banner;
            $response->reviews = $reviews;
            $status = 200;
            $message = 'Data retrieved successfully';

        }

        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }
}
