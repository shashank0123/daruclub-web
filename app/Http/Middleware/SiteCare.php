<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class SiteCare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (Auth::user() &&  Auth::user()->user_type == 'care') {
                return $next($request);
         }

        return redirect('/');
    }
}
