<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', 'AuthApiController@register');
Route::post('login', 'AuthApiController@login');
Route::post('validate_otp', 'AuthApiController@login');
Route::post('recover', 'AuthApiController@recover');
Route::post('access_token', 'AuthApiController@access');
Route::post('validate_password', 'AuthApiController@validatechangeOTP');
Route::get('city', 'CityController@get_city');
Route::get('passwordotp', 'AuthApiController@passowordsendOTP');
Route::get('banner_grid_list', 'AndroidHomepageController@getHome');
    Route::get('sendnotif', 'BarBookingController@sendnotif');
Route::group(['middleware' => ['jwt.auth']], function() {    

    Route::get('logout', 'AuthApiController@logout');
	Route::post('firebasetoken', 'AuthApiController@firebase');
	Route::get('my_bookings'         , 'BarBookingController@viewMyorders');
	Route::post('savebooking'         , 'BarBookingController@saveMyorders');
	Route::post('change_password', 'AuthApiController@change_password');
	Route::post('update_info', 'UserController@editprofileinfo');
	Route::post('complaint'       , 'AppController@Complaintraise');
	Route::post('support'         , 'AppController@Support');
	Route::post('wallet_balance'         , 'WalletController@getBalance');
	Route::get('my_pending_orders'         , 'UserController@viewPendingOrder');
	Route::get('my_transactions'         , 'WalletController@getTransactions');
	Route::get('otp', 'AuthApiController@sendOTP');
	// Route::post('validate_otp', 'AuthApiController@validateOTP');
	Route::get('profile_info', 'UserController@profileinfo');
	Route::get('addtocart', 'CartController@addtocart');
	Route::get('deletefromcart', 'CartController@deletefromcart');
	Route::get('viewcart', 'CartController@viewcart');	
	Route::get('orders', 'UserController@myorders');
	Route::get('promocode', 'PromoController@validatePromoCode');
	Route::post('get_bill', 'WalletController@getBill');
	Route::post('get_hash', 'WalletController@createHash');
	Route::post('request_callback', 'UserController@request_callback');
	Route::post('profile_pic', 'UserController@profilepic');
	Route::post('payment/status', 'PayTMController@paymentCallback');
	Route::post('paytm/generatechecksum', 'PaytmAppController@generatechecksum');
	Route::post('paytm/verifychecksum', 'PaytmAppController@verifychecksum');
	Route::post('paytm/verifytransaction', 'PaytmAppController@verifytransaction');

});

// Banner API
Route::get('drink_type', 'BarApiController@get_drinks');
Route::get('barlist', 'BarApiController@get_bars');
Route::get('bardetails', 'BarApiController@get_bar_details');
Route::get('menu_items', 'BarApiController@get_menu_details');
