<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','DaruClubController@index');
Route::get('/daru-about','DaruClubController@aboutUs');
Route::get('/daru-gallery','DaruClubController@getGallery');
Route::get('/daru-partner','DaruClubController@getPartners');
Route::get('/daru-faq','DaruClubController@getFaq');
Route::get('/daru-contact','DaruClubController@contactUs');
Route::get('/daru-single','DaruClubController@getSingle');






Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
Route::post('password/otp', 'Auth\ResetPasswordController@postReset')->name('password.mobile');
Route::post('password/update', 'Auth\ResetPasswordController@updatePassword')->name('password.update');
Auth::routes();
Route::post('login', 'Auth\LoginController@login');
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');
// Route::get('/home', 'HomeController@index')->name('home');
Route::post('popup/signup', 'PopupController@register');
Route::get('signup', 'PopupController@form');



Route::get('barlist', 'PopupController@form');
Route::get('bardetails', 'PopupController@form');
Route::get('bar-items', 'PopupController@form');
Route::get('bar-items/', 'PopupController@form');


Route::group(['prefix' => 'administrator', 'middleware' => ['role:admin']], function() {
	Route::resource('roles','RoleController');
	Route::resource('users','UserController');
});


Route::group(['prefix' => 'customer', 'middleware' => ['user']], function() {
	Route::get('dashboard', 'CustomerDashboardController@home');
});


Route::get('pay_success', 'PaymentController@success');
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function() {
	Route::get('complains', 'AdminController@complain');
	Route::resource('seo', 'SeoController');
	Route::resource('servicecategories', 'ServiceCategoryAdminController');
	Route::resource('androidhomes', 'AndroidHomeController');
	Route::get('requests', 'AdminController@request');
	Route::get('slider_images','AdminController@slider_image');
	Route::get('userlists','AdminController@userlist');
	
	Route::get('home', 'AdminController@showhome');
	Route::resource('feedbacks', 'FeedbackController');
	Route::get('users', 'AdminController@showusers');
	Route::get('sendsms', 'AdminController@showsendsms');
	Route::resource('promocode', 'PromoCodeAdminController');
	Route::post('sendsms', 'AdminController@sendsms');

	//User list Routes
	Route::get('user-list','Admin\UserController@index');
	Route::get('add-user','Admin\UserController@create');
	Route::post('add-user','Admin\UserController@store');
	Route::get('edit-user/{id}','Admin\UserController@edit');
	Route::post('edit-user/{id}','Admin\UserController@update');
	Route::get('user-list/{id}','Admin\UserController@destroy');

	//Bar list Routes
	Route::get('bar-list','Admin\BarListController@index');
	Route::get('add-bar','Admin\BarListController@create');
	Route::post('add-bar','Admin\BarListController@store');
	Route::get('edit-bar/{id}','Admin\BarListController@edit');
	Route::post('edit-bar/{id}','Admin\BarListController@update');
	Route::get('bar-list/{id}','Admin\BarListController@destroy');

	//Menu list Routes
	Route::get('menu-list','Admin\MenuListController@index');
	Route::get('add-menu','Admin\MenuListController@create');
	Route::post('add-menu','Admin\MenuListController@store');
	Route::get('edit-menu/{id}','Admin\MenuListController@edit');
	Route::post('edit-menu/{id}','Admin\MenuListController@update');
	Route::get('menu-list/{id}','Admin\MenuListController@destroy');

	//Drink Type Routes
	Route::get('drink-type','Admin\DrinkTypeController@index');
	Route::get('add-drink','Admin\DrinkTypeController@create');
	Route::post('add-drink','Admin\DrinkTypeController@store');
	Route::get('edit-drink/{id}','Admin\DrinkTypeController@edit');
	Route::post('edit-drink/{id}','Admin\DrinkTypeController@update');
	Route::get('drink-type/{id}','Admin\DrinkTypeController@destroy');

	//Bookings Routes
	
	Route::get('bookings','Admin\BookingController@index');
	Route::get('add-booking','Admin\BookingController@create');
	Route::post('add-booking','Admin\BookingController@store');
	Route::get('edit-booking/{id}','Admin\BookingController@edit');
	Route::post('edit-booking/{id}','Admin\BookingController@update');
	Route::get('booking/{id}','Admin\BookingController@destroy');

	//Payments Routes
	Route::get('payments','Admin\PaymentController@index');
	Route::get('add-payment','Admin\PaymentController@create');
	Route::post('add-payment','Admin\PaymentController@store');
	Route::get('edit-payment/{id}','Admin\PaymentController@edit');
	Route::post('edit-payment/{id}','Admin\PaymentController@update');
	Route::get('payment/{id}','Admin\PaymentController@destroy');

});

Route::group(['prefix' => 'club', 'middleware' => ['care']], function() {
	Route::get('userlists','AdminController@userlist');
	
	Route::get('home', 'AdminController@showhome');
	Route::resource('feedbacks', 'FeedbackController');
	Route::get('users', 'AdminController@showusers');
	Route::get('sendsms', 'AdminController@showsendsms');
	Route::resource('promocode', 'PromoCodeAdminController');
	Route::post('sendsms', 'AdminController@sendsms');

	
	//Bar list Routes
	Route::get('bar-list','Admin\BarListController@index');
	Route::get('bar-list/{id}','Admin\BarListController@destroy');

	//Menu list Routes
	Route::get('menu-list','Admin\MenuListController@index');
	Route::get('add-menu','Admin\MenuListController@create');
	Route::post('add-menu','Admin\MenuListController@store');
	Route::get('edit-menu/{id}','Admin\MenuListController@edit');
	Route::post('edit-menu/{id}','Admin\MenuListController@update');
	Route::get('menu-list/{id}','Admin\MenuListController@destroy');

	
	
});
Route::get('privacy', 'StaticPageController@showprivacy');
Route::get('terms', 'StaticPageController@showterms');









//================ frontend route ==================
Route::get('/', 'FrontendController@index');
Route::get('/about', 'FrontendController@about');
Route::get('/admin1', 'FrontendController@admin');
Route::get('/home', 'FrontendController@index');
Route::get('index', 'FrontendController@index');
Route::get('brands', 'FrontendController@car_brand');
Route::get('accessories', 'FrontendController@accessories' );
Route::get('car_accessories', 'FrontendController@car_accessories');
Route::get('car_servicing', 'FrontendController@car_servicing');
Route::get('car_modification', function () {    return view('frontend/car_modification'); });
// Route::get('car_servicing', function () {    return view('frontend/car_servicing'); });
Route::get('car_wash', function () {    return view('frontend/car_wash'); });
Route::get('car_wrap', function () {    return view('frontend/car_wrap'); });
Route::get('contact_us', function () {    return view('frontend/contact_us'); });
// Route::get('brands', function () {return view('frontend/brands'); });

Route::get('package', 'FrontendController@car_packages');
Route::get('luxury','FrontendController@brand');
Route::get('luxury/{brand_name}','FrontendController@brand_data');

// Route::get('package', function () {    return view('frontend/package'); });
/*Route::get('bookpackage', function () {    return view('frontend/package_book'); });
*/
Route::get('shop_prod/{accesories_id}','FrontendController@shop_prod');
Route::get('support', function () {    return view('frontend/support'); });
Route::get('wash_pack', function () {    return view('frontend/wash_pack'); });
Route::get('policy', function () {    return view('frontend/policy'); });
Route::POST('complain/add_complain', 'FrontendController@complain_store');
Route::POST('request/add_request', 'FrontendController@requests_store');
Route::POST('request/call_back', 'FrontendController@callback_store');
Route::POST('submit_contact', 'FrontendController@contact_store');
Route::get('wash_pack/{service}/{category}', 'FrontendController@choosebrand');
Route::get('getmodel/{brand_id}', 'FrontendController@model_data');
Route::get('getmodel2/{brand_id}', 'FrontendController@model_data2');
Route::POST('getpackages', 'FrontendController@package_data');
Route::POST('getserform', 'FrontendController@getserform');
Route::POST('shop/ondemand_req', 'FrontendController@ondemand');
Route::POST('package_req', 'FrontendController@package');
Route::POST('package_form', 'FrontendController@package_form');

Auth::routes();
Route::POST('book_your_package', 'HomeController@done_booking');
Route::get('/book_package/{pack_id}/{model_id}', 'HomeController@bookPackage');

Route::POST('update/user_profile', 'HomeController@user_profile')->name('home');
Route::POST('update/password', 'HomeController@changePassword')->name('home');
Route::POST('home/wallet/add_money', 'WalletController@addBalance')->name('add_money');
Route::get('event-registration', 'PayTMController@register');
Route::get('paytm/carwash/{id}/{booking}', 'PayTMController@customerpayment');
Route::post('payment', 'PayTMController@order');


