<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarWashServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_wash_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id');            
            $table->integer('model_id');
            $table->integer('user_id');
            $table->string('car_type');
            $table->string('package_id');
            $table->string('paytm_payment_id')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('paid_amount')->nullable();
            $table->string('used_wallet_balance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_wash_services');
    }
}
