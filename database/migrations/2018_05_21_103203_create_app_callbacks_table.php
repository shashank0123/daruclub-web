<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppCallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_callbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('car_brand')->nullable();
            $table->string('car_model')->nullable();
            $table->string('fuel_type')->nullable();
            $table->string('description')->nullable();
            $table->string('enquiry_for')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_callbacks');
    }
}
