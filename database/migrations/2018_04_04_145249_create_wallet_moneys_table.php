<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletMoneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_moneys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('gateway')->default('payumoney');
            $table->string('status');
            $table->string('remark');
            $table->string('transaction_id')->unique();
            $table->string('amount');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_moneys');
    }
}
