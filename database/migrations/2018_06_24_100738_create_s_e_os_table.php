<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSEOsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_e_o_s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_name');
            $table->string('title')->default('Camphor Services');
            $table->string('keywords')->default('Camphor Services');
            $table->string('tags')->default('Camphor Services');
            $table->string('meta_desc')->default('Camphor Services is a first indian company with car washing facility at your door step');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_e_o_s');
    }
}
