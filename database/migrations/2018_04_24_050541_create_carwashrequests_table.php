<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarwashrequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carwashrequests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id');            
            $table->integer('model_id');
            $table->string('car_type');
            $table->string('name');
            $table->string('mobile');
            $table->string('city');
            $table->string('locality');
            $table->string('apartment');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carwashrequests');
    }
}
