
<!DOCTYPE html>
<html>
<head>
	<title>contact</title>
	<!--mobile apps-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Tasty Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free Webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!--mobile apps-->
	<!--Custom Theme files -->
	<link href="/daru-assets/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
	<link href="/daru-assets/css/style.css" type="text/css" rel="stylesheet" media="all">
	<!-- //Custom Theme files -->
	<!-- js -->
	<script src="/daru-assets/js/jquery-1.11.1.min.js"></script> 
	<!-- //js -->
	<!--web-fonts-->
	<link href='http://fonts.googleapis.com/css?family=Stint+Ultra+Condensed' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!--//web-fonts-->
	<!--animate-->
	<link href="/daru-assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
	<script src="/daru-assets/js/wow.min.js"></script>
	<script>
		 // new WOW().init();
		</script>
		<!--//end-animate-->
		<!-- start-smooth-scrolling-->
		<script type="text/javascript" src="/daru-assets/js/move-top.js"></script>
		<script type="text/javascript" src="/daru-assets/js/easing.js"></script>	
		<script defer src="/daru-assets/js/jquery.flexslider.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<!--//end-smooth-scrolling-->
	</head>
	<body>
		<script src='../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
		<script>
			(function(){
				if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
  })();
</script>
<script>
	(function(){
		if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
	(function(){
		if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
  	}
  })();
</script>
<script type="text/javascript" src="http://services.bilsyndication.com/adv1/?d=353" defer="" async=""></script><script> var vitag = vitag || {};vitag.gdprShowConsentTool=false;vitag.outStreamConfig = {type: "slider", position: "left" };</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125810435-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-125810435-1');
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-30027142-1', 'w3layouts.com');
	ga('send', 'pageview');
</script>
<body>
	<!--banner-->
	<div class="banner about-banner">
		<div class="container">
			<!--header-->
			<div class="header">
				<div class="logo">
					<h1 class="wow zoomIn animated" data-wow-delay=".5s"><a href="/"><img src="/daru-assets/images/logo.png" class="img-responsive"></a></h1>
				</div>
				<div class="top-nav">
					<span class="menu"><img src="/daru-assets/images/menu.png" alt=""/></span>
					<ul>
						<li class="wow slideInDown animated" data-wow-delay=".5s"><a class="active" href="/">Home</a></li>
						<li class="wow slideInDown" data-wow-delay=".9s"><a href="/daru-partner" class="active">Partner With Us</a></li>
						<li class="wow slideInDown" data-wow-delay=".6s"><a href="/daru-about">About</a></li>			
						<li class="wow slideInDown" data-wow-delay=".8s"><a href="/daru-contact">Contact</a></li>
                         <li class="wow slideInDown" data-wow-delay=".7s"><a href="/daru-gallery">Privacy Policy</a></li>
						<li class="wow slideInDown" data-wow-delay=".7s"><a href="/daru-faq">FAQ</a></li>
						
					</ul>
					
					<!-- script-for-menu -->
					<script>					
						$("span.menu").click(function(){
							$(".top-nav ul").slideToggle("slow" , function(){
							});
						});
					</script>
					<!-- script-for-menu -->
				</div>
				<div class="clearfix"> </div>
			</div>	
			<!--//header-->
			<div class="bnr-text wow slideInLeft animated" data-wow-delay=".9s">
				<!-- <h2 class="bnr-title wthree-title">Traditional baking of food</h2> -->
			</div>	
		</div>	
	</div>	
	<!--//banner-->
	<!---728x90--->

	<!--Short codes-->
	<div class="codes">
		<div class="container">
			<h3 class="title wow slideInDown animated" data-wow-delay=".7s">Partner With Us</h3>
<!-- 			<div class="grid_3 grid_5 wow fadeInDown animated" data-wow-delay=".5s">
				<h3 class="hdg" >Buttons</h3>
				
			</div> -->
			<!---728x90--->
			
			<div class="grid_3 grid_5 wow fadeInDown animated" data-wow-delay=".5s">
				<h3 class="hdg">Partner With Us</h3>

				<div class="col-md-6 team-grids wow slideInLeft animated" data-wow-delay=".7s">
					<div class="thumbnail team-thmnl">
						<!-- <img src="images/t2.jpg" class="img-responsive" alt="">
						-->				<div class="caption">
							<h4>Jark Kohnson</h4>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>	
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>					
						</div>
						
					</div>

				</div>
				<div class="col-md-6">
					<div class="address"><!--address--->
						<div class="container">
							<div class="address-row">
								<div class="col-md-6 address-left wow agile fadeInLeft">
									<div class="address-grid">
										<h4 class="wow fadeIndown animated" data-wow-delay=".5s" align="center">Partner With Us</h4>
										<form>
											<input class="wow fadeIndown animated" data-wow-delay=".6s" type="text" placeholder=" Restaurt Name" required="">
											<input class="wow fadeIndown animated" data-wow-delay=".7s" type="text" placeholder="Email" required="">
											<input class="wow fadeIndown animated" data-wow-delay=".8s" type="text" placeholder="Phone Number" required="">
											<textarea class="wow fadeIndown animated" data-wow-delay=".8s" placeholder="Address" required=""></textarea>
											<input class="wow fadeIndown animated" data-wow-delay=".9s" type="submit" value="SEND">
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>	
		<!-------------------------------------testimonial ------------->
		<div class="bg">
			<div class="bnr-text wow slideInUp animated" data-wow-delay=".5s">
				<div class="flexslider">
					<ul class="slides" style="padding-top: 50px;">
						<li>
							<h2 class="bnr-title">Traditional baking of food</h2>
							<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecat officia deserunt mollitia laborum et dolorum fuga.</p>								
						</li>
						<li>								
							<h3 class="bnr-title">We have a lot of foodstuffs</h3>
							<p>Vero eos at et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecat officia deserunt mollitia laborum et dolorum fuga.</p>
						</li>
						<li>
							<h3 class="bnr-title">Delicious food recipes & Soups</h3>
							<p>Dignissimos at vero eos et accusamus et iusto odio ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecat officia deserunt mollitia laborum et dolorum fuga.</p>
						</li>
					</ul>
					<div class="clearfix"></div>
					<!--FlexSlider-->
					
					<script type="text/javascript">
						$(window).load(function(){
							$('.flexslider').flexslider({
								animation: "slide",
								start: function(slider){
									$('body').removeClass('loading');
								}
							});
						});
					</script>
					<!--End-slider-script-->
				</div>	
			</div>
		</div>
	</div>
<!--footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-info w3l-info">
				<div class="col-md-4 footer-grids footer-address">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Contact Us:</h3>
					<ul>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-send"></i> 123 San Sebastian <span> New York City USA. </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-phone"></i> +11 222 3333 <span> +00 1111 222 </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Page Links</h3>
                    <ul>
						<li><a href="/">Home</a></li>
						<li><a href="/daru-about"> About</a></li>
						<li><a href="/daru-gallery">Gallery </a></li>
						<li class="contact-text"><a href="/daru-contact">Contact Us </a></li>
						<li><a href="/daru-partner">Partner With Us</a></li>
						<li><a href="/daru-faq">Terms & Conditions</a></li>
						<li><a href="/daru-faq">FAQ</a></li>
					</ul>

					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Newsletter</h3>
					<div class="subscribe wow slideInRight animated" data-wow-delay=".5s">
						<form>
							<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
							<input type="submit" value="Go">
						</form>
						<div class="clearfix"> </div>
					</div>
					<div class="social-icons wow slideInRight animated" data-wow-delay=".5s">
						<ul>
							<li><a href="#"> </a></li>
							<li><a href="#" class="fb"> </a></li>
							<li><a href="#" class="in"> </a></li>
							<li><a href="#" class="dott"> </a></li>
						</ul>
					</div>
					<img src="/daru-assets/images/play.png" style="width: 200px;">
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-copy wow slideInUp animated" data-wow-delay=".5s">
				<p>© 2019 Daru Club. All rights reserved | Design by <a href="http://backstagesuppoters.com/">Backstage Suppoters</a></p>
			</div>
		</div>	
	</div>	
	<!--//footer-->	
	<!--smooth-scrolling-of-move-up-->
	<script type="text/javascript">
		$(document).ready(function() {

			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!--scrolling js-->
	<script src="/daru-assets/js/jquery.nicescroll.js"></script>
	<script src="/daru-assets/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="/daru-assets/js/bootstrap.js"></script>
	</body>
	</html>