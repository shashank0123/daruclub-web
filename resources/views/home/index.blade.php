
<!DOCTYPE html>
<html>
<head>
<title> Home </title>
<!--mobile apps-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tasty Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free Webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> 
// addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!--mobile apps-->
<!--Custom Theme files -->
<link href="/daru-assets/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="/daru-assets/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="/daru-assets/css/flexslider.css" type="text/css" media="screen" />
<!-- //Custom Theme files -->
<!-- js -->
<script src="/daru-assets/js/jquery-1.11.1.min.js"></script> 
<!-- //js -->
<!--web-fonts-->
<link href='http://fonts.googleapis.com/css?family=Stint+Ultra+Condensed' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--animate-->
<link href="/daru-assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/daru-assets/js/wow.min.js"></script>
	<script>
		 // new WOW().init();
	</script>
<!--//end-animate-->
<!-- start-smooth-scrolling-->
<script type="text/javascript" src="/daru-assets/js/move-top.js"></script>
<script type="text/javascript" src="/daru-assets/js/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smooth-scrolling-->
</head>
<body>
<script src='../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
  	}
})();
</script>
<script type="text/javascript" src="http://services.bilsyndication.com/adv1/?d=353" defer="" async=""></script><script> var vitag = vitag || {};vitag.gdprShowConsentTool=false;vitag.outStreamConfig = {type: "slider", position: "left" };</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125810435-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125810435-1');
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');
</script>
<body>
	<!--banner-->
	<div class="banner">
		<div class="container">
			<!--header-->
			<div class="header">
				<div class="logo">
					<h1 class="w3ls wow zoomIn animated" data-wow-delay=".5s"><a href="/"><img src="/daru-assets/images/logo.png" class="img-responsive"></a></h1>
				</div>
				<div class="top-nav">
					<span class="menu"><img src="/daru-assets/images/menu.png" alt=""/></span>
					<ul>
						<li class="wow slideInDown animated" data-wow-delay=".5s"><a class="active" href="/">Home</a></li>
						<li class="wow slideInDown" data-wow-delay=".9s"><a href="/daru-partner">Partner With Us</a></li>
						<li class="wow slideInDown" data-wow-delay=".6s"><a href="/daru-about">About</a></li>			
						<li class="wow slideInDown" data-wow-delay=".8s"><a href="/daru-contact">Contact</a></li>
                         <li class="wow slideInDown" data-wow-delay=".7s"><a href="/daru-gallery">Privacy Policy</a></li>
						<li class="wow slideInDown" data-wow-delay=".7s"><a href="/daru-faq">FAQ</a></li>
						
					</ul>
					<!-- script-for-menu -->
					<script>					
						$("span.menu").click(function(){
							$(".top-nav ul").slideToggle("slow" , function(){
							});
						});
					</script>
					<!-- script-for-menu -->
				</div>
				<div class="clearfix"> </div>
			</div>	
			<!--//header-->
			<div class="bnr-text wow slideInUp animated" data-wow-delay=".5s">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<h2 class="bnr-title">Traditional baking of food</h2>
							<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecat officia deserunt mollitia laborum et dolorum fuga.</p>								
						</li>
						<li>								
							<h3 class="bnr-title">We have a lot of foodstuffs</h3>
							<p>Vero eos at et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecat officia deserunt mollitia laborum et dolorum fuga.</p>
						</li>
						<li>
							<h3 class="bnr-title">Delicious food recipes & Soups</h3>
							<p>Dignissimos at vero eos et accusamus et iusto odio ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecat officia deserunt mollitia laborum et dolorum fuga.</p>
						</li>
					</ul>
					<div class="clearfix"></div>
					<!--FlexSlider-->
					<script defer src="/daru-assets/js/jquery.flexslider.js"></script>
					<script type="text/javascript">
						$(window).load(function(){
						  $('.flexslider').flexslider({
							animation: "slide",
							start: function(slider){
							  $('body').removeClass('loading');
							}
						  });
						});
					</script>
					<!--End-slider-script-->
				</div>
				<a href="/daru-about" class="more more-right">More About</a>
				<a href="/daru-singlae" class="more more-left">Learn More</a>
			</div>
		</div>	
	</div>	
	<!--//banner-->
	<!---728x90--->

	<!--welcome-->
	<div class="welcome">
		<div class="container">
			<h3 class="title wow fadeInDown animated" data-wow-delay=".5s">What About us </h3>
			<div class="welcome-info">
				<div class="col-md-4 welcome-left w3l wow slideInLeft animated" data-wow-delay=".5s">
					
				</div>
				<div class="col-md-8 welcome-right w3l">
					<div class="welcome-text wow fadeInDown animated" data-wow-delay=".5s">
						<div class="col-md-2 wtext-left">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
						</div>
						<div class="col-md-10 wtext-right">
							<h4>Odio dignissimos</h4>
							<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum </p>
						</div>
						<div class="clearfix"> </div>
						<div class="welcome-bars">
							<label>60%</label>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
								</div>
							</div>
						</div>
					</div>
					<div class="welcome-text wow fadeInDown animated" data-wow-delay=".7s">
						<div class="col-md-2 wtext-left w3l">
							<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
						</div>
						<div class="col-md-10 wtext-right">
							<h4>Sccusamus et</h4>
							<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum </p>
						</div>
						<div class="clearfix"> </div>
						<div class="welcome-bars">
							<label class="bar2">82%</label>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style="width: 82%;">
								</div>
							</div>
						</div>
					</div>	
					<div class="welcome-text w3l wow fadeInDown animated" data-wow-delay=".9s">
						<div class="col-md-2 wtext-left">
							<span class="glyphicon glyphicon-signal" aria-hidden="true"></span>
						</div>
						<div class="col-md-10 wtext-right">
							<h4>Blanditiis prae</h4>
							<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum </p>
						</div>
						<div class="clearfix"> </div>
						<div class="welcome-bars agile">
							<label class="bar3">45%</label>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
								</div>
							</div>
						</div>
					</div>
				</div>	
				<div class="clearfix"> </div>
			</div>	
		</div>	
	</div>	
	<!--//welcome-->
	<!---728x90--->

	<!--menu-form-->
	<div class="menu-form">
		<div class="menu-info">
			<h3 class="title wow fadeInDown animated" data-wow-delay=".5s">Our Menu</h3>
			<div class="scrollbar scrollbar1">
				<div class="menu-grids agile wow fadeInDown animated" data-wow-delay=".5s">
					<div class="menu-left">
						<h4>Fratelli Sette</h4>
						<p> This one works beautifully with spicy Indian dishes. </p>
					</div>
					<div class="menu-right">
						<h5>$18</h5>
					</div>
					<div class="clearfix"> </div>
				</div>	
				<div class="menu-grids menu-grids-mdl wow fadeInDown animated" data-wow-delay=".6s">
					<div class="menu-left">
						<h4>YORK Arros</h4>
						<p>The York Arros wine is a perfect companion for tandoori and grilled meat.</p>
					</div>
					<div class="menu-right">
						<h5>$12</h5>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="menu-grids wow fadeInDown animated" data-wow-delay=".7s">
					<div class="menu-left">
						<h4> Charosa </h4>
						<p>The amazing blend of grapes and spices in this wine make it one of the newest wines in the country.</p>
					</div>
					<div class="menu-right">
						<h5>$14</h5>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="menu-grids menu-grids-mdl">
					<div class="menu-left">
						<h4> Ham hock </h4>
						<p>Porttitor vestibulum eu nisl dignissimos ducimus qui blanditiis praesentium voluptatum</p>
					</div>
					<div class="menu-right">
						<h5>$20</h5>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="menu-grids">
					<div class="menu-left">
						<h4> Calamari </h4>
						<p>Vestibulum porttitor eu nisl dignissimos ducimus qui blanditiis praesentium voluptatum</p>
					</div>
					<div class="menu-right">
						<h5>$25</h5>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="menu-grids menu-grids-mdl">
					<div class="menu-left">
						<h4> Twinkies </h4>
						<p>Dignissimos porttitor vestibulum eu nisl ducimus qui blanditiis praesentium voluptatum</p>
					</div>
					<div class="menu-right">
						<h5>$20</h5>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="menu-grids">
					<div class="menu-left">
						<h4> Beignets </h4>
						<p>Porttitor vestibulum eu nisl dignissimos ducimus qui blanditiis praesentium voluptatum</p>
					</div>
					<div class="menu-right">
						<h5>$11</h5>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>	
	<!--//menu-form-->
	<!---728x90--->

	<!--reservation-->
	<div class="reservation">
		<div class="container">
			<h3 class="title w3agile wow fadeInDown animated" data-wow-delay=".5s">Make a Reservation</h3>
			<div class="book-info">
				<div class="col-md-5 book-left wow fadeInLeft animated" data-wow-delay=".5s">
					<h4 class="wow fadeInDown animated" data-wow-delay=".5s">Working Hours:</h4>
					<h5 class="wow fadeInDown animated" data-wow-delay=".6s">Monday – Saturday 8:00 a.m – 11:00 p.m <br>
						Sunday 10 a.m – 9 p.m <br>
						<span class="glyphicon glyphicon-earphone"></span> +11 999 8888 7777 </h5>
					<span class="burger wow zoomIn animated" data-wow-delay=".7s"> </span>
				</div>
				<div class="col-md-7 book-right agileinfo wow fadeInRight animated" data-wow-delay=".5s">
					<form>
						<label class="wow fadeInDown animated" data-wow-delay=".5s">Date :</label>
						<input class="wow fadeInDown animated" data-wow-delay=".5s"type="date">
						<div class="form-left wow fadeInDown animated" data-wow-delay=".7s">
							<label>No.of People :</label>
							<select class="form-control">
								<option>1 Person</option>
								<option>2 People</option>
								<option>3 People</option>
								<option>4 People</option>
								<option>5 People</option>
								<option>More</option>
							</select>
						</div>
						<div class="form-right wow fadeInDown animated" data-wow-delay=".7s">
							<label>Time :</label>
							<input type="time">
						</div>
						<div class="clearfix"> </div>
						<label class="wow fadeInDown animated" data-wow-delay=".9s">Contact Info :</label>
						<input class="wow fadeInDown animated" data-wow-delay=".9s" type="text" value="Contact Info" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Contact';}" required="">
						<input type="submit" value="Book a Table">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>	
	<!--//reservation-->
	<!--footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-info w3agile">
				<div class="col-md-4 footer-grids footer-address">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Contact Us:</h3>
					<ul>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-send"></i> 123 San Sebastian <span> New York City USA. </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-phone"></i> +11 222 3333 <span> +00 1111 222 </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Page Links</h3>
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="/daru-about"> About</a></li>
						<li><a href="/daru-gallery">Gallery </a></li>
						<li class="contact-text"><a href="/daru-contact">Contact Us </a></li>
						<li><a href="/daru-partner">Partner With Us</a></li>
					    <li><a href="/daru-faq">Terms & Conditions</a></li>
					    <li><a href="/daru-faq">FAQ</a></li>
					    </ul>

						
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Newsletter</h3>
					<div class="subscribe wow slideInRight animated" data-wow-delay=".5s">
						<form>
							<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
							<input type="submit" value="Go">
						</form>
						<div class="clearfix"> </div>
					</div>
					<div class="social-icons wow slideInRight animated" data-wow-delay=".5s">
						<ul>
							<li><a href="#"> </a></li>
							<li><a href="#" class="fb"> </a></li>
							<li><a href="#" class="in"> </a></li>
							<li><a href="#" class="dott"> </a></li>
						</ul>
					</div>
					<img src="/daru-assets/images/play.png" style="width: 200px;">
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-copy wow slideInUp animated" data-wow-delay=".5s">
				<p>© 2019 Daru Club. All rights reserved | Design by <a href="http://backstagesuppoters.com/">Backstage Suppoters</a></p>
			</div>
		</div>	
	</div>	
	<!--//footer-->	
	<!--smooth-scrolling-of-move-up-->
	<script type="text/javascript">
		$(document).ready(function() {
		
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!--scrolling js-->
	<script src="/daru-assets/js/jquery.nicescroll.js"></script>
	<script src="/daru-assets/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/daru-assets/js/bootstrap.js"></script>
</body>
</html>