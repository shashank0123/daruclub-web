<!--banner-->
	<div class="banner about-banner">
		<div class="container">
			<!--header-->
			<div class="header">
				<div class="logo">
					<h1 class="wow zoomIn animated" data-wow-delay=".5s"><a href="/daru-home"><img src="/daru-assets/images/logo.png" class="img-responsive" style="width: 180px;"></a></h1>
				</div>
				<div class="top-nav">
					<span class="menu"><img src="/daru-assets/images/menu.png" alt=""/></span>
					<ul>
						<li class="wow slideInDown animated" data-wow-delay=".5s"><a href="/daru-home">Home</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".6s"><a class="" href="/daru-about">About</a></li>					
						<li class="wow slideInDown animated" data-wow-delay=".7s"><a href="/daru-gallery">Gallery</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".7s"><a href="/daru-partner">Partner With Us</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".8s"><a href="/daru-faq">FAQ</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".9s"><a href="/daru-contact">Contact</a></li>
					</ul>
					<!-- script-for-menu -->
					<script>					
						$("span.menu").click(function(){
							$(".top-nav ul").slideToggle("slow" , function(){
							});
						});
					</script>
					<!-- script-for-menu -->
				</div>
				<div class="clearfix"> </div>
			</div>	
			<!--//header-->
			<div class="bnr-text wow slideInLeft animated" data-wow-delay=".7s">
				<!-- <h2 class="bnr-title agileinfo">Traditional baking of food</h2>
	 -->		</div>	
		</div>	
	</div>	
	<!--//banner-->
	<!---728x90--->