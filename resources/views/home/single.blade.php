
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<title>Tasty Food a Hotel Category Flat Bootstrap Responsive Website Template | Single Page :: w3layouts</title>
<!--mobile apps-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tasty Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free Webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--mobile apps-->
<!--Custom Theme files -->
<link href="/daru-assets/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="/daru-assets/css/style.css" type="text/css" rel="stylesheet" media="all">
<!-- //Custom Theme files -->
<!-- js -->
<script src="/daru-assets/js/jquery-1.11.1.min.js"></script> 
<!-- //js -->
<!--web-fonts-->
<link href='http://fonts.googleapis.com/css?family=Stint+Ultra+Condensed' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--animate-->
<link href="/daru-assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/daru-assets/js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- start-smooth-scrolling-->
<script type="text/javascript" src="/daru-assets/js/move-top.js"></script>
<script type="text/javascript" src="/daru-assets/js/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smooth-scrolling-->
</head>
<body>
<script src='../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
  	}
})();
</script>
<script type="text/javascript" src="http://services.bilsyndication.com/adv1/?d=353" defer="" async=""></script><script> var vitag = vitag || {};vitag.gdprShowConsentTool=false;vitag.outStreamConfig = {type: "slider", position: "left" };</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125810435-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125810435-1');
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');
</script>
<body>
	<!--banner-->
	<div class="banner about-banner">
		<div class="container">
			<!--header-->
			<div class="header">
				<div class="logo">
					<h1 class="wow zoomIn animated" data-wow-delay=".5s"><a href="/"><img src="/daru-assets/images/logo.png" class="img-responsive" style="width: 180px;"></a></h1>
				</div>
				<div class="top-nav">
					<span class="menu"><img src="/daru-assets/images/menu.png" alt=""/></span>
					<ul>
						<li class="wow slideInDown animated" data-wow-delay=".5s"><a href="/"  class="active">Home</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".6s"><a href="/daru-about">About</a></li>					
						<li class="wow slideInDown animated" data-wow-delay=".7s"><a href="/daru-gallery">Gallery</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".7s"><a href="/daru-partner">Partner with us</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".8s"><a href="/daru-faq">Faq</a></li>
						<li class="wow slideInDown animated" data-wow-delay=".9s"><a href="/daru-contact">Contact</a></li>
					</ul>
					<!-- script-for-menu -->
					<script>					
						$("span.menu").click(function(){
							$(".top-nav ul").slideToggle("slow" , function(){
							});
						});
					</script>
					<!-- script-for-menu -->
				</div>
				<div class="clearfix"> </div>
			</div>	
			<!--//header-->
			<div class="bnr-text wow slideInLeft animated" data-wow-delay=".9s">
				<h2 class="bnr-title">Traditional baking of food</h2>
			</div>	
		</div>	
	</div>	
	<!--//banner-->
	<!---728x90--->

	<!--single-page-->
	<div class="blog single-page">
		<div class="container">
			<div class="blog-info">
				<div class="blog-left wow slideInLeft animated" data-wow-delay=".5s">
					<h6>25<span>th</span></h6>
					<h5>April</h5>
				</div>
				<div class="blog-right wow slideInRight animated" data-wow-delay=".5s">
					<div class="blog-img">
						<img src="/daru-assets/images/img6.jpg" alt=" " class="img-responsive" />
					</div>
					<div class="blog-text">
						<h4 class="w3l wow fadeInDown animated" data-wow-delay=".5s">Integer interdum eros vitae sem ultrices sed eleifend tellus tincidunt</h4>
						<p class="wow fadeInDown animated" data-wow-delay=".5s">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
						sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
						nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
						reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
						pariatur,Duis aute irure dolor in quis nostrud exercitation ullamco
						reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
						pariatur</p>
						<div class="article-links wow fadeInDown animated" data-wow-delay=".7s">
							<ul>
								<li><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>April 25, 2016</span></li>
								<li><a href="#"><i class="glyphicon glyphicon-user" aria-hidden="true"></i><span>Admin</span></a></li>
								<li><a href="#"><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><span>No comments</span></a></li>
								<li><a href="#"><i class="glyphicon glyphicon-share" aria-hidden="true"></i><span>View posts</span></a></li>
								<li><a href="#"><i class="glyphicon glyphicon-link" aria-hidden="true"></i><span>Permalink</span></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<!---728x90--->

			<!--admin-text-->
			<div class="admin-text wthree wow fadeInDown animated" data-wow-delay=".5s">
				<h5>Written By Admin</h5>
				<div class="admin-text-left">
					<a href="#"><img src="/daru-assets/images/icon1.png" alt=""/></a>
				</div>
				<div class="admin-text-right">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available, 
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<span>View all posts by:<a href="#"> Admin </a></span>
				</div>
				<div class="clearfix"> </div>
			</div>
			<!--//admin-text-->
			<!---728x90--->

			<!--related-posts-->
			<div class="row related-posts agile">
				<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Related Posts</h3>
				<div class="col-xs-3 col-md-3 related-grids wow zoomIn animated" data-wow-delay=".5s">
					<a href="/daru-single" class="thumbnail">
						<img src="images/g7.jpg" alt=""/>
					</a>
					<h5><a href="/daru-single">Lorem Ipsum is simply</a></h5>
				</div>	
				<div class="col-xs-3 col-md-3 related-grids wow zoomIn animated" data-wow-delay=".5s">
					<a href="/daru-single" class="thumbnail">
						<img src="images/g3.jpg" alt=""/>
					</a>
					<h5><a href="/daru-single">Lorem Ipsum is simply</a></h5>
				</div>
				<div class="col-xs-3 col-md-3 related-grids wow zoomIn animated" data-wow-delay=".6s">
					<a href="/daru-single" class="thumbnail">
						<img src="images/g5.jpg" alt=""/>
					</a>
					<h5><a href="/daru-single">Lorem Ipsum is simply</a></h5>
				</div>
				<div class="col-xs-3 col-md-3 related-grids agileits wow zoomIn animated" data-wow-delay=".6s">
					<a href="/daru-single" class="thumbnail">
						<img src="images/g7.jpg" alt=""/>
					</a>
					<h5><a href="/daru-single">Lorem Ipsum is simply</a></h5>
				</div>					
			</div>				
			<!--//related-posts-->
			<!--comments-->
			<div class="comment-grid-top">
				<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Responses</h3>
				<div class="comments-top-top wow fadeInLeft animated" data-wow-delay=".5s">
					<div class="top-comment-left">
						<a href="#"><img class="img-responsive" src="images/icon1.png" alt=""></a>
					</div>
					<div class="top-comment-right">
						<ul>
							<li><span class="left-at"><a href="#">Admin</a></span></li>
							<li><span class="right-at">April 28, 2016 at 10.30am</span></li>
							<li><a class="reply" href="#">Reply</a></li>
						</ul>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.The point of using Lorem Ipsum is that it has a more-or-less </p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="comments-top-top top-grid-comment wow fadeInRight animated" data-wow-delay=".5s">
					<div class="top-comment-left">
						<a href="#"><img class="img-responsive" src="images/icon1.png" alt=""></a>
					</div>
					<div class="top-comment-right w3">
						<ul>
							<li><span class="left-at"><a href="#">Admin</a></span></li>
							<li><span class="right-at">May 18, 2016 at 10.30am</span></li>
							<li><a class="reply" href="#">Reply</a></li>
						</ul>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.The point of using Lorem Ipsum is that it has a more-or-less </p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<!--//comments-->
			<div class="artical-commentbox">
				<h3 class="wow fadeInDown animated" data-wow-delay=".5s">leave a comment</h3>
				<div class="table-form agileinfo">
					<form>
						<input class="wow fadeInDown animated" data-wow-delay=".5s" type="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
						<input class="wow fadeInDown animated" data-wow-delay=".6s" type="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
						<input class="wow fadeInDown animated" data-wow-delay=".7s" type="text" value="Phone number" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone number';}">
						<textarea class="wow fadeInDown animated" data-wow-delay=".7s" value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>	
						<input class="wow zoomIn animated" data-wow-delay=".5s" type="submit" value="Send">
					</form>
				</div>
			</div>	
		</div>	
	</div>	
	<!--//blog-->	
	<!--footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-info">
				<div class="col-md-4 footer-grids footer-address">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Contact Us:</h3>
					<ul>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-send"></i> 123 San Sebastian <span> New York City USA. </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-phone"></i> +11 222 3333 <span> +00 1111 222 </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Popular Items</h3>
					<div class="footer-grd wow zoomIn animated" data-wow-delay=".5s">
						<a href="/daru-single">
							<img src="/daru-assets/images/f1.jpg" class="img-responsive" alt=" ">
						</a>
					</div>
					<div class="footer-grd wow zoomIn animated" data-wow-delay=".5s">
						<a href="/daru-single">
							<img src="/daru-assets/images/f2.jpg" class="img-responsive" alt=" ">
						</a>
					</div>
					<div class="footer-grd wow zoomIn animated" data-wow-delay=".5s">
						<a href="/daru-single">
							<img src="/daru-assets/images/f3.jpg" class="img-responsive" alt=" ">
						</a>
					</div>
					<div class="clearfix"> </div>
					<div class="footer-grd wow zoomIn animated" data-wow-delay=".5s">
						<a href="/daru-single">
							<img src="/daru-assets/images/f4.jpg" class="img-responsive" alt=" ">
						</a>
					</div>
					<div class="footer-grd wow zoomIn animated" data-wow-delay=".5s">
						<a href="/daru-single">
							<img src="/daru-assets/images/f5.jpg" class="img-responsive" alt=" ">
						</a>
					</div>
					<div class="footer-grd wow zoomIn animated" data-wow-delay=".5s">
						<a href="/daru-single">
							<img src="/daru-assets/images/f6.jpg" class="img-responsive" alt=" ">
						</a>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Newsletter</h3>
					<div class="subscribe wow slideInRight animated" data-wow-delay=".5s">
						<form>
							<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
							<input type="submit" value="Go">
						</form>
						<div class="clearfix"> </div>
					</div>
					<div class="social-icons wow slideInRight animated" data-wow-delay=".5s">
						<ul>
							<li><a href="#"> </a></li>
							<li><a href="#" class="fb"> </a></li>
							<li><a href="#" class="in"> </a></li>
							<li><a href="#" class="dott"> </a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-copy wow slideInUp animated" data-wow-delay=".5s">
				<p>© 2016 Tasty Food. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
			</div>
		</div>	
	</div>	
	<!--//footer-->	
	<!--smooth-scrolling-of-move-up-->
	<script type="text/javascript">
		$(document).ready(function() {
		
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!--scrolling js-->
	<script src="/daru-assets/js/jquery.nicescroll.js"></script>
	<script src="/daru-assets/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/daru-assets/js/bootstrap.js"></script>
</body>
</html>