
<!DOCTYPE html>
<html>

<head>
<title>Privacy Policy</title>
<!--mobile apps-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tasty Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free Webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--mobile apps-->
<!--Custom Theme files -->
<link href="/daru-assets/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="/daru-assets/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="/daru-assets/css/lightbox.css">
<!-- //Custom Theme files -->
<!-- js -->
<script src="/daru-assets/js/jquery-1.11.1.min.js"></script> 
<!-- //js -->
<!--web-fonts-->
<link href='http://fonts.googleapis.com/css?family=Stint+Ultra+Condensed' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--animate-->
<link href="/daru-assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/daru-assets/js/wow.min.js"></script>
	<script>
		 // new WOW().init();
	</script>
<!--//end-animate-->
</head>
<body>
<script src='../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
  	}
})();
</script>
<script type="text/javascript" src="http://services.bilsyndication.com/adv1/?d=353" defer="" async=""></script><script> var vitag = vitag || {};vitag.gdprShowConsentTool=false;vitag.outStreamConfig = {type: "slider", position: "left" };</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125810435-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125810435-1');
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');
</script>
<body>
	<!--banner-->
	<div class="banner about-banner">
		<div class="container">
			<!--header-->
			<div class="header">
				<div class="logo">
					<h1 class="wow zoomIn animated" data-wow-delay=".5s"><a href="/"><img src="/daru-assets/images/logo.png" class="img-responsive"></a></h1>
				</div>
				<div class="top-nav">
					<span class="menu"><img src="/daru-assets/images/menu.png" alt=""/></span>
					<ul>
						<li class="wow slideInDown animated" data-wow-delay=".5s"><a href="/">Home</a></li>
						<li class="wow slideInDown" data-wow-delay=".9s"><a href="/daru-partner">Partner With Us</a></li>
						<li class="wow slideInDown" data-wow-delay=".6s"><a href="/daru-about">About</a></li>			
						<li class="wow slideInDown" data-wow-delay=".8s"><a href="/daru-contact">Contact</a></li>
                         <li class="wow slideInDown" data-wow-delay=".7s"><a href="/daru-gallery"  class="active">Privacy Policy</a></li>
						<li class="wow slideInDown" data-wow-delay=".7s"><a href="/daru-faq">FAQ</a></li>
						
					</ul>
					<!-- script-for-menu -->
					<script>					
						$("span.menu").click(function(){
							$(".top-nav ul").slideToggle("slow" , function(){
							});
						});
					</script>
					<!-- script-for-menu -->
				</div>
				<div class="clearfix"> </div>
			</div>	
			<!--//header-->
			<div class="bnr-text wow slideInLeft animated" data-wow-delay=".9s">
		<!-- 		<h2 class="bnr-title wthree-title">Traditional baking of food</h2> -->
			</div>	
		</div>	
	</div>	
	<!--//banner-->
	<!---728x90--->
<style>
	ul {padding-left: 50px}
	ul li { font-size: 1em;
    color: #999;
    line-height: 24px;
    /* margin-top: -9px; */
    font-family: 'Roboto', sans-serif;  }
</style>
	<!--gallery-->
	<div class="gallery">
		<div class="container">
			<h3 class="title">Privacy Policy</h3>
			<!---728x90--->

			<div class="gallery-info">
				<div class="blog-text">
					<h3>Privacy Policy of Company Name</h3><br>
<p>This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.</p><br>

<p>Your use of the Website and services and tools are governed by the following terms and conditions ( "Terms of Use" ) as applicable to the Website including the applicable policies which are incorporated herein by way of reference. If You transact on the Website, You shall be subject to the policies that are applicable to the Website for such transaction. By mere use of the Website, You shall be contracting with M-Biz Trading Private Limited and these terms and conditions including the policies constitute Your binding obligations, with M-Biz</p><br>
<ul>
	<li>belongs to another person and to which You does not have any right to;</li>
	<li>is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever; or unlawfully threatening or unlawfully harassing including but not limited to "indecent representation of women" within the meaning of the Indecent Representation of Women (Prohibition) Act, 1986;</li>
	<li>is misleading in any way;</li>
	<li>is patently offensive to the online community, such as sexually explicit content, or content that promotes obscenity, paedophilia, racism, bigotry, hatred or physical harm of any kind against any group or individual;</li>
	<li>harasses or advocates harassment of another person;</li>
	<li>involves the transmission of "junk mail", "chain letters", or unsolicited mass mailing or "spamming";</li>
	</ul><br>

<p>If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that we collect are used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p><br>

<p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Website URL, unless otherwise defined in this Privacy Policy.</p><br>



<h3>Contact Us</h3>
<p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.</p>


				</div>
				<div class="clearfix"> </div>
				<script src="/daru-assets/js/lightbox-plus-jquery.min.js"> </script>
			</div>	
		</div>	
	</div>	
	<!--//gallery-->
	<!---728x90--->

	<!--footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-info w3l-info">
				<div class="col-md-4 footer-grids footer-address">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Contact Us:</h3>
					<ul>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-send"></i> 123 San Sebastian <span> New York City USA. </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-phone"></i> +11 222 3333 <span> +00 1111 222 </span></li>
						<li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Page Links</h3>
					
						
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="/daru-about"> About</a></li>
						<li><a href="/daru-gallery">Gallery </a></li>
						<li class="contact-text"><a href="/daru-contact">Contact Us </a></li>
						<li><a href="/daru-partner">Partner With Us</a></li>
					    <li><a href="/daru-faq">Terms & Conditions</a></li>
					    <li><a href="/daru-faq">FAQ</a></li>
					    </ul>
					
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 footer-grids">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Newsletter</h3>
					<div class="subscribe wow slideInRight animated" data-wow-delay=".5s">
						<form>
							<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
							<input type="submit" value="Go">
						</form>
						<div class="clearfix"> </div>
					</div>
					<div class="social-icons wow slideInRight animated" data-wow-delay=".5s">
						<ul>
							<li><a href="#"> </a></li>
							<li><a href="#" class="fb"> </a></li>
							<li><a href="#" class="in"> </a></li>
							<li><a href="#" class="dott"> </a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-copy wow slideInUp animated" data-wow-delay=".5s">
				<p>© 2019 Daru Club. All rights reserved | Design by <a href="http://backstagesuppoters.com/">Backstage Suppoters</a></p>
			</div>
		</div>	
	</div>	
	<!--//footer-->	
	<!-- start-smooth-scrolling-->
	<script type="text/javascript" src="/daru-assets/js/move-top.js"></script>
	<script type="text/javascript" src="/daru-assets/js/easing.js"></script>	
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!--//end-smooth-scrolling-->
	<!--smooth-scrolling-of-move-up-->
	<script type="text/javascript">
		$(document).ready(function() {
		
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!--scrolling js-->
	<script src="/daru-assets/js/jquery.nicescroll.js"></script>
	<script src="/daru-assets/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/daru-assets/js/bootstrap.js"></script>
</body>
</html>