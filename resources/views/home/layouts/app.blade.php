<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<!-- Mirrored from p.w3layouts.com/demos/apr-2016/09-04-2016/tasty_food/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Jul 2019 06:28:21 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<title> Home </title>
<!--mobile apps-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tasty Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
  Smartphone Compatible web template, free Webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> 
// addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!--mobile apps-->
<!--Custom Theme files -->
<link href="/daru-assets/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="/daru-assets/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="/daru-assets/css/flexslider.css" type="text/css" media="screen" />
<!-- //Custom Theme files -->
<!-- js -->
<script src="/daru-assets/js/jquery-1.11.1.min.js"></script> 
<!-- //js -->
<!--web-fonts-->
<link href='http://fonts.googleapis.com/css?family=Stint+Ultra+Condensed' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--animate-->
<link href="/daru-assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/daru-assets/js/wow.min.js"></script>
  <script>
     // new WOW().init();
  </script>
<!--//end-animate-->
<!-- start-smooth-scrolling-->
<script type="text/javascript" src="/daru-assets/js/move-top.js"></script>
<script type="text/javascript" src="/daru-assets/js/easing.js"></script> 
<script type="text/javascript">
    jQuery(document).ready(function($) {
      $(".scroll").click(function(event){   
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
      });
    });
</script>
<!--//end-smooth-scrolling-->
</head>
<body>
<script src='../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
  if(typeof _bsa !== 'undefined' && _bsa) {
      // format, zoneKey, segment:value, options
      _bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
    }
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
  // format, zoneKey, segment:value, options
  _bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
  if(typeof _bsa !== 'undefined' && _bsa) {
      // format, zoneKey, segment:value, options
      _bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
    }
})();
</script>
<script type="text/javascript" src="http://services.bilsyndication.com/adv1/?d=353" defer="" async=""></script><script> var vitag = vitag || {};vitag.gdprShowConsentTool=false;vitag.outStreamConfig = {type: "slider", position: "left" };</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125810435-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125810435-1');
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');
</script>
<body>



  @yield('content')



  <!--footer-->
  <div class="footer">
    <div class="container">
      <div class="footer-info w3agile">
        <div class="col-md-4 footer-grids footer-address">
          <h3 class="wow fadeInDown animated" data-wow-delay=".5s">Contact Us:</h3>
          <ul>
            <li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-send"></i> 123 San Sebastian <span> New York City USA. </span></li>
            <li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-phone"></i> +11 222 3333 <span> +00 1111 222 </span></li>
            <li class="wow slideInLeft animated" data-wow-delay=".5s"><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
          </ul>
        </div>
        <div class="col-md-4 footer-grids">
          <h3 class="wow fadeInDown animated" data-wow-delay=".5s">Page Links</h3>
          <ul>
            <li><a href="/daru-home">Home</a></li>
            <li><a href="/daru-about"> About</a></li>
            <li><a href="/daru-gallery">Gallery </a></li>
            <li><a href="/daru-contact">Contact Us </a></li>
            <li><a href="/daru-partner">Partner With Us</a></li>
              <li><a href="/daru-faq">Terms & Conditions</a></li>
              <li><a href="/daru-faq">FAQ</a></li>
              </ul>

            
          <div class="clearfix"> </div>
        </div>
        <div class="col-md-4 footer-grids">
          <h3 class="wow fadeInDown animated" data-wow-delay=".5s">Newsletter</h3>
          <div class="subscribe wow slideInRight animated" data-wow-delay=".5s">
            <form>
              <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
              <input type="submit" value="Go">
            </form>
            <div class="clearfix"> </div>
          </div>
          <div class="social-icons wow slideInRight animated" data-wow-delay=".5s">
            <ul>
              <li><a href="#"> </a></li>
              <li><a href="#" class="fb"> </a></li>
              <li><a href="#" class="in"> </a></li>
              <li><a href="#" class="dott"> </a></li>
            </ul>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="footer-copy wow slideInUp animated" data-wow-delay=".5s">
        <p>© 2019 Daru Club. All rights reserved | Design by <a href="http://backstagesuppoters.com/">Backstage Suppoters</a></p>
      </div>
    </div>  
  </div>  
  <!--//footer--> 
  <!--smooth-scrolling-of-move-up-->
  <script type="text/javascript">
    $(document).ready(function() {
    
      var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear' 
      };
      
      $().UItoTop({ easingType: 'easeOutQuart' });
      
    });
  </script>
  <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
  <!--//smooth-scrolling-of-move-up-->
  <!--scrolling js-->
  <script src="/daru-assets/js/jquery.nicescroll.js"></script>
  <script src="/daru-assets/js/scripts.js"></script>
  <!--//scrolling js-->
  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/daru-assets/js/bootstrap.js"></script>
</body>

<!-- Mirrored from p.w3layouts.com/demos/apr-2016/09-04-2016/tasty_food/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Jul 2019 06:28:38 GMT -->
</html>