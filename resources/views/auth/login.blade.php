<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DaruClub</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="/admin/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="/admin/css/style.css">
</head>
<body class="fgrowthlogin">

    <div class="main">

        <section class="signup">
            <!-- <img src="img/signup-bg.jpg" alt=""> -->
            <div class="container">
                <div class="signup-content">
                    <form method="POST" id="signup-form" class="signup-form">
                        <h2 class="form-title">Login account</h2>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-input" name="mobile" id="name" placeholder="User Name"/>
                        </div>
                       <!--  <div class="form-group">
                            <input type="email" class="form-input" name="email" id="email" placeholder="Your Email"/>
                        </div> -->
                        <div class="form-group">
                            <input type="password" class="form-input" name="password" id="password" placeholder="Password"/>
                            <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="login"/>
                        </div>
                    </form>
                    <p class="loginhere">
                        Have no account ? <a href="register" class="loginhere-link">Register here</a><br>
                        <a href="forgetPassword" class="loginhere-link">Forget Password ?</a>
                    </p>
                    
                </div>
            </div>
        </section>

    </div>

    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>