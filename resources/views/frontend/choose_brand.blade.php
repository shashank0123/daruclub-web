@extends('frontend/layouts.nav2')

@section('content')


<!-- <div class="jumbotron" style="background: url(http://localhost:8000/images/front/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Car Accessories</h1>
  
</div> -->
<div class="jumbotron" style=" background-size: 100% 100%; margin-top: -20px; padding: 0;">
  <h1 class="about_title" style="">Car Accessories</h1>
  {{-- <hr style="width: 20%;"> --}}
</div>
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="" style="padding: 30px;">
	<div class="container">
		<div class="row">
			{{-- <h1 style="font-family: berlin;text-align: center; color: #183861;">Choose Yo</h1> --}}
			<hr>
			<div style="margin:auto; width: 50%;">
			<!-- <h3 style="font-family: 'Raleway', sans-serif;text-align: center;color: #183861;">Request A Callback</h3> -->
			<div class="form_cont">
				<form action="{{ url('package_req') }}" method="POST" style="text-align: center;">
					{{ csrf_field() }}
					<div class="form-group">
					    <!-- <label class="sr-only" for="name">Name</label> -->
					    <select name="brand_id" required class="form-control brand" onChange="getmodel(this.value)" id="">
					    	<option value="">--Choose Your Brand--</option>
					    	@foreach($brands as $brand)
					    	<option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
					    	@endforeach
					    </select>
					    {{-- <input list="brand"  name="car_brand" class="form-control brand" placeholder="Choose Car Brand" required>
					    <datalist id="brand">
					    	@foreach($brands as $brand)
					    	<option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
					    	@endforeach
					    </datalist> --}}
					</div>
					<input type="hidden" name="service" value="{{ $service }}">
					<input type="hidden" name="category" value="{{ $category }}">
					<div id="model_data"></div>

										
					<div class="col-md-12" style="text-align: center;margin-top: 20px;">
						<input type="submit" class="btn btn-primary" value="Proceed">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<!-- ================================================= -->
<!-- ================================================= -->

<section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection