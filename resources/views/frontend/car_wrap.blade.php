@extends('frontend/layouts.nav2')

@section('content')
<div class="lightbox text-right fixed-top"><label>✖</label></div>

<!-- <h1 style="font-family: berlin; text-align: center; color: rgb(24, 56, 97);">Car Modifications</h1> -->
<!-- Start of slide -->
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			      <!-- Indicators -->
			      <ol class="carousel-indicators">
			        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
			      </ol>

			      <!-- Wrapper for slides -->
			      <div class="carousel-inner" role="listbox">
			        <div class="item active">
			          <img src="{{ asset('images/front/gallery/wrapping') }}/car_wrapping_1.jpg" class="img-responsive" style="width: 100%; height: 400px;" alt="...">
			          <div class="carousel-caption">
			                    <p>Car Wrapping @ Camphor</p>
			          </div>
			        </div>
			        <div class="item">
			          <img src="{{ asset('images/front/gallery/wrapping') }}/car_wrapping_2.jpg" class="img-responsive" style="width: 100%; height: 400px;" alt="...">
			          <div class="carousel-caption">
			            <p>New Style=New Car</p>
			          </div>
			        </div>
			        <div class="item">
			          <img src="{{ asset('images/front/gallery/wrapping') }}/car_wrapping_3.jpg" class="img-responsive" style="width: 100%;height: 400px;" alt="...">
			          <div class="carousel-caption">
			            <p>Make Your Car Breathe Fresh</p>
			          </div>
			        </div>
			        <div class="item">
			          <img src="{{ asset('images/front/gallery/wrapping') }}/car_wrapping_4.png" class="img-responsive" style="width: 100%;height: 400px;" alt="...">
			          <div class="carousel-caption">
			            <p>Go With New Look</p>
			          </div>
			        </div>
			      </div>


			      <!-- Controls -->
			      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			        <span class="sr-only">Previous</span>
			      </a>
			      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			        <span class="sr-only">Next</span>
			      </a>
			    </div>
		<!-- End of slide -->
<section class="content" style="margin-top:  30px">
	<div class="container">
		<p style="text-align:justify;font-size:20px ">
			Camphor Services in one step destination for car wrapping. Vehicle Wrapping is where large sheets of printed vinyl are used to cover a vehicle and transform its appearance.Our vehicle wraps are full colour and we can create any designs as per the customer requirements. We have a brilliant in-house design team too and so can design your vehicle's appearance from scratch to your specifications, or if you just want a straightforward colour change, we offer that too. Solid colour wraps last up to 10 years while full colour digitally printed wraps last up to five. We provide wrap services to any kind of vehicle and we have designed templates for every type of van, car, truck and coach ready to use. We can also provide wraps for everything and our materials removes cleanly, so it's easy and inexpensive to return your vehicles to their original appearance.


  
		</p>
	
	    <p style="text-align:justify;font-weight: bold;font-size:20px ">
			<blockquote>Please email us at: <u>wrapping@camphorservices.com</u></blockquote>   		
		</p>



	</div>
</section>	

 <!-- =============== Request Callback ===================== -->

    <section class="req_callback">
        <div class="container">
            <div class="row">
                <h3>Request A Callback</h3>
                    <form method="post" role="form" class="form-inline" action='request/call_back'> 
                        {{ csrf_field() }}
                <div class="form_cont">
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="name">Name</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-user" aria-hidden="true"></i></div>
                              <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="email">E-mail</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div>
                              <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                            </div>
                        </div>
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="phone">Phone No.</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-mobile" aria-hidden="true"></i></div>
                              <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No.">
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="text-align: center;">
                            <label class="sr-only" for="phone">Your Query</label>
                            <div class="input-group" style="width: 90%; margin-top: 20px;">
                              <!-- <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div> -->
                              <!-- <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No."> -->
                              <textarea class="form-control" style="resize: none;" name="message" id="query" placeholder="Your Query"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                            <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                        </div>
                </div>
                    </form>
            </div>
        </div>
    </section>


    <!-- ========================================================== -->

<!-- Start of gallery section -->
<section id="portfolio" class="portfolio-section white-section default-section">
			<div class="container">
				<div class="section-content">
				<h2 class="text-center" style="font-family: berlin;color: #183861;">Gallery</h2>
						
	<div class="top">
    	<ul>
        	<li><a href="#img_1"><img src="{{ asset('images/front/gallery/wrapping') }}/1.png"></a></li>
            <li><a href="#img_2"><img src="{{ asset('images/front/gallery/wrapping') }}/2.png"></a></li>
            <li><a href="#img_3"><img src="{{ asset('images/front/gallery/wrapping') }}/3.png"></a></li>
            <li><a href="#img_4"><img src="{{ asset('images/front/gallery/wrapping') }}/4.png"></a></li>
            <li><a href="#img_5"><img src="{{ asset('images/front/gallery/wrapping') }}/5.png"></a></li>
            <li><a href="#img_6"><img src="{{ asset('images/front/gallery/wrapping') }}/6.png"></a></li>
            <li><a href="#img_7"><img src="{{ asset('images/front/gallery/wrapping') }}/7.png"></a></li>
            <li><a href="#img_8"><img src="{{ asset('images/front/gallery/wrapping') }}/8.png"></a></li>
            <li><a href="#img_9"><img src="{{ asset('images/front/gallery/wrapping') }}/9.png"></a></li>
            <li><a href="#img_10"><img src="{{ asset('images/front/gallery/wrapping') }}/10.png"></a></li>
            <li><a href="#img_11"><img src="{{ asset('images/front/gallery/wrapping') }}/11.png"></a></li>
            <li><a href="#img_12"><img src="{{ asset('images/front/gallery/wrapping') }}/12.png"></a></li>
            <li><a href="#img_13"><img src="{{ asset('images/front/gallery/wrapping') }}/13.png"></a></li>
            <li><a href="#img_14"><img src="{{ asset('images/front/gallery/wrapping') }}/14.png"></a></li>
            <li><a href="#img_15"><img src="{{ asset('images/front/gallery/wrapping') }}/15.png"></a></li>
            <li><a href="#img_16"><img src="{{ asset('images/front/gallery/wrapping') }}/16.png"></a></li>
            <li><a href="#img_17"><img src="{{ asset('images/front/gallery/wrapping') }}/17.png"></a></li>
            <li><a href="#img_18"><img src="{{ asset('images/front/gallery/wrapping') }}/18.png"></a></li>
            <li><a href="#img_19"><img src="{{ asset('images/front/gallery/wrapping') }}/19.png"></a></li>
            <li><a href="#img_20"><img src="{{ asset('images/front/gallery/wrapping') }}/20.png"></a></li>
            <li><a href="#img_21"><img src="{{ asset('images/front/gallery/wrapping') }}/21.png"></a></li>
            <li><a href="#img_22"><img src="{{ asset('images/front/gallery/wrapping') }}/22.png"></a></li>
            <li><a href="#img_23"><img src="{{ asset('images/front/gallery/wrapping') }}/23.png"></a></li>
            <li><a href="#img_24"><img src="{{ asset('images/front/gallery/wrapping') }}/24.png"></a></li>
            <li><a href="#img_25"><img src="{{ asset('images/front/gallery/wrapping') }}/25.png"></a></li>
            <li><a href="#img_26"><img src="{{ asset('images/front/gallery/wrapping') }}/26.png"></a></li>
            <li><a href="#img_27"><img src="{{ asset('images/front/gallery/wrapping') }}/27.png"></a></li>
            <li><a href="#img_28"><img src="{{ asset('images/front/gallery/wrapping') }}/28.jpg"></a></li>
            <li><a href="#img_29"><img src="{{ asset('images/front/gallery/wrapping') }}/29.png"></a></li>
            <li><a href="#img_30"><img src="{{ asset('images/front/gallery/wrapping') }}/30.png"></a></li>
            <li><a href="#img_31"><img src="{{ asset('images/front/gallery/wrapping') }}/31.png"></a></li>
            <li><a href="#img_32"><img src="{{ asset('images/front/gallery/wrapping') }}/32.png"></a></li>
        </ul>
        <a href="#_1" class="lightbox trans" id="img_1"><img src="{{ asset('images/front/gallery/wrapping') }}/1.png"></a>
        <a href="#_2" class="lightbox trans" id="img_2"><img src="{{ asset('images/front/gallery/wrapping') }}/2.png"></a>
        <a href="#_3" class="lightbox trans" id="img_3"><img src="{{ asset('images/front/gallery/wrapping') }}/3.png"></a>
        <a href="#_4" class="lightbox trans" id="img_4"><img src="{{ asset('images/front/gallery/wrapping') }}/4.png"></a>
        <a href="#_5" class="lightbox trans" id="img_5"><img src="{{ asset('images/front/gallery/wrapping') }}/5.png"></a>
        <a href="#_6" class="lightbox trans" id="img_6"><img src="{{ asset('images/front/gallery/wrapping') }}/6.png"></a>
        <a href="#_7" class="lightbox trans" id="img_7"><img src="{{ asset('images/front/gallery/wrapping') }}/7.png"></a>
        <a href="#_8" class="lightbox trans" id="img_8"><img src="{{ asset('images/front/gallery/wrapping') }}/8.png"></a>
        <a href="#_9" class="lightbox trans" id="img_9"><img src="{{ asset('images/front/gallery/wrapping') }}/9.png"></a>
        <a href="#_10" class="lightbox trans" id="img_10"><img src="{{ asset('images/front/gallery/wrapping') }}/10.png"></a>
        <a href="#_11" class="lightbox trans" id="img_11"><img src="{{ asset('images/front/gallery/wrapping') }}/11.png"></a>
        <a href="#_12" class="lightbox trans" id="img_12"><img src="{{ asset('images/front/gallery/wrapping') }}/12.png"></a>
        <a href="#_13" class="lightbox trans" id="img_13"><img src="{{ asset('images/front/gallery/wrapping') }}/13.png"></a>
        <a href="#_14" class="lightbox trans" id="img_14"><img src="{{ asset('images/front/gallery/wrapping') }}/14.png"></a>
        <a href="#_15" class="lightbox trans" id="img_15"><img src="{{ asset('images/front/gallery/wrapping') }}/15.png"></a>
        <a href="#_16" class="lightbox trans" id="img_16"><img src="{{ asset('images/front/gallery/wrapping') }}/16.png"></a>	
         <a href="#_17" class="lightbox trans" id="img_17"><img src="{{ asset('images/front/gallery/wrapping') }}/17.png"></a>	
          <a href="#_18" class="lightbox trans" id="img_18"><img src="{{ asset('images/front/gallery/wrapping') }}/18.png"></a>	
           <a href="#_19" class="lightbox trans" id="img_19"><img src="{{ asset('images/front/gallery/wrapping') }}/19.png"></a>	
            <a href="#_20" class="lightbox trans" id="img_20"><img src="{{ asset('images/front/gallery/wrapping') }}/20.png"></a>
            <a href="#_21" class="lightbox trans" id="img_21"><img src="{{ asset('images/front/gallery/wrapping') }}/21.png"></a>
            <a href="#_22" class="lightbox trans" id="img_22"><img src="{{ asset('images/front/gallery/wrapping') }}/22.png"></a>
            <a href="#_23" class="lightbox trans" id="img_23"><img src="{{ asset('images/front/gallery/wrapping') }}/23.png"></a>
            <a href="#_24" class="lightbox trans" id="img_24"><img src="{{ asset('images/front/gallery/wrapping') }}/24.png"></a>
            <a href="#_25" class="lightbox trans" id="img_25"><img src="{{ asset('images/front/gallery/wrapping') }}/25.png"></a>
            <a href="#_26" class="lightbox trans" id="img_26"><img src="{{ asset('images/front/gallery/wrapping') }}/26.png"></a>
            <a href="#_27" class="lightbox trans" id="img_27"><img src="{{ asset('images/front/gallery/wrapping') }}/27.png"></a>
            <a href="#_28" class="lightbox trans" id="img_28"><img src="{{ asset('images/front/gallery/wrapping') }}/28.jpg"></a>
            <a href="#_29" class="lightbox trans" id="img_29"><img src="{{ asset('images/front/gallery/wrapping') }}/29.png"></a>
            <a href="#_30" class="lightbox trans" id="img_30"><img src="{{ asset('images/front/gallery/wrapping') }}/30.png"></a> 
            <a href="#_31" class="lightbox trans" id="img_31"><img src="{{ asset('images/front/gallery/wrapping') }}/31.png"></a> 
            <a href="#_32" class="lightbox trans" id="img_32"><img src="{{ asset('images/front/gallery/wrapping') }}/32.png"></a>	

		</div>
	
</div>
</section>

<!-- End of gallery section -->

@endsection