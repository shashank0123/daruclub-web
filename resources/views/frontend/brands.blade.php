@extends('frontend/layouts.nav2')

@section('content')


<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="pack_brand" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;color: #183861;text-align: center;">
				Our Packages
			</h1>
			<hr>

					 <div class="col-md-6">
					 	<a href="{{ url('package') }}" class="">
					 		<p class="pack_brand_text">Daily Car Wash</p>
					 	</a>
					 	<a href="{{ url('package') }}">
					 		
					 	<div class="box">
					 		

					  <div class="bg" style="background-image: url({{ asset('images/front/car_wash.jpg') }});"></div>

					  <div class="inner-box" onclick="document.location='{{ url('package') }}'">
					   <span class="top"></span>
					   <span class="right"></span>
					   <span class="bottom"></span>
					   <span class="left"></span>

					   <a href="{{ url('package') }}" class="btn">Select Package</a>
					  </div>

					 </div>
					 	</a>

					  <!-- <p class="pack_brand_text">Daily Car Wash</p> -->
					 	</div>

					 <div class="col-md-6">
					 
					 	</div>

					 <div class="col-md-6">
					 	<a href="{{ url('car_servicing') }}" class="">
					 		<p class="pack_brand_text">Car Servicing</p>
					 	</a>

					 	<div class="box">
					 	<a href="{{ url('car_servicing') }}">
					 		

					  <div class="bg" style="background-image: url({{ asset('images/front/car_service.png') }});"></div>

					  <div class="inner-box" onclick="document.location='{{ url('car_servicing') }}'">
					   <span class="top"></span>
					   <span class="right"></span>
					   <span class="bottom"></span>
					   <span class="left"></span>

					   <a href="{{ url('car_servicing') }}" class="btn">Select Package</a>
					  </div>

					 </div>
					 	</a>	
			
					 	</div>
		</div>


    <div class="jumbotron" style="margin-top: 50px;background: #3a5f91; color: white;" >
        
        <!-- <div class="row">
            <div class="col-sm-12"> -->

                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <h2 class="section-heading" style="font-family: raleway;letter-spacing: 2px; font-weight: 600;">Brands We Service</h2>
                        <hr style="width: 60%;float: left; margin-top: 0;">
                        {{-- <p class="section-subheading" style="font-size: 16px; font-family: monospace;">Services for all major brands &amp; manufacturers including luxury cars</p> --}}

                        <div class="row">
                            <div class="col-sm-12 hidden-xs">
                                <a class="btn btn-outline" href="/cars">View All Brands</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                    	 <div class="daily_wash">
				            @foreach($brands_data as $brand)
				            <div>
				              {{-- <div class="brand_logo"> --}}
				                <img src="{{ asset('images/brand') }}/{{ $brand->brand_img }}" style="width: 100%; height: 100px; padding: 20px; border-radius: 35px;" alt="">
				              {{-- </div> --}}
				            </div>
				            @endforeach
				          </div>
                    </div>

                   
                </div>
         <!--    </div>
        </div> -->
    </div>

		
	</div>
</section>



@endsection