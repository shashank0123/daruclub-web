<div class="container pack_content" style="text-align: center;">
	<h1 style="text-align: center;">Book Your Package</h1>
	<hr style="width: 20%;">
@foreach($packagedata as $data)
<div class="col-md-3">	
	{{-- <a href="{{ url('book_package/'.$data->id.'/'.$model_data->id) }}"> --}}
		
	{{-- <img class="package_data_img" src="{{ asset('images') }}/pack_back.png" alt="">			 --}}
	<div class="panel package_tab" style="height: auto;">
		<div class="panel-body" style="padding: 0;">
			<div class="pck_img" style="background:white;">
				 
				<img src="{{ asset('images/carmodels').'/'.$model_data->model_img}}" style="height: 250px; width: 100%;" alt="">
			</div>
			<a class="buttons parallelogram" href="{{ url('book_package/'.$data->id.'/'.$model_data->id) }}" style="position: relative; top: -35px;">
				<span class="skew-fix"><i class="fas fa-rupee-sign"></i>{{ $data->package_price }}</span>
			</a>
			<h1 style="margin-top: -15px; margin-bottom: 10px">{{ $data->package_name }}</h1>
			<table class="table" style="width: 100%;">
				<tr style="border-bottom: 1px solid white;">
					<td>Model </td>
					<td>-</td>
					<td>{{ $model_data->model_name}} </td>
				</tr>

			</table>
			<div class="package_desc">
				
				<?php echo $data->package_desc; ?>

			</div>
			<a class="buttons parallelogram" style="margin-bottom: 20px;" href="{{ url('book_package/'.$data->id.'/'.$model_data->id) }}">
				<span class="skew-fix">Buy Now</span>
			</a>
		</div>
	</div>
	{{-- </a> --}}
</div>

@endforeach
</div>
{{-- <div class="clearfix"></div> --}}