@extends('frontend/layouts.nav2')
@section('content')
<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>

	

<div class="jumbotron" style=" background-size: 100% 100%; margin-top: -20px; padding: 0;">
  <h1 class="about_title" style="">About Camphor</h1>
  {{-- <hr style="width: 20%;"> --}}
</div>
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="about" style="padding: 30px;">
	
	<div class="container">

		<div class="row">

			<div class="col-md-6">
				<h1 style="font-family: berlin;color: #183861;">About Camphor</h1>
				<hr>
				<p><strong>Effective date of terms of use: August 24th, 2018 &nbsp;</strong></p>

<p><strong>This document of terms and conditions is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.</strong></p>

<p><strong>This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, and Terms of Use for access or usage of (www.camphorservices.com) website.</strong></p>

<p><strong>The domain name (www.camphorservices.com) (hereinafter referred to as &quot;Website&quot;) is owned by&nbsp;Camphor Services,&nbsp;with its with its Registered Office at Plot No.&nbsp;&nbsp;94,&nbsp;Dwarka Sec&nbsp;-13, Opposite Metro Station, Near Radisson Blu, Dwarka, New Delhi 110075 (hereinafter referred to as &quot;&ldquo;Camphor Services&quot;).</strong></p>

<p><br />
<strong>These terms and conditions apply to this website and any other website, application, software, online portal, forum or service that is in any way related or linked to this website and/or its terms and conditions such as author websites and such created, operated and/or made available by Camphor Services (collectively referred to as sites). All software used on this Site and all content included on this site (including without limitation site design, text, graphics, audio and video and the selection and arrangement thereof) is the property of Camphor Services or and is protected by international copyright laws.</strong></p>

<p><strong>These terms and conditions form the contract of usage between &ldquo;you&rdquo; (user) and &ldquo;us&rdquo; (Camphor Services) and govern your use of the sites. This agreement, together with all updates, supplements, additional terms, and all of Camphor Service&rsquo;s rules and policies collectively constitute and shall be referred to as the &ldquo;Agreement&rdquo; between you and Camphor Services. By accessing this site, you are giving your consent to be borrowed by this agreement. If you do not agree with the terms of usage provided as terms and conditions, you must leave this site immediately.</strong></p>

<p><strong>Membership Eligibility</strong></p>

<p><strong>Use of the Website is available only to persons who can form legally binding contracts under Indian Contract Act, 1872. Persons who are &quot;incompetent to contract&quot; within the meaning of the Indian Contract Act, 1872 including minors, un-discharged insolvents etc. are not eligible to use the Website. If you are a minor i.e. under the age of 18 years, you shall not register as a User of the Camphor Services website and shall not transact on or use the website. As a minor if you wish to use or transact on website, such use or transaction may be made by your legal guardian or parents on the Website. Camphor Services reserves the right to terminate your membership and / or refuse to provide you with access to the Website if it is brought to Camphor Service&rsquo;s notice or if it is discovered that you are under the age of 18 years.</strong></p>

<p><strong>Your Account and Registration Obligations</strong></p>

<p><strong>If you use the Website, you shall be responsible for maintaining the confidentiality of your Display Name and Password and you shall be responsible for all activities that occur under your Display Name and Password. You agree that if you provide any information that is untrue, inaccurate, not current or incomplete or We have reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with this Terms of Use, we shall have the right to indefinitely suspend or terminate or block access of your membership on the Website and refuse to provide you with access to the Website.</strong></p>

<p><strong>Communications</strong></p>

<p><strong>When you use the Website, or send emails or other data, information or communication to us, you agree and understand that you are communicating with us through electronic records and you consent to receive communications via electronic records from us periodically and as and when required. We may communicate with you by email or by such other mode of communication, electronic or otherwise. You hereby expressly agree to receive communications and newsletters from Camphor Services by Notifications and Emails. You can unsubscribe or opt-out from receiving communications by unsubscribing to the newsletter.</strong></p>

<p><strong>Charges</strong></p>

<p><strong>Membership on the Website is paid for users. Camphor Services reserves the right to change its Fee Policy from time to time. In particular, Camphor Services may at its sole discretion introduce new services and modify some or all of the existing services offered on the Website. In such an event Camphor Services reserves the right to introduce fees for the new services offered or amend/introduce fees for existing services, as the case may be. Changes to the Fee Policy shall be posted on the Website and such changes shall automatically become effective immediately after they are posted on the Website. You shall be solely responsible for compliance of all applicable laws including those in India for making payments to Camphor Services.</strong></p>

<p><strong>Use of the Website</strong></p>

<p><strong>You agree, undertake and confirm that your use of Website shall be strictly governed by the following binding principles:</strong></p>

<ol>
    <li>
    <p><strong>You shall not host, display, upload, modify, publish, transmit, update or share any information which:</strong></p>
    </li>
</ol>

<p><strong>(a) belongs to another person and to which you do not have any right to;</strong></p>

<p><strong>(b) is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, invasive of another&#39;s privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever; or unlawfully threatening or unlawfully harassing including but not limited to &quot;indecent representation of women&quot; within the meaning of the Indecent Representation of Women (Prohibition) Act, 1986;</strong></p>

<p><strong>(c) is misleading in any way;</strong></p>

<p><strong>(d) is patently offensive to the online community, such as sexually explicit content, or content that promotes obscenity, paedophilia, racism, bigotry, hatred or physical harm of any kind against any group or individual or an offence under the Information Technology Act and other laws in existence;</strong></p>

<p><strong>(e) harasses or advocates harassment of another person;</strong></p>

<p><strong>(f) involves the transmission of &quot;junk mail&quot;, &quot;chain letters&quot;, or unsolicited mass mailing or &quot;spamming&quot;;</strong></p>

<p><strong>(g) promotes illegal activities or conduct that is abusive, threatening, obscene, defamatory or libellous;</strong></p>

<p><strong>(h) infringes upon or violates any third party&#39;s rights [including, but not limited to, intellectual property rights, rights of privacy (including without limitation unauthorized disclosure of a person&#39;s name, email address, physical address or phone number) or rights of publicity];</strong></p>

<p><strong>(i) promotes an illegal or unauthorized copy of another person&#39;s copyrighted work, such as providing pirated computer programs or links to them, providing information to circumvent manufacture-installed copy-protect devices, or providing pirated music or links to pirated music files;</strong></p>

<p><strong>(j) contains restricted or password-only access pages, or hidden pages or images (those not linked to or from another accessible page);</strong></p>

<p><strong>(k) provides material that exploits people in a sexual, violent or otherwise inappropriate manner or solicits personal information from anyone;</strong></p>

<p><strong>(l) provides instructional information about illegal activities such as making or buying illegal weapons, violating someone&#39;s privacy, or providing or creating computer viruses;</strong></p>

<p><strong>(m) contains video, photographs, or images of another person (with a minor or an adult).</strong></p>

<p><strong>(n) tries to gain unauthorized access or exceeds the scope of authorized access to the Website or to profiles, blogs, communities, account information, bulletins, friend request, or other areas of the Website or solicits passwords or personal identifying information for commercial or unlawful purposes from other users;</strong></p>

<p><strong>(o) engages in commercial activities and/or sales without our prior written consent such as contests, sweepstakes, barter, advertising and pyramid schemes, or the buying or selling of &quot;virtual&quot; products related to the Website. Throughout this Terms of Use, Camphor Service&rsquo;s prior written consent means a communication coming Camphor Service&rsquo;s Management, specifically in response to your request, and specifically addressing the activity or conduct for which you seek authorization;</strong></p>

<p><strong>(p) solicits gambling or engages in any gambling activity which we, in our sole discretion, believes is or could be construed as being illegal;</strong></p>

<p><strong>(q) interferes with another USER&#39;s use and enjoyment of the Website or any other individual&#39;s User and enjoyment of similar services;</strong></p>

<p><strong>(r) refers to any website or URL that, in Our sole discretion, contains material that is inappropriate for the Website or any other website, contains content that would be prohibited or violates the letter or spirit of these Terms of Use.</strong></p>

<p><strong>(s) harm minors in any way;</strong></p>

<p><strong>(t) infringes any patent, trademark, copyright or other proprietary rights or third party&#39;s trade secrets or rights of publicity or privacy or shall not be fraudulent or involve the sale of counterfeit or stolen products;</strong></p>

<p><strong>u) violates any law for the time being in force;</strong></p>

<p><strong>(v) deceives or misleads the addressee/ users about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</strong></p>

<p><strong>(w) impersonate another person;</strong></p>

<p><strong>(x) contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource; or contains any trojan horses, worms, time bombs, cancelbots, easter eggs or other computer programming routines that may damage, detrimentally interfere with, diminish value of, surreptitiously intercept or expropriate any system, data or personal information;</strong></p>

<p><strong>(y) threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.</strong></p>

<p><strong>(z) shall not be false, inaccurate or misleading;</strong></p>

<p><strong>(aa) shall not, directly or indirectly, offer, attempt to offer, trade or attempt to trade in any item, the dealing of which is prohibited or restricted in any manner under the provisions of any applicable law, rule, regulation or guideline for the time being in force.</strong></p>

<p><strong>(ab) shall not create liability for us or cause us to lose (in whole or in part) the services of our internet service provider (&quot;ISPs&quot;) or other suppliers;</strong></p>

<p><strong>(ac) You shall not use any &quot;deep-link&quot;, &quot;page-scrape&quot;, &quot;robot&quot;, &quot;spider&quot; or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the Website or any Content, or in any way reproduce or circumvent the navigational structure or presentation of the Website or any Content, to obtain or attempt to obtain any materials, documents or information through any means not purposely made available through the Website. We reserve our right to bar any such activity.</strong></p>

<p><strong>(ad) You shall not attempt to gain unauthorized access to any portion or feature of the Website, or any other systems or networks connected to the Website or to any server, computer, network, or to any of the services offered on or through the Website, by hacking, password &quot;mining&quot; or any other illegitimate means.</strong></p>

<ol>
    <li>
    <p><strong>You shall not probe, scan or test the vulnerability of the Website or any network connected to the Website nor breach the security or authentication measures on the Website or any network connected to the Website. You may not reverse look-up, trace or seek to trace any information on any other User of or visitor to Website, or any other customer, including any account on the Website not owned by You, to its source, or exploit the Website or any service or information made available or offered by or through the Website, in any way where the purpose is to reveal any information, including but not limited to personal identification or information, other than Your own information, as provided for by the Website.</strong></p>
    </li>
    <li>
    <p><strong>You shall not make any negative, denigrating or defamatory statement(s) or comment(s) about us or the brand name or domain name used by us or otherwise engage in any conduct or action that might tarnish the image or reputation, of Camphor Services on platform or otherwise tarnish or dilute any Camphor Service trade or service marks, trade name and/or goodwill associated with such trade or service marks, trade name as may be owned or used by us. You agree that you will not take any action that imposes an unreasonable or disproportionately large load on the infrastructure of the Website or Camphor Service&rsquo;s systems or networks, or any systems or networks connected to Camphor.</strong></p>
    </li>
    <li>
    <p><strong>You agree not to use any device, software or routine to interfere or attempt to interfere with the proper working of the Website or any transaction being conducted on the Website, or with any other person&#39;s use of the Website.</strong></p>
    </li>
    <li>
    <p><strong>You may not forge headers or otherwise manipulate identifiers in order to disguise the origin of any message or transmittal You send to on or through the Website or any service offered on or through the Website. You may not pretend that you are, or that you represent, someone else, or impersonate any other individual or entity.</strong></p>
    </li>
    <li>
    <p><strong>You may not use the Website or any content for any purpose that is unlawful or prohibited by these Terms of Use, or to solicit the performance of any illegal activity or other activity which infringes the rights of Camphor Services and &nbsp;or others.</strong></p>
    </li>
    <li>
    <p><strong>You shall at all times ensure full compliance with the applicable provisions of the Information Technology Act, 2000 and rules thereunder as applicable and as amended from time to time and also all applicable Domestic laws, rules and regulations (including the provisions of any applicable Exchange Control Laws or Regulations in Force) and International Laws, Foreign Exchange Laws, Statutes, Ordinances and Regulations (including, but not limited to GST, Income Tax, , Service Tax, Central Excise, Custom Duty, Local Levies) regarding Your use of Our service and Your listing, purchase, solicitation of offers to purchase, and sale of products or services. You shall not engage in any transaction in an item or service, which is prohibited by the provisions of any applicable law including exchange control laws or regulations for the time being in force.</strong></p>
    </li>
    <li>
    <p><strong>Solely to enable us to use the information you supply us with, so that we are not violating any rights you might have in your Information, you agree to grant us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable (through multiple tiers) right to exercise the copyright, publicity, database rights or any other rights you have in your Information, in any media now known or not currently known, with respect to your Information. We will only use your information in accordance with the Terms of Use and Privacy Policy applicable to use of the Website.</strong></p>
    </li>
    <li>
    <p><strong>It is possible that other users (including unauthorized users or &quot;hackers&quot;) may post or transmit offensive or obscene materials on the Website and that you may be involuntarily exposed to such offensive and obscene materials. It also is possible for others to obtain personal information about you due to your use of the Website, and that the recipient may use such information to harass or injure You. We do not approve of such unauthorized uses, but by using the Website you acknowledge and agree that We are not responsible for the use of any personal information that You publicly disclose or share with others on the Website. Please carefully select the type of information that You publicly disclose or share with others on the Website.</strong></p>
    </li>
</ol>

<p><strong>Report Abuse</strong></p>

<p><strong>If you become aware of any abuse on the app or website, please report it to (mention an email address)</strong></p>

<p><strong>Copyright and Ownership</strong></p>

<p><strong>All content on this site including but not limited to the text, music, sound, photograph, videos, graphics and images are governed protected by the Indian Copyright Act, 1957 and subsequent amendments, the Trademark Act, 1999 and the subsequent amendments, moral rights, and other laws relating to intellectual property rights.</strong></p>

<p><strong>None of the content may be downloaded, copied, reproduced, republished, posted, transmitted, stored, sold or distributed without the prior written permission of the copyright holder. This excludes the material specifically made available for &lsquo;downloads&rsquo; and sharing on social media on any single computer for personal, non-commercial home use only, provided that all copyright and proprietary notices are kept intact.</strong></p>

<p><strong>Modification of any of the content or use of any of the content for any purpose other than as set out herein (including without limitation on any other website or computer network) is prohibited.</strong></p>

<p><strong>Except as permitted under applicable copyright laws, you are responsible for obtaining permission before re-using any copyrighted material that is available on the Sites. Such permission and requests to republish and/or reuse any of the content and to use quotations or extracts from any books published by Camphor Services or made available on the website, should be addressed to the at the Camphor Services postal address set out below:</strong></p>

<p><strong>Camphor Services </strong></p>

<p><strong>Plot No.&nbsp;&nbsp;94,&nbsp;Dwarka Sec&nbsp;-13, Opposite Metro Station, </strong></p>

<p><strong>Near Radisson Blu, Dwarka, New Delhi 110075</strong></p>

<p><strong>support@camphorservices.com</strong></p>

<p>&nbsp;</p>

<p><strong>Site Access License </strong></p>

<p><strong>Camphor Services grants you a limited, revocable, non-exclusive, non-transferable license to access and make personal, non-commercial use of the Sites or their Content and not to download (other than page caching or unless otherwise allowed by Camphor Services or permitted by law) or modify all or any portion of the Sites and their Content. This license does not include any re-sale or commercial use of the Sites or their Content; any collection and use of any product listings, descriptions, or prices; any derivative use of the Sites or their Content; any downloading or copying of account information for the benefit of another merchant; or any use of data mining, robots, or similar data gathering and extraction tools. The Sites and/or any portion of the Sites or their Content may not be reproduced, duplicated, copied, sold, resold, visited or otherwise exploited for any commercial purpose without Camphor Service&rsquo;s express prior written consent. You shall not frame or utilize framing techniques to enclose any trademark, logo or other proprietary information (including images, text, page layout or form) of Camphor Services, its content providers or its affiliates without express prior written consent. You shall not use any meta tags or any other &ldquo;hidden text&rdquo; utilizing our name or trademarks without our express prior written consent. Additionally, you agree that you will not: (i) take any action that imposes, or may impose in our sole discretion an unreasonable or disproportionately large load on our infrastructure; (ii) interfere or attempt to interfere with the proper working of the Sites or any activities conducted on the Sites; or (iii) bypass any measures we may use to prevent or restrict access to the Sites. Any unauthorized use automatically terminates the permissions and/or licenses granted by us to you.</strong></p>

<p><strong>Corporate Identification and Trademark</strong></p>

<p><strong>All of our trademarks, service marks and trade names used herein (including but not limited to the corporate names and logos of Camphor Services and its publishing divisions and imprints, names and designs of the Sites, and any logos) are trademarks or registered trademarks of Camphor Services or its affiliates, partners, vendors or licensors. You may not use, copy, reproduce, republish, upload, post, transmit, distribute, or modify such trademarks in any way, including in advertising or publicity pertaining to distribution of materials on the Sites, without Camphor Service&rsquo;s express prior written consent. The use of our trademarks on any other website or network computer environment is not allowed. You are granted a limited, revocable, non-exclusive, non-transferable right to create a link to any page of the Sites so long as the link does not portray us, our content providers, our licensors, our affiliates, or our products or services in a false, misleading, derogatory or otherwise offensive manner. You may not use any Camphor Services logo or other proprietary graphic or trademark as part of the link without express written permission. Except as expressly stated herein, no rights or licenses are granted hereunder.</strong></p>

<p><strong>You shall comply with all applicable domestic and international laws, statutes, ordinances and regulations regarding your use of the Sites. The Sites, the Content and all related rights shall remain the exclusive property of Camphor Services and its licensors unless otherwise expressly agreed in writing. You will not remove any copyright, trademark or other proprietary notices from material found on the Sites.</strong></p>

<p><strong>The trademark and registered logos are protected under the trademark laws applicable in India including the Trademark Act, 1999 and the subsequent amendments to it, if any.</strong></p>

<p>&nbsp;</p>

<p><strong>Links to third party web sites</strong></p>

<p><strong>The site may include links to third party Internet websites which are controlled and maintained by others. These links are included solely for the convenience of users and do not constitute any endorsement by Camphor Services of the sites linked or referred to, nor does Camphor Services have any control over the content of any such sites.</strong></p>

<p><strong>You shall not use Camphor Service&rsquo;s name or any language, pictures or symbols which could, in Camphor Service&rsquo;s judgment, imply Camphor Service&rsquo;s endorsement in any (i) written or oral advertising or presentation, or (ii) brochure, newsletter, book, or other written material of whatever nature, without prior written consent.</strong></p>

<p><strong>Indemnity</strong></p>

<p><strong>You shall indemnify and hold harmless Camphor Services, its owner, licensee, affiliates, subsidiaries, group companies (as applicable) and their respective officers, directors, agents, and employees, from any claim or demand, or actions including reasonable attorneys&#39; fees, made by any third party or penalty imposed due to or arising out of Your breach of this Terms of Use, privacy Policy and other Policies, or your violation of any law, rules or regulations or the rights (including infringement of intellectual property rights) of a third party.</strong></p>

<p><strong>Applicable Law</strong></p>

<p><strong>Terms of Use shall be governed by and interpreted and construed in accordance with the laws of India. The place of jurisdiction shall be exclusively in New Delhi, INDIA.</strong></p>

<p><strong>Disclaimer and Limitation of Liability</strong></p>

<p><strong>Your use of and browsing of the Sites are at your own risk. EXCEPT AS OTHERWISE PROVIDED ABOVE, EVERYTHING ON THE SITES, INCLUDING ANY PRODUCTS OR SERVICES OFFERED FOR SALE, IS PROVIDED &ldquo;AS IS&rdquo; WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT. CAMPHOR SERVICES MAKES NO WARRANTIES OR REPRESENTATIONS REGARDING THE INFORMATION OR CONTENT CONTAINED ON THE SITES, AND SPECIFICALLY DISCLAIMS ANY WARRANTY AS TO THE SUITABILITY, RELIABILITY, TIMELINESS OR ACCURACY OF THE INFORMATION OR CONTENT CONTAINED ON THE SITES FOR ANY PURPOSE. CAMPHOR SERVICESDOES NOT WARRANT THAT THE FUNCTIONALITIES CONTAINED IN THE CONTENT WILL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT THE SITES OR THE SERVER THAT MAKES SUCH CONTENT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. CAMPHOR SERVICES DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE CONTENTS IN THE SITES IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE. YOU (AND NOT CAMPHOR SERVICES) ASSUME THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR, OR CORRECTION. TO THE EXTENT APPLICABLE LAW DOES NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, THE ABOVE EXCLUSION MAY NOT APPLY TO YOU.</strong></p>

<p><strong>UNDER NO CIRCUMSTANCES SHALL CAMPHOR SERVICES OR ANY OF ITS VENDORS BE LIABLE FOR ANY DAMAGES THAT RESULT FROM THE USE OF OR INABILITY TO USE THE CAMPHOR SERVICES SITES, INCLUDING BUT NOT LIMITED TO RELIANCE BY A USER ON ANY INFORMATION OR CONTENT OBTAINED FROM CAMPHOR SERVICE&rsquo;S SITES THAT RESULT FROM MISTAKES, OMISSIONS, INTERRUPTIONS, DELETION OF FILES OR EMAIL, ERRORS, DEFECTS, VIRUSES, DELAYS IN OPERATION OR TRANSMISSION, OR ANY FAILURE OF PERFORMANCE, WHETHER OR NOT RESULTING FROM ACTS OF GOD, COMMUNICATIONS FAILURE, THEFT, DESTRUCTION, UNAUTHORIZED ACCESS TO CAMPHOR SERVICE&rsquo;S RECORDS, PROGRAMS, OR SERVICES, OR OTHERWISE. USER HEREBY ACKNOWLEDGES THAT THIS DISCLAIMER AND LIMITATION OF LIABILITY SHALL APPLY TO ALL CONTENT, MERCHANDISE, AND SERVICES AVAILABLE THROUGH THE SITES.</strong></p>

<p><strong>IN NO EVENT SHALL CAMPHOR SERVICESOR ANY AFFILIATED CAMPHOR SERVICES ENTITY OR INDIVIDUAL, LICENSOR OR VENDOR BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL, SPECIAL, INCIDENTAL OR PUNITIVE DAMAGES ARISING OUT OF YOUR ACCESS TO, OR USE OF, THE SITES OR THE CONTENT THEREON, WHETHER IN A CONTRACT ACTION OR BASED ON NEGLIGENCE, OR OTHER TORT ACTION, OR ANY OTHER CLAIM WHATSOEVER, EVEN IF CAMPHOR SERVICES OR A CAMPHOR SERVICES AUTHORIZED REPRESENTATIVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. TO THE EXTENT APPLICABLE LAW DOES NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY OR INCIDENTAL OR CONSEQUENTIAL DAMAGES, THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. IN NO EVENT SHALL CAMPHOR SERVICE&rsquo;S AGGREGATE LIABILITY TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION WHETHER IN CONTRACT, TORT (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE) OR OTHERWISE ARISING OUT OF THE AGREEMENT AND YOUR USE OF THE SITES AND THE CONTENT EXCEED THE AMOUNT PAID BY YOU FOR ANY CONTENT PURCHASED BY YOU ON THE SITES OR THE AMOUNT PAID BY YOU, IF ANY, FOR ACCESSING THE SITES.</strong></p>

			

			</div>
		</div>
	</div>
</section>

@endsection