@extends('frontend/layouts.nav2')


@section('content')

<!-- <h1 style="font-family: berlin; text-align: center; color: rgb(24, 56, 97);">Car Modifications</h1> -->
<!-- Start of slide -->
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			      <!-- Indicators -->
			      <ol class="carousel-indicators">
			        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			      </ol>

			      <!-- Wrapper for slides -->
			      <div class="carousel-inner" role="listbox">
			        <div class="item active">
			          <img src="{{ asset('images/front/gallery/modification') }}/car_modifications_1.jpg" class="img-responsive" style="width: 100%; height: 400px;" alt="...">
			          <div class="carousel-caption">
			                    <p>Car Modifications @ Camphor</p>
			          </div>
			        </div>
			        <div class="item">
			          <img src="{{ asset('images/front/gallery/modification') }}/car_modifications_2.png" class="img-responsive" style="width: 100%; height: 400px;" alt="...">
			          <div class="carousel-caption">
			            <p>Modify your Car</p>
			          </div>
			        </div>
			        <div class="item">
			          <img src="{{ asset('images/front/gallery/modification') }}/car_modifications_3.jpg" class="img-responsive" style="width: 100%;height: 400px;" alt="...">
			          <div class="carousel-caption">
			            <p>Make your car as you want</p>
			          </div>
			        </div>
			      </div>


			      <!-- Controls -->
			      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			        <span class="sr-only">Previous</span>
			      </a>
			      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			        <span class="sr-only">Next</span>
			      </a>
			    </div>
		<!-- End of slide -->
<section class="content" style="margin-top:  30px">
	<div class="container">
		<p style="text-align:justify;font-size:20px ">
			Camphor Services is a destination for giving your car the makeover as it deserves. We strive to offer highest levels of client satisfaction.  We also offer various aftermarket tuning products such as High flow air filters, Body kits, Exhausts systems, ECU remapping, Turbo kits, Engine tuning parts, Alloy wheels, Tyres, Performance brakes, Racing clutch, Suspension upgrades, Projector lights, Sun roof installation, Car wraps, Body lift kits etc. Our specialization includes the engine modifications, exterior styling work, interior leather work as well as audio upgrades as per the customers requirement and budgets. 
		</p>
		<p style="text-align:justify;font-size:20px ">
			We at Camphor Services offer various services under one roof, these include:
	    </p>
	    <ul style="font-weight:bold-italic;font-size: 15px;">
	    	<li>
	    		Complete Car Customization
	    	</li>
	    	<li>
	    		Jeep and SUV Mods
	    	</li>
	    	<li>
	    		Engine and Performance Enhancements
	    	</li>
	    	<li>
	    		Car Wrapping
	    	</li>
	    	<li>
	    		ECU Remapping
	    	</li>
	    	<li>
	    		Interior Modification and Luxury Recliners
	    	</li>
	    	<li>
	    		High End Body Kits
	    	</li>
	    	<li>
	    		Custom Painting and Graphics
	    	</li>
	    	<li>
	    		Bodywork and Accidental Repairs
	    	</li>
	    	<li>
	    		Car Detailing
	    	</li>
	    	<li>
	    		Mechanical Repairs and Servicing
	    	</li>
	    	<li>
	    		Bike Mods and Accessories
	    	</li>
	    </ul>

	    <p style="text-align:justify;font-weight: bold;font-size:20px ">
	    	<strong><marquee>If you do not like fibre modification then we do steel modfication also.</marquee></strong>
			<blockquote>Please email us at: <u>modification@camphorservices.com</u></blockquote>   

		</p>



	</div>
</section>	


 <!-- =============== Request Callback ===================== -->

    <section class="req_callback">
        <div class="container">
            <div class="row">
                <h3>Request A Callback</h3>
                    <form method="post" role="form" class="form-inline" action='request/call_back'> 
                        {{ csrf_field() }}
                <div class="form_cont">
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="name">Name</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-user" aria-hidden="true"></i></div>
                              <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="email">E-mail</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div>
                              <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                            </div>
                        </div>
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="phone">Phone No.</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-mobile" aria-hidden="true"></i></div>
                              <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No.">
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="text-align: center;">
                            <label class="sr-only" for="phone">Your Query</label>
                            <div class="input-group" style="width: 90%; margin-top: 20px;">
                              <!-- <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div> -->
                              <!-- <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No."> -->
                              <textarea class="form-control" style="resize: none;" name="message" id="query" placeholder="Your Query"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                            <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                        </div>
                </div>
                    </form>
            </div>
        </div>
    </section>

   <!-- Start of gallery section -->
<section id="portfolio" class="portfolio-section white-section default-section">
			<div class="container">
				<div class="section-content">
				<h2 class="text-center" style="font-family: berlin;color: #183861;">Gallery</h2>
				
	<div class="top">
    	<ul>
        	<li><a href="#img_1"><img src="{{ asset('images/front/gallery/modification') }}/1.png"></a></li>
            <li><a href="#img_2"><img src="{{ asset('images/front/gallery/modification') }}/2.png"></a></li>
            <li><a href="#img_3"><img src="{{ asset('images/front/gallery/modification') }}/3.png"></a></li>
            <li><a href="#img_4"><img src="{{ asset('images/front/gallery/modification') }}/4.png"></a></li>
            <li><a href="#img_5"><img src="{{ asset('images/front/gallery/modification') }}/5.png"></a></li>
            <li><a href="#img_6"><img src="{{ asset('images/front/gallery/modification') }}/6.png"></a></li>
            <li><a href="#img_7"><img src="{{ asset('images/front/gallery/modification') }}/6.png"></a></li>
            <li><a href="#img_8"><img src="{{ asset('images/front/gallery/modification') }}/8.png"></a></li>
            <li><a href="#img_9"><img src="{{ asset('images/front/gallery/modification') }}/9.png"></a></li>
            <li><a href="#img_10"><img src="{{ asset('images/front/gallery/modification') }}/10.png"></a></li>
            <li><a href="#img_11"><img src="{{ asset('images/front/gallery/modification') }}/11.png"></a></li>
            <li><a href="#img_14"><img src="{{ asset('images/front/gallery/modification') }}/14.png"></a></li>
            <li><a href="#img_15"><img src="{{ asset('images/front/gallery/modification') }}/15.png"></a></li>
            <li><a href="#img_16"><img src="{{ asset('images/front/gallery/modification') }}/16.png"></a></li>
            <li><a href="#img_17"><img src="{{ asset('images/front/gallery/modification') }}/p1.jpg"></a></li>
            <li><a href="#img_18"><img src="{{ asset('images/front/gallery/modification') }}/p2.jpg"></a></li>
            <li><a href="#img_19"><img src="{{ asset('images/front/gallery/modification') }}/p3.jpg"></a></li>
            <li><a href="#img_20"><img src="{{ asset('images/front/gallery/modification') }}/p4.jpg"></a></li>

        </ul>
        <a href="#_1" class="lightbox trans" id="img_1"><img src="{{ asset('images/front/gallery/modification') }}/1.png"></a>
        <a href="#_2" class="lightbox trans" id="img_2"><img src="{{ asset('images/front/gallery/modification') }}/2.png"></a>
        <a href="#_3" class="lightbox trans" id="img_3"><img src="{{ asset('images/front/gallery/modification') }}/3.png"></a>
        <a href="#_4" class="lightbox trans" id="img_4"><img src="{{ asset('images/front/gallery/modification') }}/4.png"></a>
        <a href="#_5" class="lightbox trans" id="img_5"><img src="{{ asset('images/front/gallery/modification') }}/5.png"></a>
        <a href="#_6" class="lightbox trans" id="img_6"><img src="{{ asset('images/front/gallery/modification') }}/6.png"></a>
        <a href="#_7" class="lightbox trans" id="img_7"><img src="{{ asset('images/front/gallery/modification') }}/7.png"></a>
        <a href="#_8" class="lightbox trans" id="img_8"><img src="{{ asset('images/front/gallery/modification') }}/8.png"></a>
        <a href="#_9" class="lightbox trans" id="img_9"><img src="{{ asset('images/front/gallery/modification') }}/9.png"></a>
        <a href="#_10" class="lightbox trans" id="img_10"><img src="{{ asset('images/front/gallery/modification') }}/10.png"></a>
        <a href="#_11" class="lightbox trans" id="img_11"><img src="{{ asset('images/front/gallery/modification') }}/11.png"></a>
        <a href="#_14" class="lightbox trans" id="img_14"><img src="{{ asset('images/front/gallery/modification') }}/14.png"></a>
        <a href="#_15" class="lightbox trans" id="img_15"><img src="{{ asset('images/front/gallery/modification') }}/15.png"></a>
        <a href="#_16" class="lightbox trans" id="img_16"><img src="{{ asset('images/front/gallery/modification') }}/16.png"></a>	
         <a href="#_17" class="lightbox trans" id="img_17"><img src="{{ asset('images/front/gallery/modification') }}/p1.jpg"></a>	
          <a href="#_18" class="lightbox trans" id="img_18"><img src="{{ asset('images/front/gallery/modification') }}/p2.jpg"></a>	
           <a href="#_19" class="lightbox trans" id="img_19"><img src="{{ asset('images/front/gallery/modification') }}/p3.jpg"></a>	
            <a href="#_20" class="lightbox trans" id="img_20"><img src="{{ asset('images/front/gallery/modification') }}/p4.jpg"></a>	
          

		</div>
	
</div>
</section>

	
	
@endsection