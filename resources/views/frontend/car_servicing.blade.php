@extends('frontend/layouts.nav2')

@section('content')

<section class="ser_sec">
	<div class="container" style="padding-top: 50px; padding-bottom: 50px;">
		<div class="col-md-6">
			<div class="car_form2" >
				
			<h1 style="font-family: serif; color: #183861; margin-top: 0;">Select Your Car</h1>
			<form action="" method="POST" id="ser_pack_form">
			 {{ csrf_field() }}
				<div class="form-group pack" style="margin-bottom: 0">
                            <div class="btn-group" style="width: 100%; padding: 1.0em;">
                                <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="Brand">
                                    <span class="multiselect-selected-text">Select Brand</span> <span style="margin-top: 3px;" class="fa fa-angle-down"></span>
                            	</button>
                                <ul class="multiselect-container dropdown-menu">
                                	@foreach($brands as $brand)
                                    <li>
                                        <a tabindex="0">
                                            <label class="radio"><input data-display="{{ $brand->brand_name }}" type="radio" name="brand" value="{{ $brand->id }}">{{ $brand->brand_name }}</label>
                                        </a>
                                                                                        
                                    </li>
                                    @endforeach
                                    
                                         
                                </ul>
                            </div>        
                        </div>
				 <div class="form-group pack" style="margin-bottom: 0">
                            <div class="btn-group" style="width: 100%; padding: 1.0em;">
                                <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="Model">
                                    <span class="multiselect-selected-text">Select Model</span> <span style="margin-top: 3px;" class="fa fa-angle-down"></span>
                              </button>
                                <ul class="multiselect-container dropdown-menu">
                                  <div id="model_data"></div>                                 
                                         
                                </ul>
                            </div>        
                        </div> 
				<div class="form-group fuel_type" style="margin-bottom: 0">
                            <div class="btn-group" style="width: 100%; padding: 1.0em;">
                                <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="fuel">
                                    <span class="multiselect-selected-text">Select Fuel Type</span> <span style="margin-top: 3px;" class="fa fa-angle-down"></span>
                            	</button>
                                <ul class="multiselect-container dropdown-menu">
                                    <li class="fuel_t">
                                        <a tabindex="0">
                                            <label class="radio"><input data-display="Petrol" type="radio" name="fuel_type" value="petrol">Petrol</label>
                                        </a>
                                                                                        
                                    </li>
                                    <li class="fuel_t">
                                        <a tabindex="0">
                                            <label class="radio"><input data-display="Diesel" type="radio" name="fuel_type" value="diesel">Diesel</label>
                                        </a>
                                                                                        
                                    </li>
                                    <li class="fuel_t">
                                        <a tabindex="0">
                                            <label class="radio"><input data-display="CNG" type="radio" name="fuel_type" value="CNG">CNG</label>
                                        </a>
                                                                                        
                                    </li>
                                    
                                         
                                </ul>
                            </div>        
                        </div>
				{{-- <div class="form-group">
				    <!-- <label class="sr-only" for="name">Name</label> -->
				    <span class="custom-dropdown big">
				    <select name="fuel_type" style="font-family: 'Rajdhani', sans-serif;text-transform: uppercase;" required >    
				        <option selected="true" disable="disable" >-- Select Fuel Type --</option>
				        <option value="petrol">Petrol</option>
				        <option value="diesel">Diesel</option>
				        <option value="cng">CNG</option>
				        
				    </select>
					</span>
				</div> --}}
				<div class="form-group"  style="text-align: center;">
					<input type="submit" class="btn btn-danger" value="Submit" style="width: 90%;">
				</div>	

			</form>
			</div>
	     </div>
		<div class="col-md-6">
			<div id="ser_data"></div>
		</div>
	</div>
</section>
<section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				@foreach($models as $model)
				<div>
					<img src="{{ asset('images/carmodels') }}/{{ $model->model_img }}" alt="" class="img-responsive" style="width: 80%; height:150px; margin: auto;">
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>

@endsection