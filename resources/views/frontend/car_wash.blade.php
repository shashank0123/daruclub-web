@extends('frontend/layouts.nav2')

@section('content')

<!-- <div class="jumbotron" style="background: url(img/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Packages</h1>
</div> -->
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="package_sec" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;text-align: center; color: #183861;">Packages for Daily Car Wash</h1>
			<hr>
			<div class="package_wrap">
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/hatchback')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/hatchback.png" alt="">
							</div>
							<div class="pack_text">
								<p>Hatchback</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/sedan ')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/sedan.png" alt="">
							</div>
							<div class="pack_text">
								<p>Sedan</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/suv ')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/suv.png" alt="">
							</div>
							<div class="pack_text">
								<p>SUV</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/premium')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/premium.png" alt="">
							</div>
							<div class="pack_text">
								<p>Premium</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- <section class="package_sec" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;text-align: center; color: #183861;">Packages for Daily Car Servicing</h1>
			<hr>
			<div class="package_wrap">
				<div class="col-sm-3">
					<div class="pack_card">
						<a href="wash_pack.php">
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/hatchback.png" alt="">
							</div>
							<div class="pack_text">
								<p>Hatchback</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href="wash_pack.php">
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/sedan.png" alt="">
							</div>
							<div class="pack_text">
								<p>Sedan</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href="wash_pack.php">
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/suv.png" alt="">
							</div>
							<div class="pack_text">
								<p>SUV</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href="wash_pack.php">
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/premium.png" alt="">
							</div>
							<div class="pack_text">
								<p>Premium</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->

<!-- ================================================= -->

<section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection