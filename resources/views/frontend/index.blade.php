@extends('frontend/layouts.nav')

@section('content')

@if (\Session::has('message'))


    <div class="alert alert-success">
        
           <p> {!! \Session::get('message') !!}</p>
        
    </div>
@endif
    
<div class="car_form" style="top: 17vw" style="">        
      <h1 style="font-family: serif; text-align: center; color: #183861; margin-top: 0;">Select Your Car</h1>
      <form action="{{ url('package_form') }}" method="POST" id="car_form">
       {{ csrf_field() }}        
         <div class="form-group pack">
                            <div class="btn-group" style="width: 100%; padding: 1.0em;">
                                <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="Brand">
                                    <span class="multiselect-selected-text">Select Brand</span> <span style="margin-top: 3px;" class="fa fa-angle-down"></span>
                              </button>
                                <ul class="multiselect-container dropdown-menu">
                                  @foreach($brands as $brand)
                                    <li>
                                        <a tabindex="0">
                                            <label class="radio"><input data-display="{{ $brand->brand_name }}" type="radio" name="brand" value="{{ $brand->id }}">{{ $brand->brand_name }}</label>
                                        </a>
                                                                                        
                                    </li>
                                    @endforeach
                                    
                                         
                                 </ul>
                            </div>        
                        </div>

                 <div class="form-group pack">
                            <div class="btn-group" style="width: 100%; padding: 1.0em;">
                                <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="Model">
                                    <span class="multiselect-selected-text">Select Model</span> <span style="margin-top: 3px;" class="fa fa-angle-down"></span>
                              </button>
                                <ul class="multiselect-container dropdown-menu">
                                  <div id="model_data"></div>
                                         
                                </ul>
                            </div>        
                        </div>        
        <div class="form-group" style="text-align: center;">
          <input type="submit" class="btn btn-danger" value="Submit" style="width: 90%;">
        </div>  

      </form>
       </div>

<main id="col-main">
      
      <div class="container no-space" style="z-index: 999; position:absolute; margin-top: 15px;  left: 5%; right: 5%; ">
        
        <div class="col-md-6 col-xs-12 col-sm-8 p-right">
            <button id="service_btn" class="btn serv" data-toggle="modal" data-target="#modal-2">Request For Service In Your Area</button>
            
            
            <div class="modal" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Request For Service In Your Area</h4>
                  </div>
                  <form method="post" role="form" class="form-horizontal" action='request/add_request'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="sr-only">city</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-location" aria-hidden="true"></i></div>
                                        <input id="city" type="text" class="form-control" name="city" placeholder="Enter Your city" required>
                                         @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> --}}
                  </div>
              </form>
                </div>
              </div>
            </div>
        </div>
        {{-- Start of request a callback model from --}}
        <div class="col-md-6">
            <button id="complain_btn" class="btn" data-toggle="modal" data-target="#callback_modal">Request A Callback</button>
    
            {{-- Modal --}}
            <div class="modal" id="callback_modal" style="z-index: 25;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Request A Callback</h4>
                  </div>
                             <form method="post" role="form" class="form-horizontal" action='request/call_back'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="query">Your Query</label>
                                    <textarea name="message" class="form-control" id="query" cols="5" rows="3" placeholder="Your Query" style="resize: none;"></textarea>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                            </form>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button> --}}
                  </div>
                </div>
              </div>
            </div>
            {{-- end of modal --}}
        </div>
        {{-- End of request of callback of modal form --}}
    
        
      </div>
      
      <div class="flexslider progression-studios-dashboard-slider">
          <ul class="slides">
            @foreach($slider as $slide)
          <li class="progression_studios_animate_left">
            <div class="progression-studios-slider-dashboard-image-background" style="background-image:url(images/slider/{{ $slide->slide_img  }});">
              <div class="progression-studios-slider-display-table">
                <div class="progression-studios-slider-vertical-align">
                
                  <div class="container">
                  <div class="col-md-6">
                    
                    <div class="progression-studios-slider-dashboard-caption-width">
                    </div><!-- close .progression-studios-slider-caption-width -->
                  </div>
                  <div class="col-md-6">
                      
                    <div class="progression-studios-slider-dashboard-caption-width">
                      <div class="progression-studios-slider-caption-align" style="color: white;">
                        <h2>{{ $slide->name }}</h2>
                        <p class="progression-studios-slider-description">{{ $slide->description }}</p>
                        <!-- <div class="card">
                          <div class="inner-card" style="background: url('images/carslide.png');">
                            {{-- <h1>test</h1> --}}
                            
                          </div> -->
                        {{-- </div> --}}
                    </div>
                      </div>
                    </div>
                  
                  </div><!-- close .container -->
                
                </div><!-- close .progression-studios-slider-vertical-align -->
              </div><!-- close .progression-studios-slider-display-table -->
            
              <div class="progression-studios-slider-mobile-background-cover"></div>
            </div><!-- close .progression-studios-slider-image-background -->
          </li>
          @endforeach          
        </ul>
      </div><!-- close .dashboard-container -->
    </main>

<section style="background: rgba(0,0,0,0.05);">
  

<div class="container u_ser" style="">
  <div class="col-md-3 up_service ">
      <div class="ser_con">
        
            <a style="text-decoration: none;" href="{{ url('car_accessories') }}">
            <div class="service-icon">
                <img src="{{ asset('images/front') }}/car.png" alt="">
            </div>
            <div class="service-contnt">
                    <h3 style="color: white;">Car Accessories</h3>
                    <p style="font-size: 14px; color: white;">We offer all types of car accessories at low cost on On-Demand basis of our customers with free shipping & COD. </p>
                    {{-- <p>Online Shopping for cars from a great selection of Seat Covers & Cushions, Mats & Carpets, Car Covers, Sun Shades, Console and Organizers, Air Fresheners & more at everyday low price. We assure you to provide quality services.</p> --}}
            </div>
            </a>
        </div>
      </div>
  <div class="col-md-3 up_service " >
    <div class="ser_con">
      
            <a style="text-decoration: none;" href="{{ url('car_servicing') }}">
            <div class="service-icon">
                <img src="{{ asset('images/front') }}/1.png" alt="">
            </div>
            <div class="service-contnt">
                    <h3 style="color: white;">Car Servicing</h3>
                    <p style="font-size: 14px; color: white;">Book online car service & repair in Gurgaon, Delhi, Noida, Ghaziabad & Faridabad. Best price with genuine spare parts. </p>
                    {{-- <p>Car Servicing with Pick Up and Drop facility. We are offering personalized service experience with assurance of genuine spares, highly skilled technicians, low cost, service warranty and seamless customer experience using technology. </p> --}}
            </div>
            </a>
        </div>
    </div>
        <div class="col-md-3 up_service " id="">
          <div class="ser_con">
            
            <a style="text-decoration: none;" href="{{ url('car_modification') }}">
            <div class="service-icon">
                <img src="{{ asset('images/front') }}/2.png" alt="">
            </div>
            <div class="service-contnt">
                    <h3 style="color: white;">Car Modification</h3>
                    <p style="font-size: 14px; color: white;">Camphor Services is best Car Modification Company in India. We are working all over India. We do offer steel modification instead of fibre.</p>
                   {{--  <p>CAMPHOR SERVICES provide Car modifications & customization services as per customers' requisite. We cater to the need of our clients by providing appropriate solutions through highly skilled craftsmanship.</p> --}}
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3 up_service " id="">
          <div class="ser_con">
            
            <a style="text-decoration: none;" href="{{ url('car_wrap') }}">
            <div class="service-icon">
                <img src="{{ asset('images/front') }}/3.png" alt="">
            </div>
            <div class="service-contnt">
                    <h3 style="color: white;">Car Wrapping</h3>
                    <p style="font-size: 14px; color: white;">We offer Car Wrapping, Car Vinyl Wrapping, Vehicle Coating, Designer Car Wrapping if  you want to give your car a fresh new look.</p>
                    {{-- <p>CAMPHOR SERVICES comes as a reliable place to get the look changed of your cars and other vehicles. we are successfully keeping up to the excellence parameters in our products and services.</p> --}}
            </div>
            </a>
          </div>
        </div>   
</div>

</section>











    <!-- <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      Indicators
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>
    
      Wrapper for slides
      <div class="carousel-inner" role="listbox">
        <?php $i=1; ?>
        @foreach ($slider as $data)
        <div class="item <?php if($i == 1){ echo "active" ;}  ?>">
          <img  src="{{ asset('images/slider').'/'.$data->slide_img }}" class="img-responsive" style="width: 100%; height: 450px;" alt="...">
          <div class="carousel-caption">
                    <p>{{ $data->description}}</p>
          </div>
        </div>
        <?php $i++; ?>
        @endforeach
        {{-- <div class="item">
          <img src="{{ asset('images/front') }}/banner-bg.png" class="img-responsive" style="width: 100%; height: 450px;" alt="...">
          <div class="carousel-caption">
            <p>Make Your Car Look As Good
            As NEW</p>
          </div>
        </div>
        <div class="item">
          <img src="{{ asset('images/front') }}/banner-bg.png" class="img-responsive" style="width: 100%; height:450px;" alt="...">
          <div class="carousel-caption">
            <p>Make Your Car Look As Good
            As NEW</p>
          </div>
        </div> --}}
      </div>
    
    
      Controls
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <div class="complain container">
        <div class="col-md-4">
            <button id="complain_btn" class="btn" data-toggle="modal" data-target="#complain_modal">Raise A Complaint</button>
    
            Modal
            <div class="modal fade" id="complain_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Raise A Complaint</h4>
                  </div>
                             <form method="post" role="form" class="form-horizontal" action='complain/add_complain'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="query">Your Query</label>
                                    <textarea name="message" class="form-control" id="query" cols="5" rows="3" placeholder="Your Query" style="resize: none;"></textarea>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                            </form>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button> --}}
                  </div>
                </div>
              </div>
            </div>
            end of modal
        </div>
    
        <div class="col-md-4">
            <button id="service_btn" class="btn" data-toggle="modal" data-target="#modal-2">Request For Service In Your Area</button>
            
            Modal
            <div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Request For Service In Your Area</h4>
                  </div>
                  <form method="post" role="form" class="form-horizontal" action='request/add_request'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="sr-only">city</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-location" aria-hidden="true"></i></div>
                                        <input id="city" type="text" class="form-control" name="city" placeholder="Enter Your city" required>
                                         @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> --}}
                  </div>
              </form>
                </div>
              </div>
            </div>
        </div>
        Start of request a callback model from
        <div class="col-md-4">
            <button id="complain_btn" class="btn" data-toggle="modal" data-target="#callback_modal">Request A Callback</button>
    
            Modal
            <div class="modal fade" id="callback_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Request A Callback</h4>
                  </div>
                             <form method="post" role="form" class="form-horizontal" action='request/call_back'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="query">Your Query</label>
                                    <textarea name="message" class="form-control" id="query" cols="5" rows="3" placeholder="Your Query" style="resize: none;"></textarea>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                            </form>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button> --}}
                  </div>
                </div>
              </div>
            </div>
            end of modal
        </div>
        End of request of callback of modal form
    
        
      </div>
    </div> -->

    <!-- ============================================================= -->

    <!-- ================ Customer Counter ====================== -->

    <section class="customer_counter">
      <div class="black_filter">
        <div class="container">
          
        <div class="row">
              <br>
              <br>
             <div class="heading-block center">
              <h2>Why Choose Us ?</h2>
              <div class="col-md-4" style="margin-top: 20px;">
                <div class="col-md-3 choose_img">
                  <img src="{{ asset('images/front') }}/booking.png" alt="">
                </div>
                <div class="col-md-9">
                  <h4>Easy Booking</h4>
                  
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 20px;">
                <div class="col-md-3 choose_img">
                  <img src="{{ asset('images/front') }}/diamond.png" alt="">
                </div>
                <div class="col-md-9">
                  <h4>Excellent Service</h4>
                  
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 20px;">
                <div class="col-md-3 choose_img">
                  <img src="{{ asset('images/front') }}/car-wash.png" alt="">
                </div>
                <div class="col-md-9">
                  <h4>Additional Service</h4>
                  
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 20px;">
                <div class="col-md-3 choose_img">
                  <img src="{{ asset('images/front') }}/icon.png" alt="">
                </div>
                <div class="col-md-9">
                  <h4>Awesome Pricing</h4>
                  
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 20px;">
                <div class="col-md-3 choose_img ">
                  <img src="{{ asset('images/front') }}/green.png" alt="">
                </div>
                <div class="col-md-9">
                  <h4>Eco-Friendly</h4>
                  
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 20px;">
                <div class="col-md-3 choose_img">
                  <img src="{{ asset('images/front') }}/hand.png" alt="">
                </div>
                <div class="col-md-9">
                  <h4>Reliable &amp; Tranparent</h4>
                  
                </div>
              </div>
             {{--  <h2>We are <span>CAMPHOR</span></h2>
              <span>CAMPHOR SERVICES is changing the way people think about car cleaning. We provide easy online booking for various services for you car with just a few click. As per the customers need, we are providing "DAILY CAR WASH" service with highly trained professionals who will be manages by CAMPHOR SERVICES to provide quality services to all our customers. We believe your car deserves to be as healthy and toned as you are. We provide these services at affordable cost as we do care of your hard earned money. We are also providing ON DEMAND Services like Online Shopping, Car Servicing, Car Modification, Car Wrapping and many more in future. 
              </span>
              <br> --}}
            <h2>Our Happy Customers</h2>
            <div class="counter number">{{ 700+round((time()-1545224968)/28800) }}</div>
            </div> 
            <br>
            <br>
        </div>
        </div>
      </div>
    </section>
{{-- <div class="car" style="margin-top: -150px;">
    <img src="{{ asset('images') }}/cosy.png" style="width: 400px;" alt="">
</div>  --}}
<section class="dailycarwash">
    <div class="container">
  <div class="row">
      <div class="col-md-6">
        <img src="{{ asset('images') }}/about.png" style="width: 100%;padding-top: 150px;" alt="">
      </div>
      <div class="col-md-6">
        <div class="carwash">
          <h1>Daily Car Wash</h1>
          <p><strong style="max-width: 100%;">We offer a range of Daily car wash at your doorstep. All Services are performed by highly trained professionals. We have customized the packages keeping your needs in mind and have made it available at affordable prices.</strong></p>
         <center>
            <strong style="font-size: 20px;padding-right: 10px;">Cleaner</strong><img src="{{ asset('images/front') }}/arrow.png" style="margin-top:-5px">
            <strong style="font-size: 20px;padding-right: 10px; padding-left: 10px;">Safer</strong>
            <img src="{{ asset('images/front') }}/arrow.png" style="margin-top:-5px">
            <strong style="font-size: 20px;padding-left: 10px;">Better</strong>
            <br>
            <br><strong style="font-size: 20px;">6 Days a Week Service</strong>

          <div style="padding-left: 105px;">
                <ul style="">
                  <li style="text-align: left;"><strong >Exterior Car Wash With Water</strong></li>
                </ul>
             </div>   
            <strong style="font-size: 20px;padding-right:90px;">Once a week</strong>
          <div style="padding-left: 105px;">
        <ul style="">
          <li style="text-align: left;"><strong >Footmats Clean</strong></li>
          <li style="text-align: left;"><strong>Windows in and out clean</strong></li>
        </ul>
      </div>
          <br><br><a class="select_btn" href=" {{ url('package') }}" style="font-size: 20px;">Select Your Package</a>
        </center>
         <marquee style="margin-top: 35px;"><p><strong style="font-size: 20px;">Cars will be cleaned with a clean water along with microfiber cloth which saves the car paint from swirl marks.</strong> </p></marquee>
         <!--  <div class="daily_wash">
            @foreach($brands as $brand)
            <div>
              {{-- <div class="brand_logo"> --}}
                <img src="{{ asset('images/brand') }}/{{ $brand->brand_img }}" width="70" height="70" alt="">
              {{-- </div> --}}
            </div>
            @endforeach
          </div> -->
         
        </div>
      </div>
    </div>
  </div>
</section>
    <!-- =============== Request Callback ===================== -->

    <section class="req_callback">
        <div class="container">
            <div class="row">
                <h3>Request A Callback</h3>
                    <form method="post" role="form" class="form-inline" action='request/call_back'> 
                        {{ csrf_field() }}
                <div class="form_cont">
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="name">Name</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-user" aria-hidden="true"></i></div>
                              <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="email">E-mail</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div>
                              <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                            </div>
                        </div>
                        <div class="form-group col-md-4" style="text-align: center;">
                            <label class="sr-only" for="phone">Phone No.</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fas fa-mobile" aria-hidden="true"></i></div>
                              <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No.">
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="text-align: center;">
                            <label class="sr-only" for="phone">Your Query</label>
                            <div class="input-group" style="width: 90%; margin-top: 20px;">
                              <!-- <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div> -->
                              <!-- <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No."> -->
                              <textarea class="form-control" style="resize: none;" name="message" id="query" placeholder="Your Query"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                            <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                        </div>
                </div>
                    </form>
            </div>
        </div>
    </section>

    <!-- ========================================================== -->

    <!-- ======================= Review ========================== -->

    <section class="review">
        <div class="container">
            <div class="row">
              <br>
              <br>
              <br>
                <h3>Review/Feedback</h3>
                <hr style="width: 20%;">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt iusto aspernatur hic explicabo laboriosam id inventorenon, <br>eligendi consequuntur dolor minus officiis expedita deserunt vel corrupti! Repudiandae, unde!</p>

                <div class="rev_slide slider">
                    @foreach($feedbacks as $data)
                    <div>
                            <div class="col-md-3">
                                <img src="{{ asset('images/feedbacks').'/'.$data->pic }}" alt="">
                            </div>
                            <div class="col-md-9">
                                <h4 style="color: white;margin-top: 0;text-align: left;">{{ $data->name }}</h4>
                                <p style="text-align: left;">{{ $data->desc }}</p>
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection