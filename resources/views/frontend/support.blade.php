@extends('frontend/layouts.nav2')

@section('content')


<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="about" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;color: #183861;text-align: center;">Support</h1>
			<hr>
			
			<div class="col-sm-6" style="    padding: 50px;">
				<h3 style="font-family: 'Raleway', sans-serif;text-align: center;color: #183861;">Request A Callback</h3>
				<div class="form_cont">
					<form method="post" role="form" class="form-horizontal" action='request/call_back' style="text-align: center;"> 
						{{ csrf_field() }}
						<div class="form-group">
						    <label class="sr-only" for="name">Name</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fas fa-user" aria-hidden="true"></i></div>
						      <input type="text" class="form-control" name="name" id="name" placeholder="Name">
						    </div>
						</div>
						<div class="form-group">
						    <label class="sr-only" for="email">E-mail</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div>
						      <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
						    </div>
						</div>
						<div class="form-group">
						    <label class="sr-only" for="phone">Phone</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fas fa-mobile"></i></div>
						      <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No.">
						      <!-- <textarea class="form-control" style="resize: none;" name="query" id="query" placeholder="Your Query"></textarea> -->
						    </div>
						</div>
						<div class="form-group">
						    <label class="sr-only" for="phone">Your Query</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
						      <!-- <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No."> -->
						      <textarea class="form-control" style="resize: none;" name="message" id="query" placeholder="Your Query"></textarea>
						    </div>
						</div>
						<div class="col-md-12" style="text-align: center;margin-top: 20px;">
							<input type="submit" class="btn btn-primary" value="Request Callback">
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-6" style="    padding: 50px;">
				<h3 style="font-family: 'Raleway', sans-serif;text-align: center;color: #183861;">Raise A Complain</h3>
				<div class="form_cont">
					<form method="post" role="form" class="form-horizontal" action='complain/add_complain' style="text-align: center;">
						{{ csrf_field() }}						
						<div class="form-group">
						    <label class="sr-only" for="name">Name</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fas fa-user" aria-hidden="true"></i></div>
						      <input type="text" class="form-control" name="name" id="name" placeholder="Name">
						    </div>
						</div>
						<div class="form-group">
						    <label class="sr-only" for="email">E-mail</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div>
						      <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
						    </div>
						</div>
						<div class="form-group">
						    <label class="sr-only" for="phone">Phone</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fas fa-mobile" aria-hidden="true"></i></div>
						      <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No.">
						    </div>
						</div>
						<div class="form-group">
						    <label class="sr-only" for="phone">Your Query</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div>
						      <!-- <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No."> -->
						      <textarea class="form-control" style="resize: none;" name="message" id="query" placeholder="Your Query"></textarea>
						    </div>
						</div>
						<div class="col-md-12" style="text-align: center;margin-top: 20px;">
							<input type="submit" class="btn btn-primary" value="Register Complain">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ================================================= -->

<section>
	<div class="container">
		<div class="row">
			<!-- <div class="col-sm-4">
				<h3 style="font-family: 'Raleway', sans-serif;text-align: center;color: #183861;">Give Us A Miss-call <br> Tollfree</h3>
				<p style="text-align: center;font-size: 30px;font-family: 'Roboto', sans-serif;color: #11325d;font-weight: bold;">1800-000-000</p>
			</div> -->
			<div class="col-sm-6">
				<h3 style="font-family: 'Raleway', sans-serif;text-align: center;color: #183861;">Complaining E-mail Address</h3>
				<p style="margin-top: 35px;text-align: center;font-size: 30px;font-family: 'Roboto', sans-serif;color: #11325d;font-weight: bold;">complaint@camphorservices.com</p>
			</div>
			<div class="col-sm-6">
				<h3 style="font-family: 'Raleway', sans-serif;text-align: center;color: #183861;">Support E-mail Address</h3>
				<p style="margin-top: 35px;text-align: center;font-size: 30px;font-family: 'Roboto', sans-serif;color: #11325d;font-weight: bold;">support@camphorservices.com</p>
			</div>
		</div>
	</div>
</section>

<section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection