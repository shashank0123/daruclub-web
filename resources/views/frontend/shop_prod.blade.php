@extends('frontend/layouts.nav2')

@section('content')


<div class="jumbotron" style="background: url(http://localhost:8000/images/front/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Car Accessories</h1>
  
</div>
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;text-align: center; color: #183861;">Side Car Mirror</h1>
			<hr>
			<div style="margin:auto; width: 50%;">
			<!-- <h3 style="font-family: 'Raleway', sans-serif;text-align: center;color: #183861;">Request A Callback</h3> -->
			<div class="form_cont">
				<form action="{{ url('shop/ondemand_req') }}" method="POST" style="text-align: center;">
					{{ csrf_field() }}
					<div class="form-group">
					    <!-- <label class="sr-only" for="name">Name</label> -->
					    <select name="brand_id" required class="form-control brand" onChange="getmodel(this.value)" id="">
					    	<option value="">--Choose Your Brand--</option>
					    	@foreach($brands as $brand)
					    	<option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
					    	@endforeach
					    </select>
					    {{-- <input list="brand"  name="car_brand" class="form-control brand" placeholder="Choose Car Brand" required>
					    <datalist id="brand">
					    	@foreach($brands as $brand)
					    	<option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
					    	@endforeach
					    </datalist> --}}
					</div>
					<input type="hidden" name="prod_id" value="{{ $prod_id }}">
					<div id="model_data"></div>

					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="sr-only">Name</label> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                            <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                             @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="sr-only">E-mail</label> 
                        <div class="input-group">
                             <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                            <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                             @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="sr-only">Phone No.</label> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                             @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                        <label for="location" class="sr-only">Location</label> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-location" aria-hidden="true"></i></div>
                            <input id="location" type="text" class="form-control" name="location" placeholder="Enter Your location " required>
                             @if ($errors->has('location'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('location') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
					
					<div class="col-md-12" style="text-align: center;margin-top: 20px;">
						<input type="submit" class="btn btn-primary" value="Request On Demand">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<!-- ================================================= -->
<!-- ================================================= -->

<section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection