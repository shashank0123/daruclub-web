@extends('frontend/layouts.nav2')

@section('content')

	<!-- ========================================== -->

	<!-- ======================== Carousel ===================== -->
<style>
</style>
{{-- <div class="jumbotron" style="background: url(http://localhost:8000/images/front/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Car Accessories</h1>
  
</div> --}}
{{-- <div class="jumbotron" style=" background-size: 100% 100%; margin-top: -20px; padding: 0;">
  <h1 class="about_title" style="">Car Accessories (On Demand)</h1> --}}
<!-- <div class="jumbotron" style="background: url(http://localhost:8000/images/front/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Car Accessories</h1>
  
</div> -->

<div class="jumbotron" style=" background-size: 100% 100%; margin-top: -20px; padding: 0;">
  <h1 class="about_title" style="">Car Accessories <span>On Demand</span></h1>
  {{-- <hr style="width: 20%;"> --}}
</div>
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<!-- <h1 style="font-family: berlin;text-align: center;">Car Accessories On Demand</h1>
			<hr> -->
			<!-- Slider for Antenna -->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
					<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/8.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Antenna') }}/9.jpg" alt="">
						</div>
					</div>
					<br>
					<p style="text-align: center;">Antenna</p>
				</a>
			</div>
			<!-- End of slider for antenna -->

	<div id="modalRegister" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align-last: center">Request for Car Accessories on Demand</h4>
            </div>

            <form method="post" role="form" class="form-horizontal" action='complain/add_complain'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                 <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Product Category</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa">&#xf288;</i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Product Category (like Perfume, Side Mirror,etc)" required>
                                         @if ($errors->has('category'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('category') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!-- <label for="query">Your Query</label> -->
                                    <textarea name="message" class="form-control" id="query" cols="5" rows="3" placeholder="Your Query" style="resize: none;"></textarea>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                            </form>
                             <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button> --}}
                  </div>

            
           
        </div>
    </div>
</div>

			<!-- Slider for Aux Cable -->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Aux Cable') }}/Aux 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Aux Cable') }}/Aux 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Aux Cable') }}/Aux 3.jpeg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Aux Cable') }}/Aux 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Aux Cable') }}/Aux 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Aux Cable') }}/Aux 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Aux Cable') }}/Aux 8.jpg" alt="">
						</div>
					</div>
					<br>
					<p style="text-align: center;">Aux Cable</p>
				</a>
			</div>
			<!-- End of slider for Aux Cable -->

			<!-- Slider for Car Seat Cover -->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 1.jpeg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 2.png" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 3.jpeg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 8.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Cover') }}/Car Seat Cover 9.jpg" alt="">
						</div>
					</div>
					<br>
					<p style="text-align: center;">Car Seat Cover</p>
				</a>
			</div>
			<!-- End of slider for Car Seat Cover-->

			<!-- Slider for Car Seat Pillow -->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 8.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 9.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 10.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 11.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 12.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Car Seat Pillow') }}/Cushion 13.jpg" alt="">
						</div>
					</div>
					<br>
					<p style="text-align: center;">Car Seat Pillow</p>
				</a>
			</div>
			<!-- End of slider for Car Seat Pillow-->

			<!-- Slider for Caharger-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Charger') }}/Charger 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Charger') }}/Charger 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Charger') }}/Charger 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Charger') }}/Charger 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Charger') }}/Charger 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Charger') }}/Charger 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Charger') }}/Charger 7.jpg" alt="">
						</div>
					
					</div>
					<br>
					<p style="text-align: center;">Charger</p>
				</a>
			</div>
			<!-- End of slider for Charger-->

			<!-- Slider for LCD Screens-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 8.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 9.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/LCD Screens') }}/LCD 10.jpg" alt="">
						</div>
					
					</div>
					<br>
					<p style="text-align: center;">LCD Screens</p>
				</a>
			</div>
			<!-- End of slider for LCD Screens-->

			<!-- Slider for Lights-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 2.jpeg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 8.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 9.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 10.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Lights') }}/Light 11.jpg" alt="">
						</div>
					
					</div>
					<br>
					<p style="text-align: center;">Lights</p>
				</a>
			</div>
			<!-- End of slider for Lights-->

			<!-- Slider for Mobile Holder-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 5.jpeg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 8.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 9.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 10.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Mobile Holder') }}/Holder 11.jpg" alt="">
						</div>

					
					</div>
					<br>
					<p style="text-align: center;">Mobile Holder</p>
				</a>
			</div>
			<!-- End of slider for Mobile Holder-->

			<!-- Slider for Perfume-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Perfume') }}/Perfume 8.jpg" alt="">
						</div>
						
					
					</div>
					<br>
					<p style="text-align: center;">Perfume</p>
				</a>
			</div>
			<!-- End of slider for Perfume-->

			<!-- Slider for Roof rail-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Roof rail') }}/Roof rail 8.jpg" alt="">
						</div>
						
					
					</div>
					<br>
					<p style="text-align: center;">Roof rail</p>
				</a>
			</div>
			<!-- End of slider for Roof rail-->

			

			<!-- Slider for Side Beading-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Beading') }}/Side Beading 8.jpg" alt="">
						</div>
						
					</div>
					<br>
					<p style="text-align: center;">Side Beading</p>
				</a>
			</div>
			<!-- End of slider for Side Mirror-->

			<!-- Slider for Side Mirror-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Mirror') }}/1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Mirror') }}/2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Mirror') }}/3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Mirror') }}/4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Mirror') }}/5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Side Mirror') }}/6.jpg" alt="">
						</div>
					</div>
					<br>
					<p style="text-align: center;">Side Mirror</p>
				</a>
			</div>
			<!-- End of slider for Side Mirror-->

			<!-- Slider for Speakers-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 1.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 2.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 3.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 4.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 5.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 6.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 7.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 8.jpg" alt="">
						</div>
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all/Speakers') }}/Speaker 9.jpg" alt="">
						</div>
					</div>
					<br>
					<p style="text-align: center;">Speakers</p>
				</a>
			</div>
			<!-- End of slider for Speakers-->

			<!-- Slider for Speakers-->
			<div class="col-sm-3">
				<a href="#"  data-toggle="modal" data-target="#modalRegister">
									<div class="shop-product">
						<div>
							<img class="accessories_slide" src=" {{ asset('images/front/all') }}/wantmore.png" alt="">
							<p> If any other product you want to have please click here</p>
						</div>
						
					</div>
					<br>
					<!-- <p style="text-align: center;">Speakers</p> -->
				</a>
			</div>
			<!-- End of slider for Speakers-->


		</div>


	</div>
</section>

@endsection