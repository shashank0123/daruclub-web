@extends('frontend/layouts.nav2')
@section('content')
<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>

	

<div class="jumbotron" style=" background-size: 100% 100%; margin-top: -20px; padding: 0;">
  <h1 class="about_title" style="">About Camphor</h1>
  {{-- <hr style="width: 20%;"> --}}
</div>
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="about" style="padding: 30px;">
	
	<div class="container">

		<div class="row">
			<form method="post" role="form" class="form-horizontal col-md-6" style="background: white;" action='popup/signup'> 
                      <div class="modal-body">
                            <div class="form_cont">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="sr-only">Name</label> 
                                        <div class="input-group">                                            
                                            <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                             
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="sr-only">E-mail</label> 
                                        <div class="input-group">                                             
                                            <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="sr-only">Phone No.</label> 
                                        <div class="input-group">                                           
                                            <input id="phone" type="text" class="form-control" name="mobile" placeholder="Enter Your Phone No." required>
                                             
                                        </div>
                                    </div>                                    
                                    <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                            </div>
                      </div>
                      <div class="modal-footer">
                        {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button> --}}
                      </div>
                  </form>
			
	    </div>
					
				</div>

			</div>
		</div>
	</div>
</section>

@endsection