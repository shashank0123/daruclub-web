@extends('frontend/layouts.nav2')
@section('content')
<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>

	

<div class="jumbotron" style=" background-size: 100% 100%; margin-top: -20px; padding: 0;">
  <h1 class="about_title" style="">About Camphor</h1>
  {{-- <hr style="width: 20%;"> --}}
</div>
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="about" style="padding: 30px;">
	
	<div class="container">

		<div class="row">

			<div class="col-md-6">
				<h1  class="despasito" style="font-family: berlin;color: #183861;">About Camphor</h1>
				<hr>
				<p>Camphor Services is all about CARS like Daily Car Wash, Car Accessories, Car Servicing, etc.</p>
				<p>We are  providing new service "Daily Car Wash" on an online portal. Now, you can easily select your car make and model to have "Daily Car Wash" service at your home at affordable cost. We provide this service 6 days a week, every morning between 4am to 10am. We have skilled employees who will clean your car at your car parking. We assure you to serve quality services.</p>
				<p>Camphor Services is a brand which is literally going to change the way people think about car servicing, car cleaning. It's all about wonderful car service experience.
				</p>
				<p>Camphor Services is a "CHANGE” in automobile cleaning and online market. If you are still in doubt that CAMPHOR SERVICES is only about "Car Cleaning" - Think Again. We do a lot more than this like online shopping for Car Accessories, Online booking for Car Services, Car Modification, Car Wrapping. Moreover, our experts and fully trained staff has always used the best solutions to effectively and safely maintain the appearance of your automobile. We were among the first in India to offer such CAR ONLINE SERVICES which is designed in a way that we meet the requirement of the customers pertaining to both quality and timely delivery of products &amp; services.
				</p>
				<p>For almost all our services, on comparison, we are cheaper compared to multi-brand or authorized service centers. We achieve savings on labour charges, centralized (bulk) procurement of spare parts, no real-estate overheads and operational efficiency. These are passed on to our customers.</p>
				<p>At Camphor Services, we treat our every customer as our priority customer, and we assure to deliver Industry’s best cleaning and detailing solutions at reasonable rates.</p>
				<p>We have a strong belief that our efforts will not only please you, but will definitely surprise you and will make you experience the “CHANGE” in the cleaning Industry.</p>
				<p style="font-size: 20px;"><strong><marquee>Come to CAMPHOR SERVICES and feel the difference in your car to get the most of it</marquee></strong></p>
			</div>
			<div class="col-md-6">
				<h1 class="despasito" style="font-family: berlin;color: #183861;">Frequently Asked Questions (FAQ)</h1>
				<hr>
				<!-- <dl>
					<dt><b>1. How does it work?</b></dt>
					<dd>Choose our services and select your car vehicle make, model and variant. Since or prices are transparent, we have mentioned the cost for every services without any hidden cost. No last minute surprises. Camphor Provide affordable services at low cost with proffesional and expert employees. We work 7 days a week for Car Servicing and 6 days a week for Daily Car wash, Car accessories orders.</dd>
					<dt><b>2. Which Brands do we serve currently?</b></dt>
					<dd>We serve all major brands like  Maruti, Hyundai, Honda, Tata, Mahindra, Volkswagen and a lot more. For the complete list head to this link.</dd>
					<dt><b>3. Which area do we serve currently?</b></dt>
					<dd>Our Services are available in entire Delhi NCR, Gurugram, Noida depends on the number of customers.</dd>
					<dt><b>4. What does the package cost on the Website include?</b></dt>
					<dd>All packages on our site are accurate estimates. The cost includes all the services mentioned in the package. In addition to that it also includes all taxes & consumables that will be required. In short, the prices you see are the prices you pay and there will be no hidden charges.</dd>
					<dt><b>5. How do you pay to Camphor?</b></dt>
					<dd>You can choose to pay after selecting the available services. We accept all digital payment options. You can pay via debit/credit cards, netbanking and leading digital wallets like Paytm, Mobikwik, Freecharge, PayUmoney etc directly through our platform. You can also choose to pay via cash after selecting your services.</dd>
					<dt><b>6. Do I get a Bill?</b></dt>
					<dd>Yes, an invoice is sent to your registered email address on every service. You can also download a copy of the invoice by logging in and heading to the ‘My Bills’ section in your Camphor Dashboard.</dd>
					<dt><b>7. What if you are not happy with our services?</b></dt>
					<dd>We are committed to providing a high-quality, professional services to all our customers. If you are not satisfied with the service, we need you to tell us. We will do everything we can do to sort out your concerns, and respond positively to your comments. We will redo the service until we can get the desired results. Write to us at support@camphorservices.com or complaint@camphorservices.com and we’ll get in touch with you.
					</dd>
				</dl> -->

				<div class="bs-example">
				    <div class="panel-group" id="accordion">
				        <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span> 
				                    <b>How does it work?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseOne" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>Choose our services and select your car vehicle make, model and variant. Since or prices are transparent, we have mentioned the cost for every services without any hidden cost. No last minute surprises. Camphor Provide affordable services at low cost with proffesional and expert employees. We work 7 days a week for Car Servicing and 6 days a week for Daily Car wash, Car accessories orders.
				                    </p>
				                </div>
				            </div>
				        </div>
				        <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span>     <b>Which Brands do we serve currently?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseTwo" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>We serve all major brands like  Maruti, Hyundai, Honda, Tata, Mahindra, Volkswagen and a lot more. <a href="{{ url('brands') }}">For the complete list head to this link. </a></p>
				                </div>
				            </div>
				        </div>
				        <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> 
				                     <b>Which area do we serve currently?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseThree" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>Our Services are available in entire Delhi NCR, Gurugram, Noida depends on the number of customers.</p>
				                </div>
				            </div>
				        </div>
				         <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-plus"></span> 
				                    	<b>What does the package cost on the Website include?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseFour" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>All packages on our site are accurate estimates. The cost includes all the services mentioned in the package. In addition to that it also includes all taxes & consumables that will be required. In short, the prices you see are the prices you pay and there will be no hidden charges.
				                    </p>
				                </div>
				            </div>
				        </div>

				        <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-plus"></span> 
				                    <b> How do you pay to Camphor?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseFive" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>You can choose to pay after selecting the available services. We accept all digital payment options. You can pay via debit/credit cards, netbanking and leading digital wallets like Paytm, Mobikwik, Freecharge, PayUmoney etc directly through our platform. You can also choose to pay via cash after selecting your services.
				                    </p>
				                </div>
				            </div>
				        </div>

				        <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-plus"></span> 
				                    <b> Do I get a Bill?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseSix" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>Yes, an invoice is sent to your registered email address on every service. You can also download a copy of the invoice by logging in and heading to the ‘My Bills’ section in your Camphor Dashboard.
				                    </p>
				                </div>
				            </div>
				        </div>

				         <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><span class="glyphicon glyphicon-plus"></span> 
				                    <b> What if you are not happy with our services?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseSeven" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>We are committed to providing a high-quality, professional services to all our customers. If you are not satisfied with the service, we need you to tell us. We will do everything we can do to sort out your concerns, and respond positively to your comments. We will redo the service until we can get the desired results. Write to us at <b>support@camphorservices.com</b> or complaint@camphorservices.com and we’ll get in touch with you.
				                    </p>
				                </div>
				            </div>
				        </div>


				          <div class="panel panel-default">
				            <div class="panel-heading">
				                <h4 class="panel-title">
				                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><span class="glyphicon glyphicon-plus"></span> 
				                    <b> How we deal Complaints?</b>
				                    </a>
				                </h4>
				            </div>
				            <div id="collapseEight" class="panel-collapse collapse">
				                <div class="panel-body">
				                    <p>
				                    	<ul>
				                    		<li>We listen and understand customers concern for which you have raised the complaint.</li>
				                    		<li>We get the facts so that we can resolve your issue as quickly as possible.</li>
				                    		<li>We offer a solution with assurance so that you will not face this issue again.</li>
				                    		<li>We aim to resolve customers issue within 24-48 hours.</li>
				                    		<li>We always follow up to see if our customers are happy with the way compalint was handled.</li>
				                    		<li>We do not close the complaint without your permission, until it is resolved.</li>
				                    	</ul>
				                    </p>
				                </div>
				            </div>
				        </div>


				    </div>
					
				</div>

			</div>
		</div>
	</div>
</section>

@endsection