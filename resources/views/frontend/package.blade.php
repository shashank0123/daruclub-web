@extends('frontend/layouts.nav2')

@section('content')

<!-- <div class="jumbotron" style="background: url(img/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Packages</h1>
</div> -->
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

		<?php
			if (!isset($packageFormdata)) {
				

?>
<section class="pack_sec">
	<div class="container">
		<div class="col-md-6" style="margin-top:50px;">
			<div class="car_form">
				
			<h1 style="font-family: serif; color: #183861;">Select Your Car</h1>
			<form action="" method="POST" id="car_pack_form">
			 {{ csrf_field() }}
				{{-- <div class="form-group">
				    <!-- <label class="sr-only" for="name">Name</label> -->
				    <span class="custom-dropdown big">
				    <select name="brand" style="font-family: 'Rajdhani', sans-serif;text-transform: uppercase;" required onchange="getmodel(this.value)">    
				        <option style ="" selected="true" disable="disable" >Choose Your Brand</option>
				        @foreach($brands as $brand)
				    		<option style ="" value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
				    	@endforeach
				    </select>
				</span>
			</div> --}}
				<div class="form-group pack">
                            <div class="btn-group" style="width: 100%; padding: 1.0em;">
                                <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="Brand">
                                    <span class="multiselect-selected-text">Select Brand</span> <span style="margin-top: 3px;" class="fa fa-angle-down"></span>
                            	</button>
                                <ul class="multiselect-container dropdown-menu">
                                	@foreach($brands as $brand)
                                    <li>
                                        <a tabindex="0">
                                            <label class="radio"><input data-display="{{ $brand->brand_name }}" type="radio" name="brand" value="{{ $brand->id }}">{{ $brand->brand_name }}</label>
                                        </a>
                                                                                        
                                    </li>
                                    @endforeach
                                    
                                         
                                </ul>
                            </div>        
                        </div>
				<div class="form-group pack">
                            <div class="btn-group" style="width: 100%; padding: 1.0em;">
                                <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="Model">
                                    <span class="multiselect-selected-text">Select Model</span> <span style="margin-top: 3px;" class="fa fa-angle-down"></span>
                              </button>
                                <ul class="multiselect-container dropdown-menu">
                                  <div id="model_data"></div>
                                 {{--  @foreach($brands as $brand)
                                    <li>
                                        <a tabindex="0">
                                            <label class="radio"><input data-display="{{ $brand->brand_name }}" type="radio" name="brand" value="{{ $brand->id }}">{{ $brand->brand_name }}</label>
                                        </a>
                                                                                        
                                    </li>
                                    @endforeach --}}
                                    
                                         
                                </ul>
                            </div>        
                        </div> 
				<div class="form-group" style="text-align: center;">
					<input type="submit" class="btn btn-danger" value="Submit" style="width: 90%;">
				</div>	

			</form>
	     </div>
		</div>
		<div class="col-md-6" style="margin-top: 50px;">
			
		</div>
	</div>
</section>
		<?php } ?>

<section id="package_data" class="hidden-xs" style="color: #397ab7; background: black;">

	<?php
			if (isset($packageFormdata)) {
				

?>




<div class="container pack_content" style="text-align: center;">
	<h1 style="text-align: center;">Book Your Package</h1>
	<hr style="width: 20%;">
@foreach($packageFormdata as $data)
<div class="col-md-3">	
		
	<div class="panel package_tab" style="height: auto;">
		<div class="panel-body" style="padding: 0;">
		<a href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}">
			<div class="pck_img" style="background:white;">				 
				<img src="{{ asset('images/carmodels').'/'.$model_form_data->model_img}}" style="height: 250px; width: 100%;" alt="">				
			</div>
			<a class="buttons parallelogram" href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}" style="position: relative; top: -35px;">
				<span class="skew-fix"><i class="fas fa-rupee-sign"></i>{{ $data->package_price }}</span>
			</a>
			<h1 style="margin-top: -15px; margin-bottom: 10px">{{ $data->package_name }}</h1>
			<table class="table" style="width: 100%;">
				<tr style="border-bottom: 1px solid white;">
					<td>Package </td>
					<td>-</td>
					<td>{{ $data->package_type}} </td>
				</tr>

			</table>
			<div class="package_desc">
				
				<?php echo $data->package_desc; ?>

			</div>
			<a class="buttons parallelogram" style="margin-bottom: 20px;" href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}">
				<span class="skew-fix">Buy Now</span>
			</a>
		</div>
	</div>
	</a>
</div>

<div class="modal fade" id="m{{ $model_form_data->id }}" role="dialog" tabindex="-1" >
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ $data->package_name }}</h4>
        </div>
        <div class="modal-body def">
          <div class="col-md-3">	
	<a href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}">
		
	</a><div class="panel package_tab" style="height: auto;"><a href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}">
		</a><div class="panel-body" style="padding: 0;"><a href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}">
			<div class="pck_img" style="background:white;">
				 
				<img src="{{ asset('images/carmodels').'/'.$model_form_data->model_img}}" style="height: 250px; width: 100%;" alt="">
			</div>
			</a><a class="buttons parallelogram" href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}" style="position: relative; top: -35px;">
				<span class="skew-fix"><i class="fas fa-rupee-sign"></i> {{ $data->package_price }}</span>
			</a>
			<h1 style="margin-top: -15px; margin-bottom: 10px">{{ $data->package_name }}</h1>
			<table class="table" style="width: 100%;">
				<tbody><tr style="border-bottom: 1px solid white;">
					<td>Package </td>
					<td>-</td>
					<td>{{ $data->package_name }}</td>
				</tr>

			</tbody></table>
			<div class="package_desc">
				
				<?php echo $data->package_desc; ?>
			</div>
			<a class="buttons parallelogram" style="margin-bottom: 20px;" href="{{ url('book_package/'.$data->id.'/'.$model_form_data->id) }}">
				<span class="skew-fix">Buy Now</span>
			</a>
		</div>
	</div>
	
</div>
        </div>
       
      </div>
      
    </div>
  </div>

@endforeach
</div>

<div class="col-md-12 hidden-sm">
  <div class="row space">
  	@foreach($packageFormdata as $data)
    <div class="col-md-6 col-xs-6">
      <button class="btn btn-primary btn-round"data-toggle="modal" data-target="#m{{ $model_form_data->id }}">
                       {{ $data->package_name }}
      </button>
    </div>
@endforeach
  </div>	
</div>	
		<?php		
			}

	?>	
</section>


{{-- <section class="package_sec" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;text-align: center; color: #183861;">Packages for Daily Car Wash</h1>
			<hr>
			<div class="package_wrap">
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/hatchback')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/hatchback.png" alt="">
							</div>
							<div class="pack_text">
								<p>Hatchback</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/sedan ')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/sedan.png" alt="">
							</div>
							<div class="pack_text">
								<p>Sedan</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/suv ')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/suv.png" alt="">
							</div>
							<div class="pack_text">
								<p>SUV</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_wash/premium')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/premium.png" alt="">
							</div>
							<div class="pack_text">
								<p>Premium</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="package_sec" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;text-align: center; color: #183861;">Packages for Daily Car Servicing</h1>
			<hr>
			<div class="package_wrap">
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_servicing/hatchback')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/hatchback.png" alt="">
							</div>
							<div class="pack_text">
								<p>Hatchback</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_servicing/sedan')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/sedan.png" alt="">
							</div>
							<div class="pack_text">
								<p>Sedan</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_servicing/suv')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/suv.png" alt="">
							</div>
							<div class="pack_text">
								<p>SUV</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pack_card">
						<a href='{{ url('wash_pack/car_servicing/premium')}}'>
							<div class="pack_icon">
								<img src="{{ asset('images/front') }}/premium.png" alt="">
							</div>
							<div class="pack_text">
								<p>Premium</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ================================================= -->

<section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
			</div>
		</div>
	</div>
</section> --}}

@endsection