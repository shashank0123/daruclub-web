@extends('frontend/layouts.nav2')

@section('content')

	<!-- ======================== Carousel ===================== -->

<div class="jumbotron" style="background: url(http://localhost:8000/images/front/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Packages</h1>
</div>
<!-- ========================================================== -->

<!-- ===================== About ===================== -->




<section class="package_sec" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;text-align: center; color: #183861;">Our Packages</h1>
			<hr>
			<div class="package_wrap">
				<div class="col-md-10">
					@foreach($packages as $value)
					<div class="col-sm-4">
						<div class="circle_cont">
							<div class="center_p">
								<p>{{ $value->package_name }}</p>
								<div class="hidden_p">
									<p>{{ $value->package_desc }}</p>
									<p><i class="fas fa-rupee-sign"></i> {{ $value->package_price }}</p>
									<a href="#" class="btn btn-info">Book Your Package</a>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
				<div class="col-md-2">
					@foreach($cardata as $data)
						<img src="{{ asset('images/car').'/'.$data->car_img }}" style="width: 100%;" alt="">
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
@endsection