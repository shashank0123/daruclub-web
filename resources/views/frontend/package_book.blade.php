@extends('frontend/layouts.nav2')

@section('content')

<!-- <div class="jumbotron" style="background: url(img/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Packages</h1>
</div> -->
<!-- ========================================================== -->

<!-- ===================== About ===================== -->

<section class="package_sec" style="padding: 30px;">
	<div class="container">
		<div class="row">
			<h1 style="font-family: berlin;text-align: center; color: #183861;">Book your package</h1>
			<hr>
			<div class="book_package">
		        <p class="text-center" style="font-size: 20px;">Your Package Details</p>
		        <div class="plan_form">
		            <form method="post" action="/book_your_package">
		            	{{ csrf_field() }}
		                <ul class="plan_form">
		                    <li class="plan_form">
		                        <label for="data">Package Name</label>
		                        <input type="text" class="form-control" name="packagename"  disabled value="{{ $package_data->package_name }}">
		                    </li>
		                    <li class="plan_form">
		                        <label for="data">Price</label>
		                        <input type="text" class="form-control" name="package_price"  disabled value="{{ $package_data->package_price }}">
		                    </li>
		                    <li class="plan_form">
		                        <label for="data">Model</label>
		                        <input type="text" class="form-control" name="model_name"  disabled value="{{ $car_data->model_name }}">
		                    </li>
		                     <li class="plan_form">
		                        <label for="data">Brand</label>
		                        <input type="text" class="form-control" name="brand_name"  disabled value="{{ $car_data->brand_name }}">
		                    </li>
		                    <li class="plan_form">
		                        <label for="data">Address</label>
		                        <input type="text" class="form-control" required name="packageaddress" placeholder="Enter your address">
		                    </li>
		                    <li class="plan_form">
		                        <label for="data">Mobile</label>
		                        <input type="text" class="form-control" required name="mobile" value="{{Auth::user()->mobile}}">
		                    </li>
		                    <li class="plan_form">
		                      <label for="">Number Plate</label>
		                        <input type="text" class="form-control" required name="numberPlate" placeholder="Enter your vehicle number plate">
		                    </li>  
		                    <li class="plan_form">
		                      <label for="">Subscription Date</label>
				                <div class='input-group date' id='datetimepicker1'>
				                    <input type='text' name="subscription" value="01-09-2018" readonly class="form-control" />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
		                    </li>
		                    <li class="plan_form">
		                      <label for="">Time Period</label>
		                      <!-- <textarea name="purpose" class="form-control" required="" id="" cols="10" rows="3"></textarea> -->
		                      <select name="period" class="form-control">
		                      	<option value="3:30 AM - 3:45 AM">3:30 AM - 3:45 AM</option>
		                      	<option value="3:45 AM - 4:00 AM">3:45 AM - 4:00 AM</option>
		                      	<option value="4:00 AM - 4:15 AM">4:00 AM - 4:15 AM</option>
		                      	<option value="4:15 AM - 4:30 AM">4:15 AM - 4:30 AM</option>
		                      	<option value="4:30 AM - 4:45 AM">4:30 AM - 4:45 AM</option>
		                      	<option value="4:45 AM - 5:00 AM">4:45 AM - 5:00 AM</option>
		                      	<option value="5:00 AM - 5:15 AM">5:00 AM - 5:15 AM</option>
		                      	<option value="5:15 AM - 5:30 AM">5:15 AM - 5:30 AM</option>
		                      	<option value="5:30 AM - 5:45 AM">5:30 AM - 5:45 AM</option>
		                      	<option value="5:45 AM - 6:00 AM">5:45 AM - 6:00 AM</option>
		                      	<option value="6:00 AM - 6:15 AM">6:00 AM - 6:15 AM</option>
		                      	<option value="6:15 AM - 6:30 AM">6:15 AM - 6:30 AM</option>
		                      	<option value="6:30 AM - 6:45 AM">6:30 AM - 6:45 AM</option>
		                      	<option value="6:45 AM - 7:00 AM">6:45 AM - 7:00 AM</option>
		                      	<option value="7:00 AM - 7:15 AM">7:00 AM - 7:15 AM</option>
		                      	<option value="7:15 AM - 7:30 AM">7:15 AM - 7:30 AM</option>
		                      	<option value="7:30 AM - 7:45 AM">7:30 AM - 7:45 AM</option>
		                      	<option value="7:45 AM - 8:00 AM">7:45 AM - 8:00 AM</option>
		                      	<option value="8:00 AM - 8:15 AM">8:00 AM - 8:15 AM</option>
		                      	<option value="8:15 AM - 8:30 AM">8:15 AM - 8:30 AM</option>
		                      	<option value="8:30 AM - 8:45 AM">8:30 AM - 8:45 AM</option>
		                      	<option value="8:45 AM - 9:00 AM">8:45 AM - 9:00 AM</option>
		                      	<option value="9:00 AM - 9:15 AM">9:00 AM - 9:15 AM</option>
		                      	<option value="9:15 AM - 9:30 AM">9:15 AM - 9:30 AM</option>
		                      	<option value="9:30 AM - 9:45 AM">9:30 AM - 9:45 AM</option>
		                      	<option value="9:45 AM - 10:00 AM">9:45 AM - 10:00 AM</option>
		                      	<option value="10:00 AM - 10:15 AM">10:00 AM - 10:15 AM</option>
		                      	<option value="10:15 AM - 10:30 AM">10:15 AM - 10:30 AM</option>
		                      	<option value="10:30 AM - 10:45 AM">10:30 AM - 10:45 AM</option>
		                      	<option value="10:45 AM - 11:00 AM">10:45 AM - 11:00 AM</option>
		                      	<option value="11:00 AM - 11:15 AM">11:00 AM - 11:15 AM</option>
		                      	<option value="11:15 AM - 11:30 AM">11:15 AM - 11:30 AM</option>
		                      	
		                      </select>
		                    </li>
		                </ul>
		                <input type="hidden" name="package_price"  value="{{ $package_data->package_price }}">
		                <input type="hidden" name="package_id" value="{{ $package_data->id }}">
		                <input type="hidden" name="model_id" value="{{ $car_data->id }}">
		                <input type="hidden" name="brand_id" value="{{ $car_data->brand_id }}">
		                <div class="sub_pay">
		                   <input type="submit" class="button" value="Pay Now"> 
		                </div>
		            </form>
		        </div>   
		    </div>
		</div>
	</div>
</section>


<!-- ================================================= -->

<section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
			</div>
		</div>
	</div>
</section>

@endsection