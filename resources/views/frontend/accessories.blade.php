@extends('frontend/layouts.nav2')

@section('content')
{{-- <div class="car">
    <img src="{{ asset('images') }}/cosy.png" style="width: 400px;" alt="">
</div> --}}
	<section class="choose_brand">
		<div class="container">
			<div class="row">
				<h1 style="font-family: berlin;color: #183861;text-align: center;">Choose Car Brand</h1>
				<hr>
				@foreach($brands as $data)
				<div class="col-md-3 col-sm-6 col-xs-12" style="height: 200px">
					<div class="brand-icon">
						<a href=""><img src="{{ asset('images/brand').'/'.$data->brand_img }}" alt=""></a>
					</div>
				</div>
				@endforeach
				{{-- <div class="col-md-3 col-sm-6 col-xs-12" style="height: 200px">
					<div class="brand-icon">
						<a href=""><img src="{{ asset('images/front') }}/jaguar.png" alt=""></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12" style="height: 200px">
					<div class="brand-icon">
						<a href=""><img src="{{ asset('images/front') }}/BMW-logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12" style="height: 200px">
					<div class="brand-icon">
						<a href=""><img src="{{ asset('images/front') }}/jaguar.png" alt=""></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12" style="height: 200px">
					<div class="brand-icon">
						<a href=""><img src="{{ asset('images/front') }}/BMW-logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12" style="height: 200px">
					<div class="brand-icon">
						<a href=""><img src="{{ asset('images/front') }}/jaguar.png" alt=""></a>
					</div>
				</div> --}}
			</div>
		</div>
	</section>
	<section class="car-slider" style="margin-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="car_slide">
					<div>
						<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
					</div>
					<div>
						<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
					</div>
					<div>
						<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
					</div>
					<div>
						<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
					</div>
					<div>
						<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
					</div>
					<div>
						<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
					</div>
					<div>
						<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
					</div>
				</div>
			</div>
		</div>
	</section>

	
@endsection