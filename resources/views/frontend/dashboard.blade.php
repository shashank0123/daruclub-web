@extends('frontend/layouts.nav2')

@section('content')


	<section class="user_dashboard" style="margin-top: 30px; margin-bottom: 100px;">
  

<div class="dash_container container">

  <div class="row">

    <div class="user_dash_head" style="text-align: center;">

      <h1>Your Profile</h1>
     

      <hr>

    </div>

        <div role="tabpanel">

            <div class="col-sm-3">

                <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">

                    <li role="presentation" class="brand-nav active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;&nbsp;Your Profile</a></li>

                    <li role="presentation" class="brand-nav" ><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;&nbsp;Your Orders</a></li>

                    <!-- <li role="presentation" class="brand-nav "><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>&nbsp;&nbsp;Buyer List</a></li> -->

                    <li role="presentation" class="brand-nav"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"><i class="fab fa-google-wallet"></i>&nbsp;&nbsp;Wallet</a></li>

                    <li role="presentation" class="brand-nav"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>&nbsp;&nbsp;Setting</a></li>

                </ul>

            </div>


            <div class="col-sm-9">

         

             

                <div class="tab-content">

                 <div role="tabpanel" class="tab-pane active" id="tab1">

                  <form method="post" action="update/user_profile">
                     {{ csrf_field() }}

                              <div class="col-md-6">

                                

                                <div class="form-group ">

                                  <label for="first_name">First Name</label>

                        <input type="name" class="pr_edit form-control" name="firstname"  disabled value="{{ $user_data->firstname }}">

                                </div>

                                <div class="form-group ">

                                  <label for="first_name">Last Name</label>

                        <input type="name" class="pr_edit form-control" name="lastname"  disabled value="{{ $user_data->lastname }}">

                                </div>



                                <div class="form-group ">

                                  <label for="email">Email Address</label>

                                  <input type="email" class="pr_edit form-control" name="email" disabled value="{{ $user_data->email}}">

                                </div>

                                <div class="form-group ">

                                  <label for="mobile">Mobile No</label>

                                  <input type="number" class="pr_edit form-control" name="phone" disabled value="{{ $user_data->mobile }}">

                                </div>

                                <div id="sub_bt" style="display: none;">

                                  <button type="submit" class="btn btn-success">Submit</button>

                                </div>

                              </div>

                            </form>

                          <button class="btn btn-primary pull-right" id="edit">Edit</button>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab2" style="border: 1px solid #ccc; padding: 10px; border-radius: 5px;">

                       <table class="table" id="product_table">

                        <thead>

                         <tr>

                           <th>Package Name</th>

                           <th>Amount</th>
                           <th>Package Type</th>

                           <th>Car Type</th>
                           <th>Status</th>
                           <th>Action</th>

                         </tr>

                        </thead>

                            <tbody>



                            

                          @foreach ($carwasherquest as $element)
                          <tr>
                            
                              <td>{{$element->package_name}}</td>
                              <td>{{$element->package_price}}</td>
                              <td>{{$element->package_category}}</td>
                              <td>{{$element->car_type}}</td>
                              <?php $status = "Paid"; if ($element->payment_status == 'Pay Later') $status = 'Pending';?>
                              <td>{{$status}}</td>
                          </tr>
                          @endforeach

                        </tbody>

                       </table>

                         

                    </div>

                    {{-- <div role="tabpanel" class="tab-pane" id="tab3">

                        <div class="buyer_table">

                          <table id="" class="table table-striped table-bordered">

                              <tbody>

                                <tr>

                                  <th>Name Of Buyer</th>

                                  <th>Produt Name</th>

                                  <th>Product Image</th>

                                  <th>Total item</th>

                                  <th>Product Description</th>

                                  <th>Total Payment</th>

                                </tr>

                                <tr>

                                  <td>Ajay</td>

                                  <td>Fastrack Watch</td>

                                  <td>Not available</td>

                                  <td>1</td>

                                  <td>Denim shade fastrack watch</td>

                                  <td>1599</td>

                                </tr>


                              </tbody> 

                          </table>

                        </div>

                    </div> --}}

                    <div role="tabpanel" class="tab-pane" id="tab4">
                        <div class="panel-group">
                          <div class="panel panel-default">
                            <div class="wallet_amount" style="padding: 10px;">
                              <h2>Your Amount :<i class="fa fa-rupee-sign"> &nbsp; {{ $amount }}</i></h2>
                            </div>
                            <hr>                          
                              <h4 style="padding-left: 10px;" class="head">Add Money To Wallet</h4>
                         <form method= "post" action="wallet/add_money" style="padding: 20px; height: 200px;">
                          {{ csrf_field() }}
                            <div class="col-md-6">
                              <div class="form-group ">
                                <div>
                                  <label for="mobile">Amount *</label>
                                </div>
                                <input type="text" name="amount" class="form-control" value="" required>
                              </div>
                              <button type="submit" class="btn btn-primary">Add Amount</button>
                            </div>
                          </form>
                        </div>    

                          </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab5">

                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                          <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingOne">

                              <h4 class="panel-title">

                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Change Password</a>

                              </h4>


                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

                              <div class="panel-body">

                      <form method="post" action="update/password">
                                  {{ csrf_field() }}

                                  <div class="col-md-6">

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Old Password *</label>

                                      </div>

                                      <input type="password" name="current-password" class="form-control" value="" required>

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">New Password *</label>

                                      </div>

                                      <input type="password" name="new-password" class="form-control" value="">

                                    </div>

                                    <div class="form-group ">

                                      <div>

                                        <label for="mobile">Confirm New Password *</label>

                                      </div>

                                    <input type="password" name="confirm_password" class="form-control" value="">

                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                  </div>

                                </form>

                              </div>

                            </div>

                          </div>
                         <!--  <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingThree">

                              <h4 class="panel-title">

                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

                                  Delete Account

                                </a>

                              </h4>

                            </div>

                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">

                              <div class="panel-body text-center">

                                <a href="" class="btn btn-danger">Delete Account</a>

                              </div>

                            </div>

                          </div> -->

                        </div>

                    </div>

                </div>

            </div>

        </div>

  </div>

</div>

</section>





@endsection