@extends('frontend/layouts.nav2')

@section('content')

<!-- <div class="jumbotron" style="background: url(img/bmw02.jpg); background-size: 100% 100%;height: 400px;">
  <h1 style="text-align: center;color: white;line-height: 3;">Packages</h1>
</div> -->
<!-- ========================================================== -->
<div class="jumbotron" style="padding: 0; margin: 0;">
	<img src="{{ asset('images/front') }}/contact.png" alt="contact" class="img-responsive">
</div>
<!-- ===================== About ===================== -->
<style>
	.contact .input-group-addon {
    background: white;
    /* border-right: 2px solid #11325d; */
}
</style>
<section class="contact" style="background: #213439; padding: 50px;">
	<div class="container">
        <div class="col-md-4">
            <button id="complain_btn" class="btn" data-toggle="modal" data-target="#complain_modal">Raise A Complaint</button>

            <!-- Modal -->
            <div class="modal fade" id="complain_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Raise A Complaint</h4>
                  </div>
                             <form method="post" role="form" class="form-horizontal" action='complain/add_complain'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!-- <label for="query">Your Query</label> -->
                                    <textarea name="message" class="form-control" id="query" cols="5" rows="3" placeholder="Your Query" style="resize: none;"></textarea>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                            </form>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button> --}}
                  </div>
                </div>
              </div>
            </div>
            <!-- end of modal -->
        </div>


        <div class="col-md-4">
            <button id="service_btn" class="btn" data-toggle="modal" data-target="#modal-2">Request For Service In Your Area</button>
            
            <!-- Modal -->
            <div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Request For Service</h4>
                  </div>
                  <form method="post" role="form" class="form-horizontal" action='request/add_request'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="sr-only">city</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-location" aria-hidden="true"></i></div>
                                        <input id="city" type="text" class="form-control" name="city" placeholder="Enter Your city" required>
                                         @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> --}}
                  </div>
              </form>
                </div>
              </div>
            </div>
        </div>
        <!-- Start of request a callback model from -->
        <div class="col-md-4">
            <button id="complain_btn" class="btn" data-toggle="modal" data-target="#callback_modal">Request A Callback</button>

            <!-- Modal -->
            <div class="modal fade" id="callback_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Request A Callback</h4>
                  </div>
                             <form method="post" role="form" class="form-horizontal" action='request/call_back'> 
                  <div class="modal-body">
                        <div class="form_cont">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">Name</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-mail</label> 
                                    <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="sr-only">Phone No.</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                                         @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!-- <label for="query">Your Query</label> -->
                                    <textarea name="message" class="form-control" id="query" cols="5" rows="3" placeholder="Your Query" style="resize: none;"></textarea>
                                </div>
                                <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                        </div>
                  </div>
                            </form>
                  <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button> --}}
                  </div>
                </div>
              </div>
            </div>
            <!-- end of modal -->
        </div>
        <!-- End of request of callback of modal form -->

        
      </div>
      <br>
      <br>
      <br>
	<div class="container">
		<div class="row">
			<h1 style="text-align: center;color: white;">Contact Us</h1>
			<hr style="width: 20%;">
			<div class="form_cont col-md-6">
				<form action="submit_contact" method="post">
					{{ csrf_field() }}
					<div class="form-group col-md-12">
					    <label class="sr-only" for="name">Name</label>
					    <div class="input-group">
					      <div class="input-group-addon" style="background: white; border-right: none;"><i class="fas fa-user" aria-hidden="true"></i></div>
					      <input type="text" class="form-control" name="name" id="name" placeholder="Name">
					    </div>
					</div>
					<div class="form-group col-md-12">
					    <label class="sr-only" for="email">E-mail</label>
					    <div class="input-group">
					      <div class="input-group-addon" style="background: white; border-right: none;"><i class="fas fa-envelope" aria-hidden="true"></i></div>
					      <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
					    </div>
					</div>
					<div class="form-group col-md-12">
					    <label class="sr-only" for="phone">Phone No.</label>
					    <div class="input-group">
					      <div class="input-group-addon" style="background: white; border-right: none;"><i class="fas fa-mobile" aria-hidden="true"></i></div>
					      <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No.">
					    </div>
					</div>
					<div class="form-group col-md-12">
					    <label class="sr-only" for="phone">Your Query</label>
					    <div class="input-group" style="width: 100%;">
					      <div class="input-group-addon" style="background: white; border-right: none;"><i class="fas fa-envelope" aria-hidden="true"></i></div>
					      <!-- <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone No."> -->
					      <textarea class="form-control" style="resize: none;" name="message" id="query" placeholder="Your Query"></textarea>
					    </div>
					</div>
					<div class="col-md-12" style="text-align: center;margin-top: 20px;">
						<input type="submit" class="btn btn-primary" value="Submit">
					</div>
				</form>
			</div>
			<div class="col-md-6 maps">
		          		
            	<h4 style="text-align: center; letter-spacing: 2px;">CAMPHOR SERVICES :</h4>
            	<hr style="width: 20%;">
	            <div class="col-md-6">
	            	<h4>Phone</h4>
	            	<hr>
	            	 <p>+91 93548 98031</p>
	            </div>
	            <div class="col-md-6">
	            	<h4> Email Us</h4>
	            	<hr>
	              <p>contact@camphorservices.com </p>
	              
		        </div>
      		</div>
		</div>
	</div>
</section>

<!-- ================================================= -->

{{-- <section class="car-slider">
	<div class="container">
		<div class="row">
			<div class="car_slide">
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
				<div>
					<img src="{{ asset('images/front') }}/car-wrap.png" alt="" class="img-responsive" style="width: 80%;margin: auto;">
				</div>
			</div>
		</div>
	</div>
</section> --}}

@endsection