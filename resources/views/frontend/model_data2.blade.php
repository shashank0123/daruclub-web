<div class="form-group">
				    <!-- <label class="sr-only" for="name">Name</label> -->
    <select name="model" class="form-control" required > 
    	<option selected="true" disable="disable" >Choose Your Model</option>   
        @foreach($data as $model)
			<option value="{{ $model->id }}">{{ $model->model_name }}</option>
			@endforeach
    </select>
</div>