<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="0s_dOxGVmbDaSQ_Ssi3SusSHlSPUhfp-ViqkAEHLO0I" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <?php $title = "Camphor Services";
    $tags = "";
    $keywords = "";
    $meta = "";
    
    if (session()->get('keywords')){
      $keywords = session()->get('keywords');
      $title = session()->get('title');
      $tags = session()->get('tags');
      $meta = session()->get('meta');
    }
    ?>
    <title>{{$title}}</title>
    <meta name="tags" content="{{$tags}}">
    <meta name="description" content="{{$meta}}">        
    <meta name="keywords" content="{{$keywords}}" />

    <title>{{ config('nav.name', 'Camphor') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend.css') }}" rel="stylesheet">
    <link href="{{ asset('css/new-respnsive.css') }}" rel="stylesheet">
    <script src="bower_components/sweetalert/src/sweetalert.js"></script>
    <link rel="stylesheet" href="bower_components/sweetalert/src/sweetalert.css">
  
    <link href="https://fonts.googleapis.com/css?family=Raleway|Roboto" rel="stylesheet">  
<style>
body{
  
overflow-x:hidden !important; 
  
  
}
@media(min-width:319px) and (max-width:425px){

body{
overflow-x:hidden !important; 
width:401px;  
  
}



}

</style>

</head>
<body>
    <div id="app">

       <nav class="navbar top-nav ">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ asset('images/front/logo.png') }}" width="200" class="img-responsive" id="img_desktop">
      </a>
      <a href="{{ url('/')}}">
      </a>
       
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div>
        <ul class="nav navbar-nav navbar-right" id="login">
          @if (Auth::guest())
            <li class="active"><a href="{{ url('login') }}" class="login_b"><i class="fas fa-user-circle" aria-hidden="true"></i>&nbsp;Log In<span class="sr-only">(current)</span></a></li>
             <li class="active"><a href="{{ url('register') }}" class="register_b"><i class="fas fa-user-plus" aria-hidden="true"></i>&nbsp;Register<span class="sr-only">(current)</span></a></li>
                       @else
                      <li><a href="{{ url('home') }}"> My Account </a></li>
                       <li class="dropdown">
                               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fas fa-user-circle"></i>

                                   {{ Auth::user()->firstname }}{{Auth::user()->lastname }} <span class="caret"></span>
                               </a>
       
                               <ul class="dropdown-menu" role="menu" style="width: 100%;">
                                
                                   <li>
                                       <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                           Logout
                                       </a>
       
                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           {{ csrf_field() }}
                                       </form>
                                   </li>
                               </ul>
                           </li>
                       @endif
                       
                       
            <!-- Modal -->
                <div class="modal fade signup" id="modal-5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
              
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                  </div>
                                  <div class="modal-body">
                                    <div class="panel panel-login">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <a href="#" class="active" id="login-form-link" class="login_b">Login</a>
                                            </div>
                                            <div class="col-xs-6">
                                                <a href="#" id="register-form-link" class="register">Register</a>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                               
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
        </ul>
    </div>
    <div>
        <ul class="navbar-right social">
            <li>
                <a href="" role="button">
                    <img height="47px" src="images/front/play.png" class="google_play" alt="Download From Google Play">
                </a>
            </li>
            <li class="fb_list">
                <a href="{{ asset('images/front/play.png') }}"><i class="fab fa-facebook" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href=""><i class="fab fa-google-plus" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href=""><i class="fab fa-instagram" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href=""><i class="fab fa-youtube" aria-hidden="true"></i></a>
            </li>
      
        </ul>
    </div>
  </div><!-- /.container -->
</nav>
      
   

<!-- =========== Navigation Ends =============  -->

<!-- ============ Service Nav Starts ==============  -->

<nav class="navbar service-navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span style="color: white;">Service Menu</span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse shift service-navigation-items" id="bs-example-navbar-collapse-1">
      <ul class="nav nav-pills nav-justified">
        <li><a href="{{ url('/') }}">Home<span class="sr-only">(current)</span></a></li>
        <li><a href="{{ url('about') }}">About Us</a></li>
        <li><a href="{{ url('brands') }}">Packages</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">On Demand <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ url('car_accessories') }}">Car Accessories</a></li>
            <li><a href="{{ url('car_servicing') }}">Car Servicing</a></li>
            <li><a href="{{ url('car_modification') }}">Car Modification</a></li>
            <li><a href="{{ url('car_wrap') }}">Car Wrappping</a></li>
          </ul>
        </li>
        {{-- <li><a href="{{ url('accessories') }}">Car Accessories</a></li> --}}
        <li><a href="{{ url('contact_us') }}">Contact Us</a></li>
        {{-- <li><a href="{{ url('support') }}">Support</a></li> --}}
      </ul>
    </div> <!-- .navbar-collapse -->
  </div> <!-- container-fluid -->
</nav>

        @yield('content')
    </div>

<footer class="container-fluid" style="background: aliceblue; padding-bottom: 150px;">
    <div class="container">
      <div class="row">
            <ul class="footer-links">

                <li class="col-xs-6 col-sm-4 col-md-2 col-md-offset-1">
                    <ul style="padding: 0;">
                        <li class="title">About Us</li>
                        <li><a href="/about_us"> Company</a></li>
                        <li><a href=""> Sitemap</a></li>
                    </ul>
                </li>

                <li class="col-xs-6 col-sm-4 col-md-2">
                    <ul style="padding: 0;">
                        <li class="title">Support</li>
                        <li><a href="{{ url('about') }}"> FAQs</a></li>
                        <li><a href="{{ url('contact_us') }}"> Contact Us</a></li>
                        <li><a href="{{ url('terms') }}"> Terms & Conditions</a></li>
                        <li><a href="{{ url('login') }}"> Customer Login</a></li>
                    </ul>
                </li>

                <li class="col-xs-6 col-sm-4 col-md-2">
                    <ul style="padding: 0;">
                        <li class="title">Our Services</li>
                        <li><a href="{{ url('package') }}"> Daily Car Wash</a></li>
                        <li><a href="{{ url('car_servicing') }}"> Car Servicing</a></li>
                        <li><a href="{{ url('car_accessories') }}"> Online Car Shop</a></li>
                        <li><a href="{{ url('car_modification') }}"> Car Modification</a></li>
                        <li><a href="{{ url('car_wrap') }}"> Car Wrapping</a></li>
                    </ul>
                </li>

                <li class="col-xs-6 col-sm-4 col-md-2">
                    <ul style="padding: 0;">
                          <li class="title">Major Brands</li>
                          <li><a href="javascript:void(0)">Maruti</a></li>
                          <li><a href="javascript:void(0)">Honda</a></li>
                          <li><a href="javascript:void(0)">Hyundai</a></li>
                          <li><a href="javascript:void(0)">Ford</a></li>
                          <li><a href="javascript:void(0)">Toyota</a></li>
                          <li><a href="javascript:void(0)">Skoda</a></li>
                          <li><a href="javascript:void(0)">Chevrolet</a></li>
                          <li><a href="javascript:void(0)">Mahindra</a></li>
                    </ul>
                </li>

                <li class="col-xs-6 col-sm-4 col-md-2 ">
                    <ul style="padding: 0;">
                        <li class="title">Premium Brands</li>
                        <li><a href="javascript:void(0)"> Mercedes</a></li>
                        <li><a href="javascript:void(0)"> BMW</a></li>
                        <li><a href="javascript:void(0)"> Audi</a></li>
                        <li><a href="javascript:void(0)"> Volvo</a></li>
                        <li><a href="javascript:void(0)"> Mitsubishi</a></li>
                        <li><a href="javascript:void(0)"> Landrover</a></li>
                        <li><a href="javascript:void(0)"> Jaguar</a></li>
                    </ul>
                </li> 
                <li class="stretch"></li>
            </ul>
        </div>
    <div class="car1">
      <marquee direction="right" scrollamount="20" class="qus">
    <img src="{{ asset('images') }}/cosy.png" style="width: 400px;" alt="">
    </marquee>
</div>    
</footer>
<footer style="padding: 40px 0 0 ;">
  
  <div class="container">
    
        <div class="row">
            <div>
                <ul class="social-link">
                    <li>
                        <a href="https://www.facebook.com/camphorservices"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href=""><i class="fab fa-google-plus" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href=""><i class="fab fa-instagram" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href=""><i class="fab fa-youtube" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
  </div>
            <!-- <div class="address">
                <address>
                    4th Floor, Plot No. 94, Dwarka Sector 13, <br>
                    Opposite Dwarka Sector 13 Metro Station, Near Raddison Blu, <br>
                    New Delhi - 110078 <br>
                    Phone: +91-9876543210
                </address>
            </div> -->
        </div>
    </div>
    <div class="container">
      
      <div class="row">
          <p><i class="fa fa-copyright" aria-hidden="true"></i> &nbsp; Copyright 2017. Camphor Services.
          <span class="">
            <a href="{{ url('terms') }}">Terms &amp; Conditions</a> | 
            <a href="{{ url('policy') }}">Policy</a>
          </span>
          </p>
      </div>
    </div>
</footer>
<div class="modal" id="popupmodal" tabindex="-1" role="dialog" aria-labelledby="popupmodallabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="background: #183861; color: white; font-family: 'Times New Roman', Times, serif;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h1 style="color: white;font-size: xx-large;" class="modal-title" id="myModalLabel">Be Smart! Save Money</h1>
                  </div>
                  <div style="background: url('/images/popup/popup.png')">
                    <div class="col-md-6" style="background: white; height: 231px;">
                      <div class="container" >
                        <h2 style="color: #183861; font-family: 'Times New Roman', Times, serif;">Book Daily Car Wash</h2>
                        <h2 style="color: #183861; font-family: 'Times New Roman', Times, serif;">Starts @350*</h2>
                        <h4>Register Now &</h4>
                        <h4>Pay after 25th December To Get </h4>
                        <h4>Professional Services</h4>
                      </div>
                    </div>
                    
                      <form method="post" role="form" id="popupform" class="form-horizontal col-md-6" style="background: white;" action='popup/signup'> 
                      <div class="modal-body">
                            <div class="form_cont">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="sr-only">Name</label> 
                                        <div class="input-group">                                            
                                            <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                             
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="sr-only">E-mail</label> 
                                        <div class="input-group">                                             
                                            <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="sr-only">Phone No.</label> 
                                        <div class="input-group">                                           
                                            <input id="phone" type="text" class="form-control" name="mobile" placeholder="Enter Your Phone No." required>
                                             
                                        </div>
                                    </div>                                    
                                    <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                                        <input id="sendpopup" type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                            </div>
                      </div>
                      <div class="modal-footer">
                        {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button> --}}
                      </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
    <!-- Scripts -->
    {{-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="{{ asset('js/jquery-3.1.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

        <script src="js/jquery.flexslider-min.js" defer></script><!-- FlexSlider JS Plugin -->
            <script src="js/afterglow.min.js" defer></script><!-- Video Player JS Plugin -->

    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/wow.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ asset('js/panel.js') }}"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"></link>
</body>
<script>
    $('.fuel_t').on('click', function () {
    $('.fuel_type .dropdown-menu li input[type="radio"]').attr('checked', !1);
    $(this).find('input[type="radio"]').attr('checked', !0);
    var val = $(this).find('input[type="radio"]').val();
    var display = $(this).find('input[type="radio"]').attr('data-display');
    $(this).parents('.btn-group').find('.multiselect-selected-text').text(display);
    target_element = $(this);
  });
</script>
<script>
  var msg = '{{Session::get('message')}}';
  var exist = '{{Session::has('message')}}';
  if(exist){
    alert(msg);
  }
  $('.modal-backdrop').remove();
</script>
 <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
            <?php if (!session()->get('pop', false)  || isset(auth()->user()->id)) { ?>
            setTimeout(function() {
                            $('#popupmodal').modal();
                        }, 6000);

            $("#sendpopup").click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "popup/signup",
                    data: $("#popupform").serialize(),
                    success: function(result) {
                      swal({
                        title: "Success", 
                          text: result.message, 
                          type: "success"
                      });
                        
                    },
                    error: function(result) {
                        alert('error');
                    }
                });
            });
                    <?php }  ?>
        </script>
</html>
