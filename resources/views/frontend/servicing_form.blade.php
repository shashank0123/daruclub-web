<div class="panel">
    
<div class="form_cont panel-body">
    <h1 style="text-align: center;">Request For Car Servicing</h1>
    <hr style="width: 20%;">
                <form action="{{ url('shop/ondemand_req') }}" method="POST" style="text-align: center;">
                    {{ csrf_field() }}

                    <input type="hidden" name="brand_id" value="{{ $brand_id }}">
                    <input type="hidden" name="model_id" value="{{ $model_id }}">
                    <input type="hidden" name="fuel_type" value="{{ $fuel_type }}">

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="sr-only">Name</label> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fas fa-user" aria-hidden="true"></i></div>
                            <input id="name" type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                             @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="sr-only">E-mail</label> 
                        <div class="input-group">
                             <div class="input-group-addon"><i class="fas fa-envelope" aria-hidden="true"></i></div>
                            <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Mail id" required>
                             @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="sr-only">Phone No.</label> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Your Phone No." required>
                             @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="sr-only">Enter Your Address</label> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fas fa-map-marker" aria-hidden="true"></i></div>
                            <input id="address" type="text" class="form-control" name="address" placeholder="Enter Your Address " required>
                             @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-12" style="text-align: center;margin-top: 20px;">
                        <input type="submit" class="btn btn-primary" value="Request On Demand">
                    </div>
                </form>
            </div>
</div>