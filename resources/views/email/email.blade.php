<h5>Dear Customer,<h5>

<span>Thank you for signing up with Camphor Services. We are delighted to have you with us and look forward to serving you with the best online Car Services experience.</span>

<span>To be able to sign in to your account, please verify your email address first by clicking the following link:</span>
<a href="{{ url('/verifyemail/'.$email_token) }}">click here</a>

If you have any concerns or questions please contact us at 9354898031, or write to us at contact@camphorservices.com .

Cheers!
Team Camphor Services

