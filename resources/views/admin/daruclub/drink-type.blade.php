@extends('layouts/admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="panel panel-default"><br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel-heading">Drink Management</div>
                    </div>
                    <div class="col-sm-4">
                        <a href="/admin/add-drink" class="btn btn-success">Add Drink </a>
                    </div>
                    <div class="col-sm-4">
                        <form>
                            <input type="search" class="btn btn-success " style="width: 70%" placeholder="Enter here">&nbsp;<span class="btn btn-success"><i class="fa fa-search " style="font-size: 15px"></i></span>
                        </form>
                    </div>
                </div>
                

                <div class="panel-body">
                    @if ($message = Session::get('message'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    
                    <br>
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0;?>
                            @foreach ($drinks as $key => $drink)
                            
                            <tr class="list-users">
                                <td>{{ ++$i }}</td>
                                <td>{{ $drink->name }}</td>
                                <td><img src="{{ URL::to('/') }}/daru-assets/images/Admin/Drinks/{{ $drink->image }} " width="150px"  /></td>
                                <td>
                                   <label class="label label-success"><?php if($drink->status == '1'){echo 'Active';} else {echo 'Inactive';} ?></label>
                                        {{-- @if(!empty($drink->roles))
                                            @foreach($drink->roles as $role)
                                                <label class="label label-success">{{ $role->display_name }}</label>
                                            @endforeach
                                            @endif --}}
                                        </td>
                                        <td>
                                            {{-- <a class="btn btn-info" href="{{ route('drinks.show',$drink->id) }}">Show</a> --}}
                                            <a class="btn btn-primary" href="/admin/edit-drink/{{$drink->id }}">Edit</a>

                                            <form action="/admin/drink-type/{{$drink->id}}" method="GET" style="display: inline-block">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-task-{{ $drink->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @endsection