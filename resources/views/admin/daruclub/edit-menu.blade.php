@extends('layouts/admin')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<div class="container" width="100%">
	<div class="row">
		<div class="col-md-12 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Bar Management</div>
				<div class="panel-body">
					<a href="/admin/menu-list"><button type="submit" class="btn btn-success">Back</button> </a><br><br>

					@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</div>
					@endif

					@if($message = Session::get('message'))
					<div class="alert alert-primary">
						<p>{{ $message }}</p>
					</div>
					@endif
					
					<form action="/admin/edit-menu/{{$menu->id}}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{-- @csrf --}}

						{{-- Row 1 --}}

						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Item : </label>
							</div>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control" id="name" value="{{$menu->name}}">
							</div>
						</div>

						
						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Bar : </label>
							</div>
							<div class="col-md-9">
								<select name="bar_id" class="form-control">
									@foreach($bars as $bar)
									<option value="{{$bar->id}}" <?php $menu->bar_id == $bar->id ? 'selected':null ?>>{{$bar->name}}</option>
									@endforeach
								</select>
								{{-- <input type="text" name="bar_id" class="form-control" id="bar_id"> --}}
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Price : </label>
							</div>
							<div class="col-md-9">
								<input type="text" name="price" class="form-control" id="price" value="{{$menu->price}}">
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Availability : </label>
							</div>
							<div class="col-md-9">
								<select name="availability" class="form-control">
									<option value="1" <?php $menu->availability == '1' ? 'selected':null?> >In Stock</option>
									<option value="0" <?php $menu->availability == '0' ? 'selected':null?> >Out of Stock</option>
								</select>
								{{-- <input type="text" name="availability" class="form-control" id="availability"> --}}
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Image : </label>
							</div>
							<div class="col-md-9">
								<input type="file" name="image" class="form-control" id="image">
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Status : </label>
							</div>
							<div class="col-md-9">
								<select name="status" class="form-control">
									<option value="1" <?php $menu->status == 1 ? 'selected':null?> >Active</option>
									<option value="0" <?php $menu->status == 0 ? 'selected':null?> >Inactive</option>
								</select>								
							</div>
						</div>
					

						<button type="submit" class="btn btn-success">Submit</button>
					{{-- </div> --}}
				</div>
			</form>

		</div>
	</div>
</div>
</div>
</div>



@endsection