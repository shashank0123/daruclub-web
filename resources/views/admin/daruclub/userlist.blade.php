@extends('layouts/admin')

@section('content')
<style>
</style>
<div class="container" width="100%">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="panel panel-default"><br>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel-heading">User Management</div>
                    </div>
                    <div class="col-sm-6">
                        <form>
                            <input type="search" class="btn btn-success " style="width: 70%" placeholder="Enter here">&nbsp;<span class="btn btn-success"><i class="fa fa-search " style="font-size: 15px"></i></span>
                        </form>
                    </div>
                </div>

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                        {{-- <div class="container">
                        	<div class="row"> --}}

                        		{{-- <a href="/admin/add-user" class="btn btn-success">New User</a> --}}
                        		{{-- <form><input type="search" class="btn btn-success" placeholder="Enter here"><i class="fa fa-search btn-success" style="font-size: 20px"></i> </form>
                        	</div>
                        </div> --}}
                        <br>
                        <table class="table table-striped table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roles</th>
                                    {{-- <th>Action</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0;?>
                                @foreach ($users as $key => $user)
                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                    	<label class="label label-success">{{ $user->user_type }}</label>
                                        {{-- @if(!empty($user->roles))
                                            @foreach($user->roles as $role)
                                                <label class="label label-success">{{ $role->display_name }}</label>
                                            @endforeach
                                            @endif --}}
                                        </td>
                                        {{-- <td> --}}
                                            {{-- <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a> --}}
                                            {{-- <a class="btn btn-primary" href="/admin/edit-user/{{$user->id }}">Edit</a>

                                            <form action="/admin/user-list/{{$user->id}}" method="POST" style="display: inline-block">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-task-{{ $user->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        @endsection