@extends('layouts/admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="panel panel-default"><br>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel-heading">Bar Management</div>
                    </div>
                    <div class="col-sm-4">
                        <a href="/admin/add-bar" class="btn btn-success">New Bar </a>
                    </div>
                    <div class="col-sm-4">
                        <form>
                            <input type="search" class="btn btn-success " style="width: 70%" placeholder="Enter here">&nbsp;<span class="btn btn-success"><i class="fa fa-search " style="font-size: 15px"></i></span>
                        </form>
                    </div>
                </div>                

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    
                  <br>
                  <table class="table table-striped table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Bar Name</th>
                            <th>Rating</th>
                            <th>Timimg</th>
                            <th>Total Seats</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=0;?>
                        @foreach ($bars as $key => $bar)
                        
                        <tr class="list-users">
                            <td>{{ ++$i }}</td>
                            <td>{{ $bar->name }}</td>
                            <td>{{ $bar->rating }}</td>
                            <td>
                               <label class="label label-success">{{ $bar->open_time }}</label>
                                        {{-- @if(!empty($bar->roles))
                                            @foreach($bar->roles as $role)
                                                <label class="label label-success">{{ $role->display_name }}</label>
                                            @endforeach
                                            @endif --}}
                                        </td>
                                        <td>{{ $bar->total_seats }}</td>
                                        <td><img src="{{ URL::to('/') }}/daru-assets/images/Admin/Bars/{{ $bar->image }} " width="150px"  /></td>
                                        <td><?php if($bar->status == '1'){echo 'Active';} else {echo 'Inactive';} ?></td>
                                        <td>
                                            {{-- <a class="btn btn-info" href="{{ route('bars.show',$bar->id) }}">Show</a> --}}
                                            <a class="btn btn-primary" href="/admin/edit-bar/{{$bar->id }}">Edit</a>

                                            <form action="/admin/bar-list/{{$bar->id}}" method="GET" style="display: inline-block">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-task-{{ $bar->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @endsection