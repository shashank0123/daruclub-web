@extends('layouts/admin')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<div class="container" width="100%">
	<div class="row">
		<div class="col-md-12 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Bar Management</div>
				<div class="panel-body">
					<a href="/admin/bar-list"><button type="submit" class="btn btn-success">Back</button> </a><br><br>

					@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</div>
					@endif

					@if($message = Session::get('message'))
					<div class="alert alert-primary">
						<p>{{ $message }}</p>
					</div>
					@endif
					
					<form action="/admin/add-bar" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{-- @csrf --}}

						{{-- Row 1 --}}

						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Bar Name : </label>
							</div>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control" id="name">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Bar Description : </label>
							</div>
							<div class="col-md-9">
								<textarea name="bar_desc" class="form-control" id="bar_desc"></textarea><br>  
								<script>
									CKEDITOR.replace( 'bar_desc' );
								</script>
								{{-- <input type="text" name="bar_desc" class="form-control" id="bar_desc"> --}}
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Rating : </label>
							</div>
							<div class="col-md-9">
								<input type="text" name="rating" class="form-control" id="rating">
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Timing : </label>
							</div>
							<div class="col-md-9">
								<input type="text" name="open_time" class="form-control" id="open_time">
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Total Seats : </label>
							</div>
							<div class="col-md-9">
								<input type="text" name="total_seats" class="form-control" id="total_seats">
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Status : </label>
							</div>
							<div class="col-md-9">
								<select name="status" class="form-control">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>								
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Image : </label>
							</div>
							<div class="col-md-9">
								<input type="file" name="image" class="form-control" id="image">
							</div>
						</div>


						

						<button type="submit" class="btn btn-success">Submit</button>
					{{-- </div> --}}
				</div>
			</form>

		</div>
	</div>
</div>
</div>
</div>



@endsection