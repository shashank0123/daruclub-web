@extends('layouts/admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="panel panel-default"><br>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel-heading">Bookibgs</div>
                    </div>
                    
                    <div class="col-sm-6">
                        <form>
                            <input type="search" class="btn btn-success " style="width: 70%" placeholder="Enter here">&nbsp;<span class="btn btn-success"><i class="fa fa-search " style="font-size: 15px"></i></span>
                        </form>
                    </div>
                </div> 

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    
                    <br>
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User</th>
                                <th>Bar</th>
                                <th>Item</th>
                                <th>No. of seats</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach ($bookings as $key => $booking)
                            
                            <tr class="list-users">
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                                <td>6</td>
                                <td>7</td>
                                <td>8
                                   
                                        {{-- @if(!empty($user->roles))
                                            @foreach($user->roles as $role)
                                                <label class="label label-success">{{ $role->display_name }}</label>
                                            @endforeach
                                            @endif --}}
                                        </td>
                                        <td>
                                            {{-- <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a> --}}
                                            <a class="btn btn-primary" href="/admin/edit-user/{{$user->id }}">Edit</a>

                                            <form action="/admin/user-list/{{$user->id}}" method="POST" style="display: inline-block">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-task-{{ $user->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @endsection