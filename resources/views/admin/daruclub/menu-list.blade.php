<?php
use App\Bar;
?>


@extends('layouts/admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="panel panel-default"><br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel-heading">Menu Management</div>
                    </div>
                    <div class="col-sm-4">
                        <a href="/admin/add-menu" class="btn btn-success">New Item </a>
                    </div>
                    <div class="col-sm-4">
                        <form>
                            <input type="search" class="btn btn-success " style="width: 70%" placeholder="Enter here">&nbsp;<span class="btn btn-success"><i class="fa fa-search " style="font-size: 15px"></i></span>
                        </form>
                    </div>
                </div> 
                <div class="panel-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    
                    <br>
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Bar</th>
                                <th>Price</th>
                                <th>Availability</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0;?>
                            @foreach ($menus as $key => $menu)
                            
                            <tr class="list-users">
                                <td>{{ ++$i }}</td>
                                <td>{{ $menu->name }}</td>
                                <td><?php $bar = Bar::where('id',$menu->bar_id)->first();?>
                                    {{$bar->name}}
                                </td>
                                <td>Rs. {{ $menu->price }}</td>
                                <td><?php if($menu->availability == '1'){echo 'In Stock';} else {echo 'Out of Stock';} ?></td>
                                <td><img src="{{ URL::to('/') }}/daru-assets/images/Admin/Menu/{{ $menu->image }} " width="100px" height="100px" /></td>
                                
                                <td>
                                   <?php if($bar->status == '1'){echo 'Active';} else {echo 'Inactive';} ?>
                               </td>
                               <td>
                                {{-- <a class="btn btn-info" href="{{ route('menus.show',$menu->id) }}">Show</a> --}}
                                <a class="btn btn-primary" href="/admin/edit-menu/{{$menu->id }}">Edit</a>

                                <form action="/admin/menu-list/{{$menu->id}}" method="GET" style="display: inline-block">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit" id="delete-task-{{ $menu->id }}" class="btn btn-danger">
                                        <i class="fa fa-btn fa-trash"></i>Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>
</div>


@endsection