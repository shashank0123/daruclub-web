@extends('layouts/admin')

@section('content')


<div class="container" width="100%">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Drink Management</div>
				<div class="panel-body">
					<a href="/admin/drink-type"><button type="submit" class="btn btn-success">Back</button> </a><br><br>

					@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</div>
					@endif

					@if($message = Session::get('message'))
					<div class="alert alert-primary">
						<p>{{ $message }}</p>
					</div>
					@endif
					
					<form action="/admin/add-drink" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						

						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Drink Name : </label>
							</div>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control" id="name">
							</div>
						</div>

						
						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Status : </label>
							</div>
							<div class="col-md-9">
								<select name="status" class="form-control">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>								
							</div>
						</div>


						<div class="form-group row">
							<div class="col-md-3">
								<label for="name">Image : </label>
							</div>
							<div class="col-md-9">
								<input type="file" name="image" class="form-control" id="image">
							</div>
						</div>						

						<button type="submit" class="btn btn-success">Submit</button>
						
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>

@endsection