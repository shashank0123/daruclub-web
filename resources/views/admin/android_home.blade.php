@extends('layouts.'.Auth::user()->user_type)


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Banner Images</h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>URL</th>
            <th>Picture</th>
            <th>Intent</th>
            <th>Type</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
    	@foreach ($android_homes as $data)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $data->banner_url }}</td>
            <td>{{ $data->banner_image }}</td>
            <td>{{ $data->intent }}</td>
            <td>{{ $data->type }}</td>
            
            <td>
                {{-- {{ link_to_route('data.edit','Edit',[$data->id], ['class'=> 'btn btn-primary btn-xs']) }} --}}

                    {{-- {!! Form::open(['method'=>'delete','route'=>['complain_destroy', $data->id]]) !!} --}}

                {{-- {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!}{!! Form::close() !!}  --}}
           </td>
        </tr>
        <?php $i++; ?>
        @endforeach
       
    </tbody>
</table>                    
@endsection

