@if (Auth::user()->name == 'care')
    @extends('layouts.care')
@else
    @extends('layouts.'.Auth::user()->user_type)
@endif
@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Userlist</h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif

<h2>Verified Emails</h2>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>User Type</th>
            <th>Creation date</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach ($userlists as $data)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $data->firstname }}</td>
            <td>{{ $data->lastname }}</td>
            <td>{{ $data->mobile }}</td>
            <td>{{ $data->email }}</td>
            <td>{{ $data->user_type }}</td>
            <td>{{ $data->created_at }}</td>
            
            <td>
                {{-- {{ link_to_route('data.edit','Edit',[$data->id], ['class'=> 'btn btn-primary btn-xs']) }} --}}

                    {{-- {!! Form::open(['method'=>'delete','route'=>['complain_destroy', $data->id]]) !!} --}}

                {{-- {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!} --}}
           </td>
        </tr>
        <?php $i++; ?>
        @endforeach
       
    </tbody>
</table>                    
<h2>Unverified emails</h2>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>User Type</th>
            <th>Creation date</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach ($unverified as $data)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $data->firstname }}</td>
            <td>{{ $data->lastname }}</td>
            <td>{{ $data->mobile }}</td>
            <td>{{ $data->email }}</td>
            <td>{{ $data->user_type }}</td>
            <td>{{ $data->created_at }}</td>
            
            <td>
                {{-- {{ link_to_route('data.edit','Edit',[$data->id], ['class'=> 'btn btn-primary btn-xs']) }} --}}

                    {{-- {!! Form::open(['method'=>'delete','route'=>['complain_destroy', $data->id]]) !!} --}}

                {{-- {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!} --}}
           </td>
        </tr>
        <?php $i++; ?>
        @endforeach
       
    </tbody>
</table>                   
<h2>Signups</h2>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>filling date</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach ($signup as $data)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $data->name }}</td>
            <td>{{ $data->mobile }}</td>
            <td>{{ $data->email }}</td>
            <td>{{ $data->created_at }}</td>
            
            <td>
                {{-- {{ link_to_route('data.edit','Edit',[$data->id], ['class'=> 'btn btn-primary btn-xs']) }} --}}

                    {{-- {!! Form::open(['method'=>'delete','route'=>['complain_destroy', $data->id]]) !!} --}}

                {{-- {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!} {!! Form::close() !!} --}}
           </td>
        </tr>
        <?php $i++; ?>
        @endforeach
       
    </tbody>
</table>   
@endsection

