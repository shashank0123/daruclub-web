@extends('layouts.'.Auth::user()->user_type)


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Add Banner Images</h4>
@if(isset($success))
    <div class="alert alert-success"> {{$success}} </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(isset($androidhome))
    {{ Form::model($androidhome, ['route' => ['androidhomes.update', $accessories->id],'class'=>'form-horizontal', 'files' => true, 'method' => 'patch']) }}
@else

    {!!  Form::open(array('route' =>'androidhomes.store','class'=>'form-horizontal','files' => true )) !!}

@endif
                                        
                                        
                <div class="form-group">
                    {!! Form::label('title','URL :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {!! Form::text('banner_url',Input::old('banner_url'),['class'=>'form-control'])!!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('title','Image :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {!! Form::file('banner_image',['class'=>'form-control','multiple'],Input::old('banner_image')) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('title','Intent :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {!! Form::text('intent',Input::old('intent'),['class'=>'form-control'])!!}
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::label('title','Type :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {{ Form::select('type', 
                                        [ 'banner' => 'Banner', 
                                          'grid' => 'Grid',
                                          'rsa' => 'RSA',
                                    ],Input::old('type'),['class'=>'form-control'] ) }}
                    </div>
                </div>
                	
               
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-5">
                        {!! Form::submit('submit',array('class' => 'btn btn-primary'))!!}
                    </div>
                </div>


            {!! Form::close() !!}                  
                                         


                        

                    
@endsection


