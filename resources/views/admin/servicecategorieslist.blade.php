@extends('layouts.dashboardtemp')
@include('menu.adminmenu')

@section('content')
<h4 class="header-title m-t-0 m-b-30">Work Categories</h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Category Name</th>
            <th>Description</th>
            <th>Feature</th>
            <th>Views</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
    	@foreach ($data as $client)
        <tr>
            <td>{{ $client->name }}</td>
            <td>{{ $client->description }}</td>
            <td>{{ $client->feature }}</td>
            <td>{{ $client->views }}</td>
            <td>
                {{ link_to_route('workcategories.edit','Edit',[$client->id], ['class'=> 'btn btn-primary btn-xs']) }}

                {{ link_to_route('workcategorydetails.edit','Details',[$client->id], ['class'=> 'btn btn-primary btn-xs']) }}

                    {!! Form::open(['method'=>'delete','route'=>['workcategories.destroy', $client->id]]) !!}

                {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!}
           </td>
        </tr>
        @endforeach
       
    </tbody>
</table>                    
@endsection

