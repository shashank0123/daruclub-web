@extends('layouts.dashboardtemp')
@include('menu.adminmenu')

@section('content')
<div class="page-wrapper pa-0 ma-0 auth-page">
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <h3 class="text-center txt-dark mb-10">Add New Category</h3>
                                <h6 class="text-center nonecase-font txt-grey">Enter details below</h6>
                            </div>  
                            <div class="form-wrap">
                                @if(isset($category))
                                    {{ Form::model($category, ['route' => ['workcategories.update', $category->id],'class'=>'form-horizontal', 'method' => 'patch']) }}
                                @else

                                    {!!  Form::open(array('route' =>'workcategories.store','class'=>'form-horizontal' )) !!}

                                @endif
                                                                        
                                                                        
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            {!! Form::label('title','Category Name :',['class'=>'control-label mb-10'])!!}
                                            {!! Form::text('name', old('name'),['class'=>'form-control'])!!}
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>    
                                       
                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            {!! Form::label('title','Description :',['class'=>'control-label mb-10'])!!}
                                            {!! Form::textarea('description', old('description'),['class'=>'form-control'])!!}
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('parent_category') ? ' has-error' : '' }}">
                                            {!! Form::label('title','Parent Category :',['class'=>'control-label mb-10'])!!}
                                            {!! Form::select('parent_category', $categories, ['class'=>'form-control'])!!}
                                            @if ($errors->has('parent_category'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('parent_category') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        

                                        <div class="form-group text-center">
                                            {!! Form::submit('submit',array('class' => 'btn btn-success btn-rounded'))!!}
                                        </div>


                                    {!! Form::close() !!}  
                                     </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->   
    </div>
    
</div>                  
@endsection

