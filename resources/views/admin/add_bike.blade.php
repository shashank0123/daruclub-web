@extends('layouts.'.Auth::user()->user_type)


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Add Bike</h4>
@if(isset($success))
    <div class="alert alert-success"> {{$success}} </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(isset($bikes))
    {{ Form::model($bikes, ['route' => ['bikes.update', $bikes->id],'class'=>'form-horizontal', 'files' => true, 'method' => 'patch']) }}
@else

    {!!  Form::open(array('route' =>'bikes.store','class'=>'form-horizontal','files' => true )) !!}

@endif
                                        
<?php 
    $select = [];
    $select[0]='SELECT';
        foreach($brands as $brands){
            $select[$brands->id] = $brands->brand_name;
        }




 ?>
                                        
                <div class="form-group">
                    {!! Form::label('title','Brand Name :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                 		 {{ Form::select('brand_id', $select , null, ['class'=>'form-control','onchange' => 'getmodel(this.value)'] ) }}

                        {{-- {!! Form::text('brand_name',Input::old('brand_name'),['class'=>'form-control'])!!} --}}
                    </div>
                </div>
                <div id="model_data"></div>
                <div class="form-group">
                    {!! Form::label('title','Bike Category :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {{ Form::select('bike_category', 
                        				[ 'hatchback' => 'Hatchback', 
                        				  'sedan' => 'Sedan',
                        				  'suv' => 'Suv',
                        				  'premium' => 'Premium'
                        				],Input::old('bike_category'),['class'=>'form-control'] ) }}
                    </div>
                </div>
                {{-- <div class="form-group">
                    {!! Form::label('title','Bike Model :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {!! Form::text('bike_model',Input::old('bike_model'),['class'=>'form-control'])!!}
                    </div>
                </div> --}}
                <div class="form-group">
                    {!! Form::label('title','Description :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {!! Form::text('bike_desc',Input::old('bike_desc'),['class'=>'form-control'])!!}
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::label('title','Bike Image :',['class'=>'col-sm-3 control-label'])!!}
                    <div class="col-sm-6">
                        {!! Form::file('bike_img',Input::old('bike_img'),['class'=>'form-control'])!!}
                    </div>
                </div>	
               
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-5">
                        {!! Form::submit('submit',array('class' => 'btn btn-primary'))!!}
                    </div>
                </div>


            {!! Form::close() !!}                         

                    

                    
@endsection