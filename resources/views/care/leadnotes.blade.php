@extends('layouts.'.Auth::user()->user_type)
@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Leads </h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif


<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr> 
            <th>#</th>
            <th>Note</th>
            <th>Creation date</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>
                
            </td>
            <td><form class="form-horizontal" action="/care/addnotes" method="POST">{{ csrf_field() }}<input type="hidden" name="lead_id" value="{{ $id }}"><textarea class="form-control" name="notes" id="" width="100%"></textarea></td>
            <td>
                <input class="form-control btn btn-success" type="submit" value="Add Note">
            </form>
            </td>
        </tr>
        <?php $i=1; ?>
        @foreach ($notes as $data)
        <tr>
            <td>{{ $i }}</td>
            {{-- <td>{{ $data->firstname }}</td> --}}
            <td><span>{{ $data->notes }}</span></td>
            <td>{{ $data->created_at }}</td>
            
            
        </tr>
        <?php $i++; ?>
        @endforeach
       
    </tbody>
</table>                    
                
@endsection

                  

