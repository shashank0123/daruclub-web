@extends('layouts.'.Auth::user()->user_type)
@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Leads <a href="/care/leads/create" class="btn btn-primary">Create Lead</a></h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif


<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr> 
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Creation date</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach ($leads as $data)
        <tr>
            <td>{{ $i }}</td>
            {{-- <td>{{ $data->firstname }}</td> --}}
            <td>{{ $data->name }}</td>
            <td>{{ $data->lastname }}</td>
            <td>{{ $data->email }}</td>
            <td>{{ $data->mobile }}</td>
            <td>{{ $data->created_at }}</td>
            
            <td><a class="btn btn-primary" href="/care/leadnotes/{{ $data->id }}">Notes</a>
                {{ link_to_route('leads.edit','Edit',[$data->id], ['class'=> 'btn btn-primary btn-xs']) }}

                    {{-- {!! Form::open(['method'=>'delete','route'=>['complain_destroy', $data->id]]) !!} --}}

                {{-- {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!} --}}
           </td>
        </tr>
        <?php $i++; ?>
        @endforeach
       
    </tbody>
</table>                    
                
@endsection

                  

