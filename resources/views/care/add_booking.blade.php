@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Add Car Wash Booking </h4>
@if(isset($success))
    <div class="alert alert-success"> {{$success}} </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif  <form action="/admin/book_car_wash" method="POST" class="form-horizontal" >
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $id }}">

        


        <div class="form-group">
            {!! Form::label('title','Brand Name :',['class'=>'col-sm-3 control-label'])!!}
            <div class="col-sm-6">
                {{ Form::select('brand_id1', 
                                $brands,Input::old('brand_id'),['class'=>'form-control', 'id' => 'brand1'] ) }}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title','Model Name :',['class'=>'col-sm-3 control-label'])!!}
            <div class="col-sm-6">
                {{ Form::select('model_id1', 
                                $carmodels,Input::old('model_id'),['class'=>'form-control', 'id' => 'model1'] ) }}
            </div>
        </div>

        

        <div class="form-group">
            {!! Form::label('title','Package Name :',['class'=>'col-sm-3 control-label'])!!}
            <div class="col-sm-6">
                {{ Form::select('package_id1', 
                                $packages,Input::old('package_id'),['class'=>'form-control'] ) }}
            </div>
        </div>
        
        
      
       
        <div class="form-group">
            {!! Form::label('title','Address :',['class'=>'col-sm-3 control-label'])!!}
            <div class="col-sm-6">
                {{ Form::text('address',Input::old('brand_id'),['class'=>'form-control'] ) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6 col-sm-offset-5">
                {!! Form::submit('submit',array('class' => 'btn btn-primary'))!!}
            </div>
        </div>


    {!! Form::close() !!}                  
                        


                        


                        

                    
@endsection


