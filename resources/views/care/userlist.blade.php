@extends('layouts.'.Auth::user()->user_type)
@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">User Details <a href="/care/createbooking/{{ $userlists->id }}" class="btn btn-primary">Create Booking</a></h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>User Type</th>
            <th>Creation date</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $userlists->firstname }}</td>
            <td>{{ $userlists->lastname }}</td>
            <td>{{ $userlists->mobile }}</td>
            <td>{{ $userlists->email }}</td>
            <td>{{ $userlists->user_type }}</td>
            <td>{{ $userlists->created_at }}</td>
            
            <td>
                {{-- {{ link_to_route('data.edit','Edit',[$data->id], ['class'=> 'btn btn-primary btn-xs']) }} --}}

                    {{-- {!! Form::open(['method'=>'delete','route'=>['complain_destroy', $data->id]]) !!} --}}

                {{-- {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!} --}}
           </td>
        </tr>
        <?php $i++; ?>
     
       
    </tbody>
</table>

<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr> 
            <th>#</th>
            <th>brand Name</th>
            <th>model Name</th>
            <th>car_type</th>
            <th>user_id</th>
            <th>package_id</th>
            <th>Creation date</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach ($carwashrequest as $data)
        <tr>
            <td>{{ $i }}</td>
            {{-- <td>{{ $data->firstname }}</td> --}}
            <td>{{ $data->brand_name }}</td>
            <td>{{ $data->model_name }}</td>
            <td>{{ $data->car_type }}</td>
            <td>{{ $data->user_id }}</td>
            <td>{{ $data->package_id }}</td>
            <td>{{ $data->created_at }}</td>
            
            <td>
                {{-- {{ link_to_route('data.edit','Edit',[$data->id], ['class'=> 'btn btn-primary btn-xs']) }} --}}

                    {{-- {!! Form::open(['method'=>'delete','route'=>['complain_destroy', $data->id]]) !!} --}}

                {{-- {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!} --}}
           </td>
        </tr>
        <?php $i++; ?>
        @endforeach
       
    </tbody>
</table>                    
                
@endsection

                  

